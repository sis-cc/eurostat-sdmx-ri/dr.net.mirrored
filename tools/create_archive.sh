#!/bin/bash

set -e
set -u
work="${1:-workdir}"
package="${2:-workdir/package}"
scratch="$work/scratch"
mkdir "$work"
git archive -o "$work/src.tar.gz" HEAD
if [[ -f .gitmodules ]]; then
    git submodule foreach --quiet 'git archive --prefix=\$path/ --format=tar.gz -o \$toplevel/src.\$name.tar.gz HEAD'
    if compgen -G 'src.*.tar.gz' > /dev/null; then 
        cat src.*.tar.gz >> "$work/src.tar.gz"
        rm src.*.tar.gz 
    fi
fi