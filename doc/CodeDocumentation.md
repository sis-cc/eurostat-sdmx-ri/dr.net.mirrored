# General

# Tickets
[SDMXRI-1416](https://citnet.tech.ec.europa.eu/CITnet/jira/browse/SDMXRI-1416) - Added a new class to generate a [header id](https://citnet.tech.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/dr.net/commits/a049ab3a5989f6dbdebff07196f0176d852b3356) . 
