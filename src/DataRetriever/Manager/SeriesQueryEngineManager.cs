// -----------------------------------------------------------------------
// <copyright file="SeriesQueryEngineManager.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Manager
{
    using System;

    using Estat.Nsi.DataRetriever.Engines;
    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    ///     The <see cref="IQueryEngineManager{T}" /> implementation for time series
    /// </summary>
    internal class SeriesQueryEngineManager : IQueryEngineManager<DataRetrievalInfoSeries>
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesQueryEngineManager _instance = new SeriesQueryEngineManager();

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesQueryEngineManager" /> class from being created.
        /// </summary>
        private SeriesQueryEngineManager()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesQueryEngineManager Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Get a <see cref="IDataQueryEngine{T}" /> implementation based on the specified <paramref name="info" />
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     a <see cref="IDataQueryEngine{T}" /> implementation based on the specified <paramref name="info" />
        /// </returns>
        public IDataQueryEngine<DataRetrievalInfoSeries> GetQueryEngine(DataRetrievalInfoSeries info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            var crossSectionalMeasureMappings = info.BuildXSMeasures();
            IDataQueryEngine<DataRetrievalInfoSeries> queryEngine = null;

            if (info.MeasureComponent == null)
            {
                queryEngine = SeriesDataQueryEngine.Instance;
            }
            else if (crossSectionalMeasureMappings.Count > 0)
            {
                queryEngine = SeriesDataQueryEngineXsMeasureBuffered.Instance;
            }

            return queryEngine;
        }
    }
}