﻿// -----------------------------------------------------------------------
// <copyright file="LocalDataAnnotationScope.cs" company="EUROSTAT">
//   Date Created : 2018-01-03
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;

namespace Estat.Nsi.DataRetriever.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    /// A scope class for holding the request specific annotations
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    internal class LocalDataAnnotationScope : IDisposable
    {
        /// <summary>
        /// The annotation container
        /// </summary>
        private static AsyncLocal<AnnotationContainer> _annotationContainer = new AsyncLocal<AnnotationContainer>();

        /// <summary>
        /// The dataset annotation container. This is a workaround as the <see cref="IDataWriterEngine.StartDataset"/> is called before retrieving data.
        /// This is not disposed
        /// </summary>
        private static AsyncLocal<AnnotationContainer> _datasetAnnotationContainer = new AsyncLocal<AnnotationContainer>();

        /// <summary>
        /// The has processe data set attributes
        /// </summary>
        private static AsyncLocal<bool> _hasProcesseDataSetAttributes = new AsyncLocal<bool>();

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalDataAnnotationScope" /> class.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="isDatasetLevel">if set to <c>true</c> it tell <see cref="LocalDataAnnotationScope"/> that this is for dataset level.  This is a workaround as the <see cref="IDataWriterEngine.StartDataset"/> is called before retrieving data.</param>
        /// <exception cref="InvalidOperationException">Scope already been set. Dispose first</exception>
        public LocalDataAnnotationScope(IList<MappingInfo> componentMappings, DataRetrieverSettings settings, bool isDatasetLevel)
            : this(componentMappings, null, settings, null)
        {
            if (isDatasetLevel)
            {
                _datasetAnnotationContainer = _annotationContainer;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalDataAnnotationScope"/> class.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="infoTimeMapping"></param>
        /// <exception cref="InvalidOperationException">Scope already been set. Dispose first</exception>
        public LocalDataAnnotationScope(IList<MappingInfo> componentMappings, string dimensionAtObservation, DataRetrieverSettings settings, TimeDimensionMappingEntity infoTimeMapping)
        {
            if (_annotationContainer.Value != null)
            {
                throw new InvalidOperationException("Scope already been set. Dispose first");
            }

            if (settings.OutputLocalDataAsAnnotation)
            {
                _annotationContainer.Value =  new AnnotationContainer(componentMappings, dimensionAtObservation, infoTimeMapping);
            }
            else
            {
                _annotationContainer.Value = null;
            }
        }

        /// <summary>
        /// Gets the annotation container.
        /// </summary>
        /// <value>
        /// The annotation container.
        /// </value>
        public static AnnotationContainer AnnotationContainer => _annotationContainer.Value;

        /// <summary>
        /// Gets the series annotations. Warning calling this the first time changes the state
        /// </summary>
        /// <value>
        /// The series annotations.
        /// </value>
        public static IAnnotation[] SeriesAnnotations
        {
            get
            {
                var annotationContainer = AnnotationContainer;
                if (annotationContainer == null)
                {
                    return null;
                }

                var annotationContainerSeriesAnnotations = annotationContainer.SeriesAnnotations;
                var datasetAnnotations = DataSetAnnotations;
                if (datasetAnnotations == null)
                {
                    return annotationContainerSeriesAnnotations.ToArray();
                }

                return datasetAnnotations.Union(annotationContainerSeriesAnnotations).ToArray();
            }
        }

        /// <summary>
        /// Gets the group annotations.
        /// </summary>
        /// <value>
        /// The series annotations.
        /// </value>
        public static IAnnotation[] GroupAnnotations
        {
            get
            {
                var annotationContainer = AnnotationContainer;
                return annotationContainer?.GroupAnnotations.ToArray();
            }
        }

        /// <summary>
        /// Gets the observation annotations.
        /// </summary>
        /// <value>
        /// The series annotations.
        /// </value>
        public static IAnnotation[] ObsAnnotations
        {
            get
            {
                var annotationContainer = AnnotationContainer;
                if (annotationContainer == null)
                {
                    return null;
                }

                var annotationContainerObsAnnotations = annotationContainer.ObsAnnotations;
                return annotationContainerObsAnnotations.ToArray();
            }
        }

        /// <summary>
        /// Gets the observation annotations.
        /// </summary>
        /// <value>
        /// The series annotations.
        /// </value>
        private static IAnnotation[] DataSetAnnotations
        {
            get
            {
                // Workaround, write the dataset attributes to the first caller
                var datasetcontainer = _datasetAnnotationContainer;
                if (datasetcontainer == null)
                {
                    // Dataset attribute values might be retrieved together with the series
                    if (!_hasProcesseDataSetAttributes.Value && _annotationContainer.Value != null)
                    {
                        _hasProcesseDataSetAttributes.Value = true;
                        return _annotationContainer.Value.DatasetAnnotations.ToArray();
                    }

                    return null;
                }

                // Dataset attributes are retrieved separaterly 
                _datasetAnnotationContainer = null;
                return datasetcontainer.Value?.DatasetAnnotations.ToArray();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _annotationContainer.Value = null;
                _hasProcesseDataSetAttributes.Value = false;
            }
        }
    }
}