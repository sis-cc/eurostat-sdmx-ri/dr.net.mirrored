// -----------------------------------------------------------------------
// <copyright file="SqlBuilderBase.cs" company="EUROSTAT">
//   Date Created : 2023-07-19
//   Copyright (c) 2009, 2023 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.DataRetriever.Utils
{
    using System;
    using System.Globalization;
    using System.Text;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// The sql builder utils class.
    /// </summary>
    internal static class SqlBuilderUtils
    {
        /// <summary>
        /// Adds the confidentiality status filter.
        /// </summary>
        /// <param name="whereBuilder">The whereBuilder.</param>
        /// <param name="dataRetrievalInfo">The dataRetrievalInfo.</param>
        internal static void AddConfidentialityStatusFilter(StringBuilder whereBuilder, DataRetrievalInfo dataRetrievalInfo)
        {
            if (!dataRetrievalInfo.ValidateConfidentialityStatusValues || dataRetrievalInfo.CanReadConfidentialObservations)
            {
                return;
            }

            var confidentialityStatusFilter = GenerateConfidentialityStatusFilter(dataRetrievalInfo);

            if (string.IsNullOrEmpty(confidentialityStatusFilter))
            {
                return;
            }

            var containsFilters = whereBuilder.Length > 0;

            if (containsFilters)
            {
                whereBuilder.Insert(0, "(");
                whereBuilder.AppendLine(") AND (");
            }

            whereBuilder.AppendLine(confidentialityStatusFilter);

            if (containsFilters)
            {
                whereBuilder.AppendLine(")");
            }
        }

        private static string GenerateConfidentialityStatusFilter(DataRetrievalInfo dataRetrievalInfo)
        {
            var result = new StringBuilder();

            IComponentMappingBuilder confStatusMappingBuilder;
            if (dataRetrievalInfo.MappingSet.ComponentMappingBuilders.TryGetValue("CONF_STATUS", out confStatusMappingBuilder))
            {
                result.AppendLine(confStatusMappingBuilder.GenerateComponentWhere(string.Empty, OperatorType.Exact));
                result.AppendLine("OR");
                result.AppendLine(confStatusMappingBuilder.GenerateComponentWhere(dataRetrievalInfo.Settings.ConfidentialityStatusValues, false, true));
            }
            else
            {
                throw new DataRetrieverException("Could not find component 'CONF_STATUS'");
            }

            if (dataRetrievalInfo.Settings.ConfidentialityStatusValues.Contains("E"))
            {
                IComponentMappingBuilder embargoTimeMappingBuilder;
                if (dataRetrievalInfo.MappingSet.ComponentMappingBuilders.TryGetValue("EMBARGO_TIME", out embargoTimeMappingBuilder))
                {
                    result.AppendLine("OR");
                    result.AppendLine(string.Format("({0} AND {1})",
                        confStatusMappingBuilder.GenerateComponentWhere("E", OperatorType.Exact),
                        embargoTimeMappingBuilder.GenerateComponentWhere(DateTime.UtcNow.ToString(CultureInfo.InvariantCulture), OperatorType.LessThan)));
                }
            }

            return result.ToString();
        }
    }
}
