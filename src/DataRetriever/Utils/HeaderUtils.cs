﻿using Estat.Sri.SdmxXmlConstants;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Header;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.DataRetriever.Utils
{
    public static class HeaderUtils
    {
        public static SdmxStructureEnumType GetStructureUsage(IHeader messageHeader, SdmxStructureEnumType structureUsageFromConfig)
        {
            var structureUsageInDb = messageHeader.GetAdditionalAttribtue(nameof(ElementNameTable.StructureUsage));
            if (string.IsNullOrWhiteSpace(structureUsageInDb))
            {
                return structureUsageFromConfig;
            }
            else
            {
                return SdmxStructureType.ParseClass(structureUsageInDb).EnumType;
            }
        }
    }
}
