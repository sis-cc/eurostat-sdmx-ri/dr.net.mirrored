// -----------------------------------------------------------------------
// <copyright file="GlobalSuppressions.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

[assembly:
    SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", 
        Target =
            "Estat.Nsi.DataRetriever.Engines.SeriesDataQueryEngineBase.#ExecuteGroupSqlQuery(Estat.Nsi.DataRetriever.Model.GroupInformation,Estat.Nsi.DataRetriever.Model.DataRetrievalInfoSeries,System.Data.Common.DbConnection)", 
        Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", 
        Target =
            "Estat.Nsi.DataRetriever.Engines.SeriesDataQueryEngineBase.#ExecuteDataSetAttributeSqlQuery(Estat.Nsi.DataRetriever.Model.DataRetrievalInfoSeries,System.Data.Common.DbConnection)", 
        Justification = "default")]
[assembly:
    SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Scope = "member", 
        Target = "Estat.Nsi.DataRetriever.Engines.DataQueryEngineBase`2.#ExecuteDbCommand(!0)", 
        Justification = "default")]