﻿// -----------------------------------------------------------------------
// <copyright file="DataRetrieverHelper.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever
{
    using System;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     A helper class with static methods
    /// </summary>
    public static class DataRetrieverHelper
    {
        /// <summary>
        ///     This method retrieves the <see cref="IDataflowObject" /> from a
        ///     <see cref="IBaseDataQuery" />
        /// </summary>
        /// <param name="query">
        ///     The <see cref="IBaseDataQuery" /> containing the
        ///     <see cref="IDataflowObject" />
        /// </param>
        /// <returns>
        ///     The <see cref="IDataflowObject" />
        /// </returns>
        public static IDataflowObject GetDataflowFromQuery(IBaseDataQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            return query.Dataflow;
        }
    }
}