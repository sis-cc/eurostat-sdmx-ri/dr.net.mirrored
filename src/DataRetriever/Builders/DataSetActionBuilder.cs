﻿// -----------------------------------------------------------------------
// <copyright file="DataSetActionBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-11-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Builders
{
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using System;

    /// <summary>
    ///     The method that builds a <see cref="DatasetAction"/> from a <see cref="ObservationAction"/>
    /// </summary>
    public class DataSetActionBuilder : IBuilder<DatasetAction, ObservationAction>
    {
        /// <summary>
        ///     The method that builds a <see cref="DatasetAction"/> from a <see cref="ObservationAction"/>
        /// </summary>
        /// <param name="observationAction">
        ///     The input <see cref="ObservationAction"/>
        /// </param>
        /// <returns>
        ///     The output <see cref="DatasetAction"/>
        /// </returns>
        public DatasetAction Build(ObservationAction observationAction)
        {
            DatasetActionEnumType datasetAction = DatasetActionEnumType.Information;
            switch (observationAction.EnumType)
            {
                case ObservationActionEnumType.Added:
                    datasetAction = DatasetActionEnumType.Append;
                    break;
                case ObservationActionEnumType.Updated:
                    datasetAction = DatasetActionEnumType.Replace;
                    break;
                case ObservationActionEnumType.Deleted:
                    datasetAction = DatasetActionEnumType.Delete;
                    break;
            }

            return DatasetAction.GetFromEnum(datasetAction);
        }
    }
}