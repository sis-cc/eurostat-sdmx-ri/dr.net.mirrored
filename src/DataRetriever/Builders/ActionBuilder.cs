// -----------------------------------------------------------------------
// <copyright file="ActionBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-11-11
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Nsi.DataRetriever.Model;
using Estat.Sri.Mapping.Api.Engine;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    /// Build the requested Observation actions
    /// </summary>
    internal class ActionBuilder
    {
        /// <summary>
        /// Builds the specified list of Observation Actions.
        /// </summary>
        /// <param name="mappingSet">The mapping set.</param>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>The list of observation actions</returns>
        public IList<ObservationAction> Build(DataRetrievalInfo info)
        {
            var actions = new List<ObservationAction>();
            var mappingSet = info.MappingSet;
            // Delta query support for REST. Under some conditions we need more than one dataset
            if (info.Query.LastUpdatedDate != null && mappingSet.LastUpdateMapping != null && !info.ShouldIncludeHistory)
            {
                if (mappingSet.UpdateStatusMapping != null)
                {
                    if (mappingSet.UpdateStatusMapping.Constant != null)
                    {
                        actions.Add(mappingSet.UpdateStatusMapping.Constant);
                    }
                    else
                    {
                        actions.AddRange(ObservationAction.Values.Where(observationAction => mappingSet.UpdateStatusMapping.GetLocalAction(observationAction) != null));
                    }
                }
                // If no UpdateStatusMapping present & EditorType = MultiQuery use Updated + Deleted actions
                else if (mappingSet.Dataset.EditorType == MultiQueryDatasetEditorEngine.EditorType)
                {
                    actions.AddRange(MultiQueryDatasetEditorEngine.GetActions(mappingSet.Dataset));
                }
            }

            if(!actions.Any())
            {
                actions.Add(ObservationAction.GetFromEnum(ObservationActionEnumType.Active));
            }

            return actions;
        }

        /// <summary>
        /// Builds the specified list of Observation Actions.
        /// </summary>
        /// <param name="mappingSet">The mapping set.</param>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>The list of observation actions</returns>
        public IList<ObservationAction> Build(IComponentMappingContainer mappingSet, IComplexDataQuery dataQuery)
        {
            var requestedAction = dataQuery.ObservationAction;
            var updateStatus = mappingSet.UpdateStatusMapping;
            if (updateStatus != null)
            {
                if (updateStatus.Constant != null)
                {
                    if (
                        !(requestedAction.EnumType == ObservationActionEnumType.Active && updateStatus.Constant.IsActive())
                        && !updateStatus.Constant.Equals(requestedAction))
                    {
                        throw new SdmxNoResultsException("Could not find data for the requested observation action");
                    }
                }
                else if (requestedAction.EnumType != ObservationActionEnumType.Active && updateStatus.GetLocalAction(requestedAction) == null)
                {
                    throw new SdmxNoResultsException("Could not find data for the requested observation action");
                }
            }

            return new[] { requestedAction };
        }
    }
}