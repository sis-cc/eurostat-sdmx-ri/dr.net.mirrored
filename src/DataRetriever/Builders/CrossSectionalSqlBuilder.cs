// -----------------------------------------------------------------------
// <copyright file="CrossSectionalSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     SQL Builder for CrossSectional output
    /// </summary>
    internal class CrossSectionalSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly CrossSectionalSqlBuilder _instance = new CrossSectionalSqlBuilder();

        /// <summary>
        ///     The ordered component builder
        /// </summary>
        private static readonly SeriesOrderedDimensionBuilder _orderedComponentBuilder =
            new SeriesOrderedDimensionBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(CrossSectionalSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="CrossSectionalSqlBuilder" /> class from being created.
        /// </summary>
        private CrossSectionalSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static CrossSectionalSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            _logger.Info(Resources.InfoBeginGenerateSql);

            SqlQuery sqlQuery = new SqlQuery();
            string sql;

            try
            {
                // Generate Query subparts
                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                sql = GenerateSelect(false, info.ComponentMapping.Values, databaseSetting);
                sqlQuery.AppendSql(sql);

                sqlQuery.AppendSql(GenerateFrom(info));

                // the WHERE part
                sqlQuery.AppendSql(GenerateWhere(info));

                sqlQuery.AppendSql(GenerateXSOrderByLocalColumns(info));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), 
                    Resources.ErrorUnableToGenerateSQL);

                // ErrorTypes.QUERY_PARSING_ERROR, Resources.ErrorUnableToGenerateSQL, ex);
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSql);

            info.SqlString = sqlQuery.GetSql();
        }

        /// <summary>
        ///     This method generates the ORDER BY part of the query
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     The string containing the ORDER BY part of the query
        /// </returns>
        private static string GenerateXSOrderByLocalColumns(DataRetrievalInfo info)
        {
            var orderComponents = _orderedComponentBuilder.Build(info);
            var crossOrderedComponents = orderComponents.ToList();
            crossOrderedComponents.Sort(OnCrossSectionalComparison);
            var orderBy = GenerateOrderByWithPagination(info, crossOrderedComponents);

            return orderBy;
        }

        /// <summary>
        ///     Gets an array of the CrossSectional flags of <paramref name="component" />
        /// </summary>
        /// <param name="component">
        ///     The <see cref="IComponent" />
        /// </param>
        /// <returns>
        ///     an array of the CrossSectional flags of <paramref name="component" />
        /// </returns>
        private static bool[] GetCrossSectionalFlags(IComponent component)
        {
            var isMeasureDimension = false;
            var isFrequencyDimension = false;
            var isTimeDimension = false;
            var maintainableParent = (IDataStructureObject)component.MaintainableParent;
            var crossDsd = maintainableParent as ICrossSectionalDataStructureObject;

            var dimension = component as IDimension;
            if (dimension != null)
            {
                isMeasureDimension = dimension.MeasureDimension;
                isFrequencyDimension = dimension.FrequencyDimension;
                isTimeDimension = dimension.TimeDimension;
            }

            var observationLevel = false;
            var sectionLevel = false;
            var groupLevel = false;
            var datasetLevel = false;
            if (crossDsd != null)
            {
                observationLevel = crossDsd.GetCrossSectionalAttachObservation(component.StructureType).Any(component.Equals);
                sectionLevel = !observationLevel && crossDsd.GetCrossSectionalAttachSection(true, component.StructureType).Any(component.Equals);
                groupLevel = !sectionLevel && !observationLevel && crossDsd.GetCrossSectionalAttachGroup(true, component.StructureType).Any(component.Equals);
                datasetLevel = !groupLevel && !sectionLevel && !observationLevel && crossDsd.GetCrossSectionalAttachDataSet(true, component.StructureType).Any(component.Equals);
            }

            return new[] { isMeasureDimension, observationLevel, sectionLevel, groupLevel || isFrequencyDimension || isTimeDimension, datasetLevel };
        }

        /// <summary>
        ///     Compare two <paramref name="x" /> and <paramref name="y" /> based on their Cross Sectional attachment level. The
        ///     <see cref="IComponent" /> attached to DataSet will be first, followed by Group, Section and finally
        ///     observation.
        /// </summary>
        /// <param name="x">
        ///     The first <see cref="IComponent" /> to compare
        /// </param>
        /// <param name="y">
        ///     The second <see cref="IComponent" /> to compare
        /// </param>
        /// <returns>
        ///     A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" /> , 1 if
        ///     <paramref name="x" /> is attached to a lower level than <paramref name="y" /> , -1 if <paramref name="x" /> is
        ///     attached to a higher level than <paramref name="y" /> , else 0.
        /// </returns>
        private static int OnCrossSectionalComparison(IComponent x, IComponent y)
        {
            var firstConditions = GetCrossSectionalFlags(x);
            var secondConditions = GetCrossSectionalFlags(y);

            for (var i = 0; i < firstConditions.Length; i++)
            {
                if (firstConditions[i] && secondConditions[i])
                {
                    return 0;
                }

                if (firstConditions[i] && !secondConditions[i])
                {
                    return 1;
                }

                if (!firstConditions[i] && secondConditions[i])
                {
                    return -1;
                }
            }

            return 0;
        }
    }
}