// -----------------------------------------------------------------------
// <copyright file="SeriesGroupSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.DataRetriever;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     This class is responsible for building SQL Queries for metadata.
    /// </summary>
    internal class SeriesMetadataSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesMetadataSqlBuilder _instance = new SeriesMetadataSqlBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SeriesMetadataSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesMetadataSqlBuilder" /> class from being created.
        /// </summary>
        private SeriesMetadataSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesMetadataSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the metadata for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            _logger.Info(Resources.InfoBeginGenerateSql);

            var seriesInfo = info as DataRetrievalInfoSeries;
            if (seriesInfo == null)
            {
                throw new ArgumentException("seriesInfo is not of DataRetrievalInfoSeries type");
            }

            GenerateMetadataSql(info, seriesInfo);
        }

        private static void GenerateMetadataSql(DataRetrievalInfo info, DataRetrievalInfoSeries seriesInfo)
        {
            SqlQuery sqlQuery = new SqlQuery();
            string sql;

            try
            {
                var mappingInfoList = new List<MappingInfo>(seriesInfo.MainComponentMappings.Where(x => x.Dimension != null));

                if (seriesInfo.WriteMetadataAttributes)
                {
                    mappingInfoList.AddRange(seriesInfo.MetadataAttributes);
                }

                var mappingEntities = ConvertToMapping(mappingInfoList);

                if (seriesInfo.TimeMapping != null)
                {
                    mappingEntities.Add(seriesInfo.TimeMapping);
                }

                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                sql = GenerateSelect(false, mappingEntities, databaseSetting);
                sqlQuery.AppendSql(sql);

                sqlQuery.AppendSql(GenerateFrom(info, true));
                sqlQuery.AppendSql(GenerateMetadataWhere(seriesInfo));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError),
                    Resources.ErrorUnableToGenerateSQL);
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSql);

            if (seriesInfo.WriteMetadataAttributes)
            {
                info.MetadataSqlString = sqlQuery.GetSql();
            }
            else
            {
                info.MetadataAvailabilitySqlString = sqlQuery.GetSql();
            }
        }
    }
}