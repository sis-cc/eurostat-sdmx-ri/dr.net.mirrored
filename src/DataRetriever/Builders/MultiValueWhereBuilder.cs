using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Estat.Nsi.DataRetriever.Model;
using Estat.Sri.Mapping.Api.Builder;
using log4net;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

namespace Estat.Sri.DataRetriever.Builders
{
    /// <summary>
    /// IN clause optimization
    /// Only when we have multiple value and equal operator
    /// Still it might be used if the mapping has more than one column and the DDB is sqlserver
    /// </summary>
    internal class MultiValueWhereBuilder
    {
        /// <summary>
        ///     The _logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(MultiValueWhereBuilder));
        private DataRetrievalInfo info;

        public MultiValueWhereBuilder(DataRetrievalInfo info)
        {
            if (info is null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            this.info = info;
        }

        public string GenerateComponentWhere(IComplexDataQuerySelection selection)
        {
            if (selection is null)
            {
                throw new ArgumentNullException(nameof(selection));
            }

            var values = new HashSet<string>(selection.Values.Select(x => x.Value));
           return GenerateComponentWhere(selection.ComponentId, values, info);
        }
        public string GenerateComponentWhere(IDataQuerySelection selection, bool withNull)
        {
            if (selection is null)
            {
                throw new ArgumentNullException(nameof(selection));
            }

            return GenerateComponentWhere(selection.ComponentId, selection.Values, info, withNull);
        }

        /// <summary>
        ///     Maps a component to one or more local columns and it's value to one or more local codes
        /// </summary>
        /// <param name="id">
        ///     The DSD Component identifier e.g. FREQ
        /// </param>
        /// <param name="conditionValues">
        ///     The DSD Component condition values (from the SDMX Query)
        /// e.g. from a REST v1 data query <c>/rest/data/DATAFLOW_ID/</c>
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval status
        /// </param>
        /// <returns>
        ///     An string containing the SQL query where condition
        /// </returns>
        private static string GenerateComponentWhere(string id,ISet<string> conditionValues, DataRetrievalInfo info, bool withNull=false)
        {
            // MappingEntity mapping;
            // check if there is a mapping for this component
            if (id != null && conditionValues != null)
            {
                IComponentMappingBuilder componentMappingType;
                if (info.MappingSet.ComponentMappingBuilders.TryGetValue(id, out componentMappingType))
                {
                    return componentMappingType.GenerateComponentWhere(conditionValues, withNull);
                }
                else if (info.MeasureComponent == null || !id.Equals(info.MeasureComponent.Id))
                {
                    // component is not in the mapping
                    _logger.WarnFormat(CultureInfo.InvariantCulture, " {0} is not mapped. Queried with Value: '{1}' ", id, string.Join(",", conditionValues));
                    return "1=0";
                }
            }
            return string.Empty;
        }

    }
}
