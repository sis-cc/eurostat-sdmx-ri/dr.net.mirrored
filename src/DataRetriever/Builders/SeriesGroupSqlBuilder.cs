// -----------------------------------------------------------------------
// <copyright file="SeriesGroupSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.DataRetriever;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

    /// <summary>
    ///     This class is responsible for building SQL Queries for Time Series groups.
    /// </summary>
    internal class SeriesGroupSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesGroupSqlBuilder _instance = new SeriesGroupSqlBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SeriesGroupSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesGroupSqlBuilder" /> class from being created.
        /// </summary>
        private SeriesGroupSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesGroupSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            var seriesInfo = info as DataRetrievalInfoSeries;
            if (seriesInfo == null)
            {
                throw new ArgumentException("seriesInfo is not of DataRetrievalInfoSeries type");
            }

            // generate sql queries for groups
            foreach (var groupEntity in seriesInfo.Groups)
            {
                var information = groupEntity.Value;
                information.SQL = GenerateGroupSql(information, seriesInfo);
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="groupBean">
        ///     The group Bean.
        /// </param>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     The string containing the SQL query that needs to be executed on the dissemination database, in order to return the
        ///     data required by the input query
        /// </returns>
        private string GenerateGroupSql(GroupInformation groupBean, DataRetrievalInfoSeries info)
        {
            _logger.Info(Resources.InfoBeginGenerateSql);

            SqlQuery sqlQuery = new SqlQuery();
            string sql;

            try
            {
                // Generate Query subparts
                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                sql = GenerateSelect(true, 
                    ConvertToMapping(groupBean.ComponentMappings), 
                    databaseSetting, 
                    groupBean.ThisGroup.DimensionRefs.Contains(DimensionObject.TimeDimensionFixedId) ? info.TimeMapping : null);

                sqlQuery.AppendSql(sql);

                sqlQuery.AppendSql(GenerateFrom(info));

                if (string.IsNullOrEmpty(info.SqlWhereCache))
                {
                    info.SqlWhereCache = GenerateWhere(info);
                }

                sqlQuery.AppendSql(info.SqlWhereCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), 
                    Resources.ErrorUnableToGenerateSQL);

                // ErrorTypes.QUERY_PARSING_ERROR, Resources.ErrorUnableToGenerateSQL, ex);
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSql);

            return sqlQuery.GetSql();
        }
    }
}