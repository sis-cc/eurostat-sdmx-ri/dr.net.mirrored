﻿// -----------------------------------------------------------------------
// <copyright file="LocalDataAnnotationBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-01-03
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Builders
{
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;

    internal class LocalDataAnnotationBuilder
    {
        /// <summary>
        /// Builds the specified column.
        /// </summary>
        /// <param name="column">The column.</param>
        /// <returns>The <see cref="IAnnotationMutableObject"/></returns>
        public IAnnotationMutableObject Build(DataSetColumnEntity column)
        {
            IAnnotationMutableObject annotation = new AnnotationMutableCore();
            annotation.Type = "SRI_LOCAL_DATA";
            annotation.Title = column.Name;
            annotation.AddText("en", string.Empty); // this should be updated everytime there is a new record
            return annotation;
        }
    }
}