﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationMappingBuilder.cs" company="EUROSTAT">
//   Date Created : 2018-01-03
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Builders
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    /// <summary>
    /// Build the mapping annotation.
    /// </summary>
    /// <seealso cref="DataRetrieverSettings.OutputLocalDataAsAnnotation"/>
    internal class AnnotationMappingBuilder
    {
        /// <summary>
        /// The annotation type
        /// </summary>
        private const string AnnotationType = "SRI_MAPPING";

        /// <summary>
        /// Builds the <c>SRI_MAPPING</c> Annotation from the Component Mappings.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <returns>One annotation per Component.</returns>
        /// <seealso cref="DataRetrieverSettings.OutputLocalDataAsAnnotation"/>
        public IEnumerable<IAnnotation> Build(IComponentMappingContainer container)
        {
            foreach (var containerComponentMapping in container.ComponentMappings)
            {
                var annotation = new AnnotationMutableCore();
                annotation.Type = AnnotationType;
                annotation.Title = containerComponentMapping.Key;
                if (ObjectUtil.ValidCollection(containerComponentMapping.Value.GetColumns()))
                {
                    var columnsSeparatedWithSemicolon = string.Join(";", containerComponentMapping.Value.GetColumns().Select(entity => entity.Name));
                    annotation.AddText("en", columnsSeparatedWithSemicolon);
                }

                yield return new AnnotationObjectCore(annotation, null);
            }
        }
    }
}