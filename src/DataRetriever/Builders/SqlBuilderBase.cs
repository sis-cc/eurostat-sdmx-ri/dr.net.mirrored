// -----------------------------------------------------------------------
// <copyright file="SqlBuilderBase.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Mapping.Api.Engine;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.DataRetriever;
    using Estat.Sri.DataRetriever.Builders;
    using Estat.Sri.DataRetriever.Utils;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    using ITimeRange = Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex.ITimeRange;
    using TimeRangeCore = Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference.Complex.TimeRangeCore;

    /// <summary>
    ///     The SQL builder base.
    /// </summary>
    internal class SqlBuilderBase
    {
        /// <summary>
        ///     THe comma separator followed by space.
        /// </summary>
        private const string CommaSeparator = ", ";

        /// <summary>
        ///     The _logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SqlBuilderBase));

        /// <summary>
        ///     The switched-off dimension value.
        /// </summary>
        private static readonly string _switchedOffDimensionValue = SpecialDimensionValue.GetFromEnum(SpecialDimensionValueEnumType.SwitchedOff).DimensionValue;

        /// <summary>
        ///     Convert <paramref name="componentMappings" /> to a ICollection(MappingEntity) collection
        /// </summary>
        /// <param name="componentMappings">
        ///     The component mappings.
        /// </param>
        /// <returns>
        ///     a ICollection(MappingEntity) collection
        /// </returns>
        protected static ICollection<IMappingEntity> ConvertToMapping(ICollection<MappingInfo> componentMappings)
        {
            var mappingEntities = new List<IMappingEntity>(componentMappings.Count);
            foreach (var componentMapping in componentMappings)
            {
                mappingEntities.Add(componentMapping.Mapping);
            }

            return mappingEntities;
        }

        /// <summary>
        ///     This method generates the WHERE part of the complex query
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     The string containing the required SQL part. For example, "where (FREQ='M')"
        /// </returns>
        protected static string GenerateComplexWhere(DataRetrievalInfo info)
        {
            IComplexDataQuery query = info.ComplexQuery;
            _logger.Info(Resources.InfoBeginGenerateWhere);

            var whereBuilder = new StringBuilder();

            if (query != null)
            {
                if (query.HasSelections())
                {
                    string dimId = string.Empty;
                    if (query.DataStructure.TimeDimension != null)
                    {
                        dimId = query.DataStructure.FrequencyDimension?.Id;
                    }

                    IList<IComplexDataQuerySelectionGroup> selGrps = query.SelectionGroups;
                    foreach (IComplexDataQuerySelectionGroup sg in selGrps)
                    {
                        HandleGroups(info, whereBuilder, sg, dimId);
                    }
                }

                var mappingSetEntity = info.MappingSet;
                var clauses = GenerateSqlClauses(mappingSetEntity, query.LastUpdatedDateTimeRange, query.ObservationAction, false);

                AddClauses(info, clauses, whereBuilder);

                // MAT-274
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Insert(0, "where ");
                }
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedWhereFormat1, whereBuilder));
            _logger.Info(Resources.InfoEndGenerateWhere);

            return whereBuilder.ToString();
        }

        /// <summary>
        ///     This method generates the FROM part of the query.
        /// </summary>
        /// <param name="info">
        ///     The <see cref="DataRetrievalInfo" /> that contains the mappings for the Dataflow of the query
        /// </param>
        /// <param name="isMetadata">
        ///     The optional isMetadata flag.
        /// </param>
        /// <returns>
        ///     A string containing the FROM part
        /// </returns>
        protected static string GenerateFrom(DataRetrievalInfo info, bool isMetadata = false)
        {
            _logger.Info(Resources.DataRetriever_GenerateFrom_Begin_GenerateFrom____);

            var dataset = !isMetadata
                ? info.MappingSet.Dataset
                : info.MappingSet.MetadataDataset;

            var query = dataset.EditorType == MultiQueryDatasetEditorEngine.EditorType
                ? MultiQueryDatasetEditorEngine.GenerateSqlQuery(dataset, info.CurrentObservationAction.EnumType.ToString())
                : dataset.Query;

            var from = " from (" + query + ") virtualDataset ";

            // log for easy debug
            _logger.Info(
                string.Format(
                    CultureInfo.InvariantCulture,
                    Resources.DataRetriever_GenerateFrom_Generated_FROM___0_,
                    from));
            _logger.Info(Resources.DataRetriever_GenerateFrom_End_GenerateFrom____);

            return from;
        }


        /// <summary>
        ///     This method generates the ORDER BY fragment of the query with pagination parts for the specified <paramref name="orderComponents" />
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        /// <param name="orderComponents">
        ///     The order components.
        /// </param>
        /// <param name="enforceDescOrder">Flag to enforce generation order by clause for descending order</param>
        /// <returns>
        ///     the ORDER BY part of the query for the specified <paramref name="orderComponents" />
        /// </returns>
        protected static string GenerateOrderByWithPagination(DataRetrievalInfo info, IEnumerable<IComponent> orderComponents, bool enforceDescOrder = false)
        {
            _logger.Info(Resources.InfoGenerateOrderByWithPagination);

            var orderByWithPagination = new StringBuilder(GenerateOrderBy(info, orderComponents, enforceDescOrder));

            var pagination = GeneratePagination(info);

            orderByWithPagination.AppendLine(pagination);

            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedOrderByFormat1, orderByWithPagination));
            _logger.Info(Resources.InfoEndGenerateOrderBy);

            return orderByWithPagination.ToString();
        }

        /// <summary>
        ///     This method generates the ORDER BY part of the query for the specified <paramref name="orderComponents" />
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        /// <param name="orderComponents">
        ///     The order components.
        /// </param>
        /// <param name="enforceDescOrder">Flag to enforce generation order by clause for descending order</param>
        /// <returns>
        ///     the ORDER BY part of the query for the specified <paramref name="orderComponents" />
        /// </returns>
        protected static string GenerateOrderBy(DataRetrievalInfo info, IEnumerable<IComponent> orderComponents, bool enforceDescOrder = false)
        {
            _logger.Info(Resources.InfoGenerateOrderBy);

            var orderBy = new StringBuilder(" ORDER BY ");

            var orderByClauseFromDataset = (info.HasLastNObservations && !info.HasFirstAndLastNObservations) || enforceDescOrder
                ? info.MappingSet.Dataset?.DatasetPropertyEntity?.OrderByTimePeriodDesc
                : info.MappingSet.Dataset?.DatasetPropertyEntity?.OrderByTimePeriodAsc;

            if (info.IsTimePeriodAtObservation
                && !string.IsNullOrWhiteSpace(orderByClauseFromDataset)
            )
            {
                orderBy.AppendLine(orderByClauseFromDataset);
            }
            else
            {
                orderBy.AppendLine(GenerateOrderByFromComponents(info, orderComponents, enforceDescOrder));
            }

            // log for easy debug
            _logger.Info(
                string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedOrderByFormat1, orderBy));
            _logger.Info(Resources.InfoEndGenerateOrderBy);

            return orderBy.ToString();
        }

        /// <summary>
        ///     This method generates the ORDER BY part of the query for the specified <paramref name="orderComponents" /> using component mapping
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        /// <param name="orderComponents">
        ///     The order components.
        /// </param>
        /// <param name="enforceDescOrder">Flag to enforce generation order by clause for descending order</param>
        /// <returns>
        ///     the ORDER BY part of the query for the specified <paramref name="orderComponents" />
        /// </returns>
        private static string GenerateOrderByFromComponents(DataRetrievalInfo info, IEnumerable<IComponent> orderComponents, bool enforceDescOrder = false)
        {
            var orderBy = new StringBuilder();

            var orderColumns = new List<string>();

            var effectiveDimensionAtObs = info.EffectiveDimensionAtObservation;
            var componentEntities = orderComponents as IComponent[]
                                                             ?? orderComponents.ToArray();
            var ddbType = info.MappingSet.DisseminationConnection.DbType;
            var provider = DatabaseType.GetProviderName(ddbType);
            var databaseSetting = DatabaseType.DatabaseSettings[provider];
            var reservedKeywordToStringFormat = GetReservedKeywordToStringFormat(databaseSetting);

            if (info.ShouldIncludeHistory)
            {
                orderBy.Append(info.MappingSet.LastUpdateMapping.Column.Name + ", ");
                orderBy.Append(info.MappingSet.UpdateStatusMapping.Column.Name + ", ");
            }

            // TODO there might be an issue with dim at obs sharing a column with another obs. 
            // add each components contribution to the order by of the query
            foreach (var component in componentEntities)
            {
                if (info.ComponentMapping.TryGetValue(component.Id, out var mapping))
                {
                    foreach (DataSetColumnEntity column in mapping.GetColumns())
                    {
                        AddColumnName(orderColumns, column, reservedKeywordToStringFormat);
                    }
                }
            }

            bool hasLastObs = (!info.HasFirstAndLastNObservations && info.HasLastNObservations) || enforceDescOrder;
            if (hasLastObs)
            {
                var dimAtObsComponent =
                    componentEntities.FirstOrDefault(entity => entity.Id.Equals(effectiveDimensionAtObs));

                // Groups might not have the dimension at observation
                // DataSets will not have dimension at observation.
                if (dimAtObsComponent != null)
                {
                    var mappingEntity = info.ComponentMapping[dimAtObsComponent.Id];
                    for (int i = 0; i < orderColumns.Count; i++)
                    {
                        var column = orderColumns[i];
                        if (mappingEntity.GetColumns().Any(entity =>
                            string.IsNullOrEmpty(reservedKeywordToStringFormat)
                                ? entity.Name.Equals(column)
                                : string.Format(reservedKeywordToStringFormat, entity.Name).Equals(column)))
                        {
                            orderColumns[i] += " DESC";
                        }
                    }
                }
            }

            orderBy.AppendLine(string.Join(CommaSeparator, orderColumns));

            return orderBy.ToString();
        }

        /// <summary>
        ///     This method generates the pagination fragment of an SQL query />
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        protected static string GeneratePagination(DataRetrievalInfo info)
        {
            _logger.Info(Resources.InfoGeneratePagination);

            string pagination = string.Empty;
            if (info.Range?.Item1 != null && info.Range?.Item2 != null)
            {
                //Do not apply pagination when at least one dimension/attribute has transcodings defined. 
                if (!info.ComponentMapping.Any(c => c.Value.HasTranscoding()))
                {
                    pagination =
                        $"OFFSET {info.Range.Item1} ROWS FETCH NEXT {info.Range.Item2 + 1 - info.Range.Item1} ROWS ONLY";
                }
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedPaginationFormat1, pagination));
            _logger.Info(Resources.InfoEndGenerateOrderBy);
            return pagination;
        }

        /// <summary>
        ///     This method generates the SELECT part of the query with the columns inside <paramref name="mappings" />
        /// </summary>
        /// <param name="isDistinct">
        ///     Whether the distinct keyword should be included
        /// </param>
        /// <param name="mappings">
        ///     The component mappings that contain the columns to be included at the select statement
        /// </param>
        /// <param name="databaseSetting">
        /// The database settings.
        /// </param>
        /// <param name="timeMappingEntity">
        ///     The optional component mapping of time.
        /// </param>
        /// <returns>
        ///     The SQL string.
        /// </returns>
        protected static string GenerateSelect(bool isDistinct, IEnumerable<IMappingEntity> mappings, DatabaseSetting databaseSetting, TimeDimensionMappingEntity timeMappingEntity = null)
        {
            if (mappings == null)
            {
                throw new ArgumentNullException("mappings");
            }

            var reservedKeywordToStringFormat = GetReservedKeywordToStringFormat(databaseSetting);
            string sql = string.Empty;
            StringBuilder sb = new StringBuilder(sql);

            _logger.Info(Resources.InfoBeginGenerateSelect);

            sb.Append("select ");
            sb.Append(isDistinct ? " distinct " : string.Empty);

            var selectArray = new List<string>();

            foreach (var componentMapping in mappings)
            {
                foreach (DataSetColumnEntity column in componentMapping.GetColumns())
                {
                    AddColumnName(selectArray, column, reservedKeywordToStringFormat);
                }
            }

            if (timeMappingEntity != null)
            {
                foreach (DataSetColumnEntity column in timeMappingEntity.GetColumns())
                {
                    AddColumnName(selectArray, column, reservedKeywordToStringFormat);
                }
            }

            sb.Append(string.Join(CommaSeparator, selectArray.ToArray()));

            sql = sb.ToString();

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSelectFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSelect);

            return sql;
        }

        /// <summary>
        ///     This method generates the WHERE part of the query
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     The string containing the required SQL part. For example, "where (FREQ='M')"
        /// </returns>
        protected static string GenerateWhere(DataRetrievalInfo info)
        {
            IDataQuery query = info.Query;
            _logger.Info(Resources.InfoBeginGenerateWhere);

            var whereBuilder = new StringBuilder();

            if (query != null)
            {
                if (query.HasSelections())
                {
                    string dimId = string.Empty;
                    if (query.DataStructure.TimeDimension != null)
                    {
                        dimId = query.DataStructure.FrequencyDimension?.Id;
                    }

                    HandleSelectionGroups(info, query, whereBuilder, dimId);
                }

                if (query is IDataQueryV2 dataQueryV2)
                {
                    HandleComponentFilters(dataQueryV2.ComponentFilters, whereBuilder, info);
                }

                var timeRange = query.LastUpdatedDate != null ? new ITimeRange[] { new TimeRangeCore(false, query.LastUpdatedDate, null, true, false) } : null;
                var mappingSetEntity = info.MappingSet;
                var clauses = GenerateSqlClauses(mappingSetEntity, timeRange, info.CurrentObservationAction, info.ShouldIncludeHistory);

                AddClauses(info, clauses, whereBuilder);

                SqlBuilderUtils.AddConfidentialityStatusFilter(whereBuilder, info);

                // MAT-274
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Insert(0, "where ");
                }
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedWhereFormat1, whereBuilder));
            _logger.Info(Resources.InfoEndGenerateWhere);

            return whereBuilder.ToString();
        }

        /// <summary>
        ///     This method generates the WHERE part of the metadata query
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <returns>
        ///     The string containing the required SQL part. For example, "where (FREQ='M')"
        /// </returns>
        protected static string GenerateMetadataWhere(DataRetrievalInfoSeries info)
        {
            IDataQuery query = info.Query;
            _logger.Info(Resources.InfoBeginGenerateWhere);

            var whereBuilder = new StringBuilder();

            if (query != null)
            {
                if (query.HasSelections() || info.Settings.MetadataLevel == MetadataLevelEnumType.UpperOnly || info.Settings.MetadataLevel == MetadataLevelEnumType.CurrentOnly)
                {
                    HandleMetadataSelectionGroups(info,
                        query,
                        whereBuilder,
                        query.DataStructure.TimeDimension != null
                            ? query.DataStructure.FrequencyDimension?.Id
                            : string.Empty);
                }

                if (info.WriteMetadataAttributes)
                {
                    var timeRange = query.LastUpdatedDate != null
                        ? new ITimeRange[] { new TimeRangeCore(false, query.LastUpdatedDate, null, true, false) }
                        : null;
                    var mappingSetEntity = info.MappingSet;
                    var clauses = GenerateSqlClauses(mappingSetEntity, timeRange, info.CurrentObservationAction, info.ShouldIncludeHistory);

                    AddClauses(info, clauses, whereBuilder);
                }

                // MAT-274
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Insert(0, "where ");
                }
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedWhereFormat1, whereBuilder));
            _logger.Info(Resources.InfoEndGenerateWhere);

            return whereBuilder.ToString();
        }


        private static void HandleComponentFilters(IList<ComponentFilter> filters, StringBuilder whereBuilder, DataRetrievalInfo info)
        {
            var builder = new ComponentFiltersSqlBuilder(filters, info.MappingSet.ComponentMappingBuilders);
            string sql = builder.GetSqlQuery();
            if (whereBuilder.Length > 0 && sql.Length > 0)
            {
                whereBuilder.Append(" AND ");
            }
            whereBuilder.Append(sql);
            info.Parameters.AddAll(builder.DbParameters);
        }

        /// <summary>
        /// Handles the selection groups.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="query">The query.</param>
        /// <param name="whereBuilder">The where builder.</param>
        /// <param name="dimId">The dimension identifier.</param>
        private static void HandleSelectionGroups(DataRetrievalInfo info, IDataQuery query, StringBuilder whereBuilder, string dimId)
        {
            IList<IDataQuerySelectionGroup> selGrps = query.SelectionGroups;
            foreach (IDataQuerySelectionGroup sg in selGrps)
            {
                // AND has precedence over OR
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Append(" OR ");
                }

                IList<string> freqs = new List<string>();
                if (sg.HasSelectionForConcept(dimId))
                {
                    IDataQuerySelection selConcept = sg.GetSelectionsForConcept(dimId);
                    if (selConcept.HasMultipleValues)
                    {
                        foreach (string val in selConcept.Values)
                        {
                            freqs.Add(val);
                        }
                    }
                    else
                    {
                        freqs.Add(selConcept.Value);
                    }
                }
                else
                {
                    // HACK FIXME TODO
                    freqs.Add(null);
                }

                string sqlWhere = string.Empty;
                var freqWhereBuilder = new StringBuilder();

                foreach (string freqVal in freqs)
                {
                    if (!string.IsNullOrEmpty(sqlWhere))
                    {
                        freqWhereBuilder.Append(" OR ");
                    }

                    sqlWhere = GenerateWhereClause(sg, info, freqVal);
                    freqWhereBuilder.Append(sqlWhere);
                }

                if (freqWhereBuilder.Length > 0)
                {
                    whereBuilder.Append(" ( ").Append(freqWhereBuilder).Append(" ) ");
                }

                MultiValueWhereBuilder multiValueWhereBuilder = new MultiValueWhereBuilder(info);

                foreach (IDataQuerySelection sel in sg.Selections)
                {
                    if (!string.IsNullOrEmpty(sqlWhere))
                    {
                        whereBuilder.Append(" AND ");
                    }

                    if (sel.HasMultipleValues)
                    {
                        sqlWhere = multiValueWhereBuilder.GenerateComponentWhere(
                            sel, 
                            sel.Values.Contains("") || info.CurrentObservationAction.EnumType == ObservationActionEnumType.Deleted || info.CurrentObservationAction.EnumType == ObservationActionEnumType.Updated
                        );
                        whereBuilder.Append(sqlWhere);
                    }
                    else
                    {
                        sqlWhere = GenerateComponentWhere(sel.ComponentId, sel.Value, info);
                        whereBuilder.Append(sqlWhere);
                    }
                }
            }
        }

        /// <summary>
        ///     Maps a component to one or more local columns and it's value to one or more local codes
        /// </summary>
        /// <param name="id">
        ///     The DSD Component identifier e.g. FREQ
        /// </param>
        /// <param name="conditionValue">
        ///     The DSD Component condition value (from the SDMX Query)
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval status
        /// </param>
        /// <returns>
        ///     An string containing the SQL query where condition
        /// </returns>
        private static string GenerateComponentWhere(string id, string conditionValue, DataRetrievalInfo info)
        {
            var ret = new StringBuilder();

            // MappingEntity mapping;
            // check if there is a mapping for this component
            if (id != null && conditionValue != null)
            {
                IComponentMappingBuilder componentMappingType;
                if (info.MappingSet.ComponentMappingBuilders.TryGetValue(id, out componentMappingType))
                {
                    var @operator = info.CurrentObservationAction.EnumType == ObservationActionEnumType.Deleted || info.CurrentObservationAction.EnumType == ObservationActionEnumType.Updated
                        ? OperatorType.NullOrExact
                        : OperatorType.Exact;

                    ret.Append(componentMappingType.GenerateComponentWhere(conditionValue, @operator));
                }
                else if (info.MeasureComponent == null || !id.Equals(info.MeasureComponent.Id))
                {
                    // component is not in the mapping
                    ret.Append(
                        string.Format(
                            CultureInfo.InvariantCulture,
                            " ('{0} is not mapped' = '{1}') ",
                            id.Replace("'", "''"),
                            conditionValue.Replace("'", "''")));
                }
            }

            return ret.ToString();
        }

        /// <summary>
        ///     Maps a component to one or more local columns and it's value to one or more local codes
        /// </summary>
        /// <param name="id">
        ///     The DSD Component identifier e.g. FREQ
        /// </param>
        /// <param name="conditionValue">
        ///     The DSD Component condition value (from the SDMX Query)
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval status
        /// </param>
        /// <returns>
        ///     An string containing the SQL query where condition
        /// </returns>
        private static string GenerateComponentWhere(
            string id,
            IComplexComponentValue conditionValue,
            DataRetrievalInfo info)
        {
            var ret = new StringBuilder();

            // MappingEntity mapping;
            // check if there is a mapping for this component
            if (id != null && conditionValue != null)
            {
                OperatorType operatorValue = OperatorType.Exact;
                if (conditionValue.OrderedOperator != null)
                {
                    operatorValue = GetSqlOrderedOperator(conditionValue.OrderedOperator);
                }
                else if (conditionValue.TextSearchOperator != null)
                {
                    operatorValue = GetSqlTextSearchOperator(conditionValue.TextSearchOperator);
                }

                IComponentMappingBuilder componentMappingType;
                if (info.MappingSet.ComponentMappingBuilders.TryGetValue(id, out componentMappingType))
                {
                    ret.Append(componentMappingType.GenerateComponentWhere(conditionValue.Value, operatorValue));
                }
                else if (info.MeasureComponent == null || !id.Equals(info.MeasureComponent.Id))
                {
                    // TODO check if the query is 2.1 or 2.0 and return value without executing query
                    // component is not in the mapping
                    ret.Append(" (1=0) ");
                    _logger.WarnFormat(CultureInfo.InvariantCulture, " {0} is not mapped. Queried with Value: '{1}' ", id, conditionValue);
                }
            }

            return ret.ToString();
        }

        /// <summary>
        ///     Generates SQL where clauses from <paramref name="time" />
        /// </summary>
        /// <param name="time">
        ///     The <see cref="IDataQuerySelectionGroup" /> containing the time.
        /// </param>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <param name="freqValue">
        ///     The frequency value
        /// </param>
        /// <returns>
        ///     The string containing the time part of the WHERE in an SQL query.
        /// </returns>
        private static string GenerateWhereClause(
            IDataQuerySelectionGroup time,
            DataRetrievalInfo info,
            string freqValue)
        {
            return info.TimeTranscoder != null
                       ? info.TimeTranscoder.GenerateWhere(time.DateFrom, time.DateTo, freqValue, info.CurrentObservationAction.EnumType == ObservationActionEnumType.Deleted || info.CurrentObservationAction.EnumType == ObservationActionEnumType.Updated)
                       : string.Empty;
        }

        /// <summary>
        ///     Generates SQL where clauses from <paramref name="time" />
        /// </summary>
        /// <param name="time">
        ///     The <see cref="IComplexDataQuerySelectionGroup" />
        /// </param>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <param name="freqValue">
        ///     The frequency value
        /// </param>
        /// <returns>
        ///     The string containing the time part of the WHERE in an SQL query.
        /// </returns>
        private static string GenerateWhereClause(
            IComplexDataQuerySelectionGroup time,
            DataRetrievalInfo info,
            string freqValue)
        {
            return info.TimeTranscoder != null
                       ? info.TimeTranscoder.GenerateWhere(time.DateFrom, time.DateTo, freqValue)
                       : string.Empty;
        }

        /// <summary>
        ///     Gets the component values operator.
        /// </summary>
        /// <param name="sel">The values selection of a component.</param>
        /// <returns>The operator to use for the values in <paramref name="sel" /></returns>
        private static string GetComponentValuesOperator(IComplexDataQuerySelection sel)
        {
            string componentValuesOperator = " OR ";

            var orderedOperatorEnumTypes =
                sel.Values.Where(value => value.OrderedOperator != null)
                    .Select(value => value.OrderedOperator.EnumType)
                    .Distinct()
                    .ToArray();
            if (orderedOperatorEnumTypes.Length == 1)
            {
                switch (orderedOperatorEnumTypes[0])
                {
                    case OrderedOperatorEnumType.NotEqual:
                        componentValuesOperator = " AND ";
                        break;
                }
            }
            else if (orderedOperatorEnumTypes.Length == 0)
            {
                var textOperatorEnumTypes =
                    sel.Values.Where(value => value.TextSearchOperator != null)
                        .Select(value => value.TextSearchOperator.EnumType)
                        .Distinct()
                        .ToArray();
                if (textOperatorEnumTypes.Length == 1)
                {
                    switch (textOperatorEnumTypes[0])
                    {
                        case TextSearchEnumType.NotEqual:
                            componentValuesOperator = " AND ";
                            break;
                    }
                }
            }

            return componentValuesOperator;
        }

        /// <summary>
        ///     Gets SQL operator from the given SDMX OrderedOperator
        /// </summary>
        /// <param name="ord">
        ///     The OrderedOperator
        /// </param>
        /// <returns>
        ///     An string containing the SQL query ordered operator
        /// </returns>
        private static OperatorType GetSqlOrderedOperator(OrderedOperator ord)
        {
            switch (ord.EnumType)
            {
                case OrderedOperatorEnumType.Equal:
                    return OperatorType.Exact;
                case OrderedOperatorEnumType.GreaterThan:
                    return OperatorType.GreaterThan;
                case OrderedOperatorEnumType.GreaterThanOrEqual:
                    return OperatorType.GreaterThanOrEqual;
                case OrderedOperatorEnumType.LessThan:
                    return OperatorType.LessThan;
                case OrderedOperatorEnumType.LessThanOrEqual:
                    return OperatorType.LessThanOrEqual;
                case OrderedOperatorEnumType.NotEqual:
                    return OperatorType.Not | OperatorType.Exact;
            }

            return OperatorType.Exact;
        }

        /// <summary>
        ///     Gets SQL operator from the given SDMX OrderedOperator
        /// </summary>
        /// <param name="ord">
        ///     The OrderedOperator
        /// </param>
        /// <returns>
        ///     An string containing the SQL query ordered operator
        /// </returns>
        private static OperatorType GetSqlTextSearchOperator(TextSearch ord)
        {
            switch (ord.EnumType)
            {
                case TextSearchEnumType.Contains:
                    return OperatorType.Contains;
                case TextSearchEnumType.DoesNotContain:
                    return OperatorType.Not | OperatorType.Contains;
                case TextSearchEnumType.DoesNotEndWith:
                    return OperatorType.Not | OperatorType.EndsWith;
                case TextSearchEnumType.DoesNotStartWith:
                    return OperatorType.Not | OperatorType.StartsWith;
                case TextSearchEnumType.EndsWith:
                    return OperatorType.EndsWith;
                case TextSearchEnumType.Equal:
                    return OperatorType.Exact;
                case TextSearchEnumType.NotEqual:
                    return OperatorType.Not | OperatorType.Exact;
                case TextSearchEnumType.StartsWith:
                    return OperatorType.StartsWith;
            }

            return OperatorType.Exact;
        }

        /// <summary>
        ///     Handles the groups.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="whereBuilder">The where builder.</param>
        /// <param name="sg">The string builder.</param>
        /// <param name="dimId">The dim identifier.</param>
        private static void HandleGroups(
            DataRetrievalInfo info,
            StringBuilder whereBuilder,
            IComplexDataQuerySelectionGroup sg,
            string dimId)
        {
            // AND has precedence over OR
            if (whereBuilder.Length > 0)
            {
                whereBuilder.Append(" OR ");
            }

            IList<string> freqs = new List<string>();
            if (sg.HasSelectionForConcept(dimId))
            {
                IComplexDataQuerySelection selConcept = sg.GetSelectionsForConcept(dimId);
                if (selConcept.HasMultipleValues())
                {
                    foreach (IComplexComponentValue val in selConcept.Values)
                    {
                        freqs.Add(val.Value);
                    }
                }
                else
                {
                    freqs.Add(selConcept.Value.Value);
                }
            }
            else
            {
                // HACK FIX ME TODO
                freqs.Add(null);
            }

            string sqlWhere = string.Empty;
            foreach (string freqVal in freqs)
            {
                if (!string.IsNullOrEmpty(sqlWhere))
                {
                    whereBuilder.Append(" OR ");
                }

                sqlWhere = GenerateWhereClause(sg, info, freqVal);
                whereBuilder.Append(sqlWhere);
            }

            if (sg.PrimaryMeasureValue != null && sg.PrimaryMeasureValue.Count > 0)
            {
                if (!string.IsNullOrEmpty(sqlWhere))
                {
                    whereBuilder.Append(" AND ");
                }

                foreach (IComplexComponentValue complexValue in sg.PrimaryMeasureValue)
                {
                    sqlWhere = GenerateComponentWhere(PrimaryMeasure.FixedId, complexValue, info);
                    whereBuilder.Append(sqlWhere);
                }
            }

            MultiValueWhereBuilder multiValueWhereBuilder = new MultiValueWhereBuilder(info);

            foreach (IComplexDataQuerySelection sel in sg.Selections)
            {
                if (!string.IsNullOrEmpty(sqlWhere))
                {
                    whereBuilder.Append(" AND ");
                }

                if (sel.HasMultipleValues() && QuerySelectionUtil.AreAllValuesEqual(sel))
                {
                    sqlWhere = multiValueWhereBuilder.GenerateComponentWhere(sel);
                    whereBuilder.AppendLine(sqlWhere);
                }
                else if (sel.HasMultipleValues())
                {
                    int contor = 0;

                    whereBuilder.Append("(");
                    var componentValuesOperator = GetComponentValuesOperator(sel);

                    foreach (IComplexComponentValue val in sel.Values)
                    {
                        if (contor > 0)
                        {
                            whereBuilder.Append(componentValuesOperator);
                        }

                        sqlWhere = GenerateComponentWhere(sel.ComponentId, val, info);
                        whereBuilder.Append(sqlWhere);
                        contor++;
                    }

                    whereBuilder.Append(")");
                }
                else
                {
                    sqlWhere = GenerateComponentWhere(sel.ComponentId, sel.Value, info);
                    whereBuilder.Append(sqlWhere);
                }
            }
        }

        /// <summary>
        /// Generates the last update clause.
        /// </summary>
        /// <param name="mappingSetEntity">The mapping set entity.</param>
        /// <param name="lastUpdatedDateTimeRange">The last updated date time range.</param>
        /// <returns>The <see cref="SqlClause"/> for Last Updated; otherwise null.</returns>
        private static SqlClause GenerateLastUpdateClause(IComponentMappingContainer mappingSetEntity, IList<ITimeRange> lastUpdatedDateTimeRange)
        {
            if (mappingSetEntity.LastUpdateMappingBuilder != null && ObjectUtil.ValidCollection(lastUpdatedDateTimeRange))
            {
                var generateWhere = mappingSetEntity.LastUpdateMappingBuilder.GenerateWhere(lastUpdatedDateTimeRange);
                return generateWhere;
            }

            return null;
        }

        /// <summary>
        /// Generates the update status clause.
        /// </summary>
        /// <param name="mappingSetEntity">The mapping set entity.</param>
        /// <param name="observationAction">The observation action.</param>
        /// <returns>The <see cref="SqlClause"/> for Update Status; otherwise null.</returns>
        private static SqlClause GenerateUpdateStatusClause(IComponentMappingContainer mappingSetEntity, ObservationAction observationAction)
        {
            if (mappingSetEntity.UpdateStatusMappingBuilder != null)
            {
                var generateWhere = mappingSetEntity.UpdateStatusMappingBuilder.GenerateWhere(observationAction);
                return generateWhere;
            }

            return null;
        }

        /// <summary>
        /// Generates the SQL clauses.
        /// </summary>
        /// <param name="mappingSetEntity">The mapping set entity.</param>
        /// <param name="lastUpdatedDateTimeRange">The last updated date time range.</param>
        /// <param name="observationAction">The observation action.</param>
        /// <param name="includeHistory"></param>
        /// <returns>
        /// The list of SQL clauses
        /// </returns>
        private static List<SqlClause> GenerateSqlClauses(IComponentMappingContainer mappingSetEntity, IList<ITimeRange> lastUpdatedDateTimeRange, ObservationAction observationAction, bool includeHistory)
        {
            var clauses = new List<SqlClause>();
            if (!includeHistory)
            {
                clauses.Add(GenerateUpdateStatusClause(mappingSetEntity, observationAction));
                if (mappingSetEntity.ValidToMappingBuilder != null)
                {
                    clauses.Add(mappingSetEntity.ValidToMappingBuilder.GenerateWhere());
                }
            }
            
            clauses.Add(GenerateLastUpdateClause(mappingSetEntity, lastUpdatedDateTimeRange));

            return clauses;
        }

        /// <summary>
        /// Adds the clauses.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="clauses">The clauses.</param>
        /// <param name="whereBuilder">The where builder.</param>
        private static void AddClauses(DataRetrievalInfo info, List<SqlClause> clauses, StringBuilder whereBuilder)
        {
            foreach (var sqlClause in clauses.Where(sqlClause => sqlClause != null))
            {
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.AppendLine(" AND ");
                }

                whereBuilder.Append(sqlClause.WhereClause);
                info.Parameters.AddAll(sqlClause.Parameters);
            }
        }

        private static void AddColumnName(List<string> orderColumns, DataSetColumnEntity column, string reservedKeywordToStringFormat)
        {
            var newColumnName = string.IsNullOrEmpty(reservedKeywordToStringFormat)
                ? column.Name
                : string.Format(reservedKeywordToStringFormat, column.Name);

            if (!orderColumns.Contains(newColumnName))
            {
                orderColumns.Add(newColumnName);
            }
        }

        private static string GetReservedKeywordToStringFormat(DatabaseSetting databaseSetting)
        {
            if (databaseSetting != null && !string.IsNullOrEmpty(databaseSetting.ReservedKeywordToStringFormat))
            {
                return databaseSetting.ReservedKeywordToStringFormat;
            }

            return null;
        }

        private static void HandleMetadataSelectionGroups(DataRetrievalInfo info, IDataQuery query, StringBuilder whereBuilder, string dimId)
        {
            if (query.SelectionGroups.Count == 0 &&
                (info.Settings.MetadataLevel == MetadataLevelEnumType.UpperOnly || info.Settings.MetadataLevel == MetadataLevelEnumType.CurrentOnly))
            {
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Append(" OR ");
                }

                string sqlWhere = string.Empty;

                foreach (var dimension in query.DataStructure.DimensionList.Dimensions.Where(x => !x.MeasureDimension))
                {
                    if (!string.IsNullOrEmpty(sqlWhere))
                    {
                        whereBuilder.Append(" AND ");
                    }

                    whereBuilder.Append(" ( ");
                    sqlWhere = dimension.TimeDimension
                        ? GenerateWhereClause(new DataQuerySelectionGroupImpl(null, new SdmxDateCore(DateTime.MaxValue.Date, TimeFormatEnumType.Date), null), info, string.Empty)
                        : GenerateComponentWhere(dimension.Id, _switchedOffDimensionValue, info);
                    whereBuilder.Append(sqlWhere);
                    whereBuilder.Append(" ) ");
                }

                return;
            }

            var restDataQueryV2 = (query as DataQueryV2Impl)?.DataQuery;

            foreach (var selectionGroup in query.SelectionGroups)
            {
                // AND has precedence over OR
                if (whereBuilder.Length > 0)
                {
                    whereBuilder.Append(" OR ");
                }

                IList<string> freqs = new List<string>();
                if (selectionGroup.HasSelectionForConcept(dimId))
                {
                    IDataQuerySelection selConcept = selectionGroup.GetSelectionsForConcept(dimId);
                    if (selConcept.HasMultipleValues)
                    {
                        foreach (string val in selConcept.Values)
                        {
                            freqs.Add(val);
                        }
                    }
                    else
                    {
                        freqs.Add(selConcept.Value);
                    }
                }
                else
                {
                    // HACK FIXME TODO
                    freqs.Add(null);
                }

                string sqlWhere = string.Empty;
                var freqWhereBuilder = new StringBuilder();

                foreach (string freqVal in freqs)
                {
                    if (!string.IsNullOrEmpty(sqlWhere))
                    {
                        freqWhereBuilder.Append(" OR ");
                    }

                    if ((info.Settings.MetadataLevel == MetadataLevelEnumType.UpperOnly || info.Settings.MetadataLevel == MetadataLevelEnumType.CurrentOnly) &&
                        selectionGroup.DateFrom == null && selectionGroup.DateTo == null)
                    {
                        sqlWhere = GenerateWhereClause(new DataQuerySelectionGroupImpl(null, new SdmxDateCore(DateTime.MaxValue.Date, TimeFormatEnumType.Date), null), info, freqVal);
                        freqWhereBuilder.Append(sqlWhere);
                    }
                    else
                    {
                        sqlWhere = GenerateWhereClause(selectionGroup, info, freqVal);
                        freqWhereBuilder.Append(sqlWhere);
                    }

                    if (info.Settings.MetadataLevel == MetadataLevelEnumType.CurrentOnly &&
                        (selectionGroup.DateFrom != null || selectionGroup.DateTo != null))
                    {
                        freqWhereBuilder.Append(" AND NOT ");
                        freqWhereBuilder.Append(GenerateWhereClause(new DataQuerySelectionGroupImpl(null, new SdmxDateCore(DateTime.MaxValue.Date, TimeFormatEnumType.Date), null), info, freqVal));
                    }
                }

                if (freqWhereBuilder.Length > 0)
                {
                    whereBuilder.Append(" ( ").Append(freqWhereBuilder).Append(" ) ");
                }

                MultiValueWhereBuilder multiValueWhereBuilder = new MultiValueWhereBuilder(info);

                foreach (var selection in selectionGroup.Selections)
                {
                    // Only generate where clause using dimension components in case of meta data query
                    if (!info.ComponentIdMap.TryGetValue(selection.ComponentId, out var component)
                        || component?.Dimension == null)
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(sqlWhere))
                    {
                        whereBuilder.Append(" AND ");
                    }

                    if (info.Settings.MetadataLevel != MetadataLevelEnumType.CurrentOnly)
                    {
                        selection.Values.Add(_switchedOffDimensionValue);
                    }
                    else
                    {
                        if (restDataQueryV2.QueryList.Count == 0 ||
                            restDataQueryV2.QueryList.All(x => x.All(string.IsNullOrEmpty)))
                        {
                            selection.Values.Add(_switchedOffDimensionValue);
                        }
                        else
                        {
                            selection.Values.Remove(_switchedOffDimensionValue);
                        }
                    }

                    sqlWhere = multiValueWhereBuilder.GenerateComponentWhere(selection, false);
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        continue;
                    }

                    whereBuilder.AppendFormat(" ( {0} ) ", sqlWhere);
                }

                if (info.Settings.MetadataLevel == MetadataLevelEnumType.UpperOnly || info.Settings.MetadataLevel == MetadataLevelEnumType.CurrentOnly)
                {
                    foreach (var dimension in query.DataStructure.DimensionList.Dimensions.Where(x => !selectionGroup.HasSelectionForConcept(x.Id) && !x.TimeDimension && !x.MeasureDimension))
                    {
                        if (!string.IsNullOrEmpty(sqlWhere))
                        {
                            whereBuilder.Append(" AND ");
                        }

                        whereBuilder.AppendFormat(" ( {0} ) ", GenerateComponentWhere(dimension.Id, _switchedOffDimensionValue, info));
                    }
                }
            }
        }
    }
}
