// -----------------------------------------------------------------------
// <copyright file="HeaderBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.SdmxXmlConstants;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;

    /// <summary>
    ///     Build a <see cref="IHeader" />
    /// </summary>
    internal class HeaderBuilder : IBuilder<IHeader, DataRetrievalInfo>
    {
        /// <summary>
        ///     The instance
        /// </summary>
        private static readonly HeaderBuilder _instance = new HeaderBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(HeaderBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="HeaderBuilder" /> class from being created.
        /// </summary>
        private HeaderBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance of this class
        /// </summary>
        public static HeaderBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     The method that builds a <see cref="IHeader" /> from the specified <paramref name="info" />
        /// </summary>
        /// <param name="info">
        ///     The current Data retrieval state
        /// </param>
        /// <returns>
        ///     The <see cref="IHeader" />
        /// </returns>
        public IHeader Build(DataRetrievalInfo info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            IHeader header;
            try
            {
                IHeaderRetrieverEngine headerRetrieverEngine = info.Settings.HeaderRetrieverEngine;
                header = headerRetrieverEngine?.GetHeader(info.BaseDataQuery, null, null);

                // was something returned?
                if (header == null)
                {
                    header = info.DefaultHeader;
                    if (string.IsNullOrEmpty(header.DatasetId))
                    {
                        header.DatasetId = info.BaseDataQuery.DataStructure.Id;
                    }
                    if(header.Id == HeaderId.Unset)
                    {
                        header.Id = HeaderId.Get(info.Settings.ConnectionStringSettings);
                    }
                    if (header != null)
                    {
                        _logger.Info(Resources.No_header_information_in_the_Mapping_Store);
                    }
                    else
                    {
                        throw new DataRetrieverException(
                            Resources.ErrorNoHeader, 
                            SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound));
                    }
                }
                else
                {
                    header.Id = HeaderId.Get(info.Settings.ConnectionStringSettings);
                    var datasetId = header.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetID));
                    header.DatasetId = string.IsNullOrWhiteSpace(datasetId) ? info.BaseDataQuery.DataStructure.Id : datasetId;
                    var datasetActionInDb = header.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAction));
                    if (string.IsNullOrWhiteSpace(datasetActionInDb) && !string.IsNullOrWhiteSpace(info.Settings.DatasetAction))
                    {
                        header.Action = DatasetAction.GetAction(info.Settings.DatasetAction);
                    }
                }

                // header.Truncated = info.IsTruncated;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), 
                    Resources.DataRetriever_GetMessageHeader_Error_populating_header);
            }

            return header;
        }
    }
}