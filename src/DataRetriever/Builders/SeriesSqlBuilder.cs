// -----------------------------------------------------------------------
// <copyright file="SeriesSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Globalization;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    ///     This class is responsible for building SQL Queries for Time Series.
    /// </summary>
    internal class SeriesSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesSqlBuilder _instance = new SeriesSqlBuilder();

        /// <summary>
        ///     The ordered component builder
        /// </summary>
        private static readonly SeriesOrderedDimensionBuilder _orderedComponentBuilder =
            new SeriesOrderedDimensionBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SeriesSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesSqlBuilder" /> class from being created.
        /// </summary>
        private SeriesSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            _logger.Info(Resources.InfoBeginGenerateSql);

            var seriesInfo = info as DataRetrievalInfoSeries;
            if (seriesInfo == null)
            {
                throw new ArgumentException("seriesInfo is not of DataRetrievalInfoSeries type");
            }

            var mappingSet = info.MappingSet;

            SqlQuery sqlQuery = new SqlQuery();
            string sql;

            try
            {
                // Generate Query subparts
                var mappingEntities = ConvertToMapping(seriesInfo.MainComponentMappings);
                if (seriesInfo.TimeMapping != null)
                {
                    mappingEntities.Add(seriesInfo.TimeMapping);
                }

                if (info.ShouldIncludeHistory)
                {
                    mappingEntities.Add(info.MappingSet.LastUpdateMapping);
                    mappingEntities.Add(info.MappingSet.UpdateStatusMapping);
                    mappingEntities.Add(info.MappingSet.ValidToMapping);
                }
                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                
                sql = GenerateSelect(false, mappingEntities, databaseSetting);

                var orderByClauseWithPagination = GenerateOrderByLocalColumns(seriesInfo);

                sqlQuery.AppendSql(sql);

                var datasetProperty = mappingSet.Dataset.DatasetPropertyEntity;

                var isCrossApplyApplicable = datasetProperty != null &&
                                             (datasetProperty.CrossApplySupported ?? false) &&
                                             seriesInfo.IsTimePeriodAtObservation &&
                                             !string.IsNullOrWhiteSpace(datasetProperty.CrossApplyColumn) && 
                                             ((info.HasFirstNObservations && info.FirstNObservations > 0) ||
                                              (info.HasLastNObservations && info.LastNObservations > 0));

                var fromClause = GenerateFrom(info);

                if (!isCrossApplyApplicable)
                {
                    sqlQuery.AppendSql(fromClause);
                    AppendCachedWhere(seriesInfo, sqlQuery);
                }
                else
                {
                    var sqlSelectSeries = $"SELECT distinct [{datasetProperty.CrossApplyColumn}] S_ID {fromClause}";
                    var sqlWhereClause = GenerateWhere(info);

                    // Series subquery of CROSS APPLY 
                    sqlQuery.AppendSql(" FROM ( ");
                    sqlQuery.AppendSql(sqlSelectSeries);
                    sqlQuery.AppendSql(sqlWhereClause);
                    sqlQuery.AppendSql(" ) series ");

                    // Observations subquery of CROSS APPLY 
                    sqlQuery.AppendSql(" CROSS APPLY ( ");
                    
                    if (info.HasFirstNObservations && info.FirstNObservations > 0)
                    {
                        GenerateCrossApplySubQuery(sqlQuery, seriesInfo, fromClause, sqlWhereClause,
                            datasetProperty.CrossApplyColumn, true);
                    }

                    if (info.HasFirstAndLastNObservations && info.FirstNObservations > 0 && info.LastNObservations > 0)
                    {
                        sqlQuery.AppendSql(" UNION ");
                    }

                    if (info.HasLastNObservations && info.LastNObservations > 0)
                    {
                        GenerateCrossApplySubQuery(sqlQuery, seriesInfo, fromClause, sqlWhereClause,
                            datasetProperty.CrossApplyColumn, false);
                    }

                    sqlQuery.AppendSql(" ) observations ");
                }

                sqlQuery.AppendSql(orderByClauseWithPagination);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxNoResultsException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), 
                    Resources.ErrorUnableToGenerateSQL);
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSql);

            info.SqlString = sqlQuery.GetSql();
        }

        /// <summary>
        ///     Creates an SQL statement using CROSS APPLY to improve performance of data queries with firstNObservations/lastNObservations parameter provided.
        /// </summary>
        /// <param name="sqlQuery">The SqlQuery object to be </param>
        /// <param name="seriesInfo">The current state of the data retrieval which contains the current query and mapping set</param>
        /// <param name="fromClause">The FROM clause fragment of the query</param>
        /// <param name="sqlWhereClause">The WHERE clause fragment of the query</param>
        /// <param name="crossApplyColumn">The column to be used in CROSS APPLY query</param>
        /// <param name="firstNObservation">Flag indicating that first or last N observations should be retrieved</param>
        private static void GenerateCrossApplySubQuery(SqlQuery sqlQuery, DataRetrievalInfoSeries seriesInfo, string fromClause, string sqlWhereClause, string crossApplyColumn, bool firstNObservation)
        {
            var topNRows = firstNObservation ? seriesInfo.FirstNObservations : seriesInfo.LastNObservations;

            var sqlSelectCrossApply = $"SELECT TOP({topNRows}) * {fromClause}";
            
            var orderByClause = GenerateOrderByLocalColumns(seriesInfo, false, !firstNObservation);

            sqlQuery.AppendSql(sqlSelectCrossApply);

            sqlQuery.AppendSql(sqlWhereClause);
            
            sqlQuery.AppendSql(string.IsNullOrEmpty(sqlWhereClause) ? " where " : " and ");
            sqlQuery.AppendSql($" series.S_ID = {crossApplyColumn} ");
            
            sqlQuery.AppendSql(orderByClause);
        }

        /// <summary>
        ///     Appends the cached where to <paramref name="sql" /> from <see cref="DataRetrievalInfoSeries.SqlWhereCache" /> if it
        ///     is not null or from <see cref="SqlBuilderBase.GenerateWhere" />
        /// </summary>
        /// <param name="info">The current DataRetrieval state</param>
        /// <param name="sql">The SQL.</param>
        private static void AppendCachedWhere(DataRetrievalInfoSeries info, SqlQuery sql)
        {
            if (string.IsNullOrEmpty(info.SqlWhereCache))
            {
                info.SqlWhereCache = GenerateWhere(info);
            }

            sql.AppendSql(info.SqlWhereCache);
        }

        /// <summary>
        ///     This method generates the ORDER BY part of the query
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <param name="withPagination">Controls if pagination clause should be added</param>
        /// <param name="enforceDescOrder">Flag to enforce generation order by clause for descending order</param>
        /// <returns>
        ///     The string containing the ORDER BY part of the query
        /// </returns>
        private static string GenerateOrderByLocalColumns(DataRetrievalInfo info, bool withPagination = true, bool enforceDescOrder = false)
        {
            var orderComponents = _orderedComponentBuilder.Build(info);

            var orderBy = withPagination ? GenerateOrderByWithPagination(info, orderComponents, enforceDescOrder) : GenerateOrderBy(info, orderComponents, enforceDescOrder);

            return orderBy;
        }
    }
}