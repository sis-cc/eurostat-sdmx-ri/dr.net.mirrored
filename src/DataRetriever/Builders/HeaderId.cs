﻿// -----------------------------------------------------------------------
// <copyright file="HeaderBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.MappingStore.Store.Extension;
using Estat.Sri.MappingStoreRetrieval.Manager;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.Threading;

namespace Estat.Nsi.DataRetriever.Builders
{
    /// <summary>
    /// Header id generator class
    /// </summary>
    public static class HeaderId
    {
        public static string Unset = "UNSET";
        private static int incrementalId = 1;
        private static object lockingObject = new object();
        private static int maxValue = 999999;
        private static bool useOldImplementation = false;

        /// <summary>
        /// Get header id
        /// </summary>
        /// <returns></returns>
        public static string Get(ConnectionStringSettings connectionStringSettings)
        {
            string id = string.Empty;
            var database = new Database(connectionStringSettings);
            lock (lockingObject)
            {
                try
                {
                    if (!useOldImplementation)
                    {
                        var idFromDb = database.ExecuteSequence("IREF_SEQ", maxValue);
                        id = "IREF" + idFromDb.PadLeft(6, '0');
                    }
                }
                catch (Exception)
                {
                    useOldImplementation = true;
                }
                if (useOldImplementation)
                {
                    id = "IREF" + incrementalId.ToString(CultureInfo.InvariantCulture).PadLeft(6, '0');
                    if (incrementalId <= maxValue)
                    {
                        incrementalId++;
                    }
                    else
                    {
                        incrementalId = 1;
                    }
                }
            }

            return id;
        }
    }
}