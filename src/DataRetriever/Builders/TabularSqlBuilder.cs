// -----------------------------------------------------------------------
// <copyright file="TabularSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Globalization;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     SQL builder which includes all components and orders by dimensions
    /// </summary>
    internal class TabularSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly TabularSqlBuilder _instance = new TabularSqlBuilder();

        /// <summary>
        ///     The ordered component builder
        /// </summary>
        private static readonly SeriesOrderedDimensionBuilder _orderedComponentBuilder =
            new SeriesOrderedDimensionBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(TabularSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="TabularSqlBuilder" /> class from being created.
        /// </summary>
        private TabularSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static TabularSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            _logger.Info(Resources.InfoBeginGenerateSql);

            SqlQuery sqlQuery = new SqlQuery();
            string sql;

            try
            {
                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                
                sql = GenerateSelect(false, info.ComponentMapping.Values, databaseSetting);
                sqlQuery.AppendSql(sql);

                sqlQuery.AppendSql(GenerateFrom(info));

                if (info.ComplexQuery != null)
                {
                    sqlQuery.AppendSql(GenerateComplexWhere(info));
                }
                else
                {
                    sqlQuery.AppendSql(GenerateWhere(info));
                }

                var orderComponents = _orderedComponentBuilder.Build(info);
                sqlQuery.AppendSql(GenerateOrderByWithPagination(info, orderComponents));
            }
            catch (DataRetrieverException dex)
            {
                throw new DataRetrieverException(dex, dex.SdmxErrorCode, dex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), 
                    Resources.ErrorUnableToGenerateSQL);
            }

            // log for easy debug
            _logger.Info(string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, sql));
            _logger.Info(Resources.InfoEndGenerateSql);

            info.SqlString = sqlQuery.GetSql();
        }
    }
}