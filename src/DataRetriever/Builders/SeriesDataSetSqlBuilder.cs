// -----------------------------------------------------------------------
// <copyright file="SeriesDataSetSqlBuilder.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Builders
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sri.Utils.Helper;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     This class is responsible for building SQL Queries for Time Series dataset attributes
    /// </summary>
    internal sealed class SeriesDataSetSqlBuilder : SqlBuilderBase, ISqlBuilder
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesDataSetSqlBuilder _instance = new SeriesDataSetSqlBuilder();

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SeriesDataSetSqlBuilder));

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesDataSetSqlBuilder" /> class from being created.
        /// </summary>
        private SeriesDataSetSqlBuilder()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesDataSetSqlBuilder Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        public void GenerateSql(DataRetrievalInfo info)
        {
            var seriesInfo = info as DataRetrievalInfoSeries;
            if (seriesInfo == null)
            {
                throw new ArgumentException("seriesInfo is not of DataRetrievalInfoSeries type");
            }

            GenerateDataSetSql(seriesInfo);
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <param name="info">
        ///     The current state of the data retrieval which contains the current query and mapping set
        /// </param>
        private void GenerateDataSetSql(DataRetrievalInfoSeries info)
        {
            if (!info.DataSetAttributes.Any(mapping => mapping.Mapping.GetColumns().Count > 0))
            {
                return;
            }

            _logger.Info(Resources.InfoBeginGenerateSql);

            SqlQuery sqlQuery = new SqlQuery();

            try
            {
                // Generate Query subparts
                var ddbType = info.MappingSet.DisseminationConnection.DbType;
                var provider = DatabaseType.GetProviderName(ddbType);
                var databaseSetting = DatabaseType.DatabaseSettings[provider];
                string sql = GenerateSelect(true, ConvertToMapping(info.DataSetAttributes), databaseSetting);
                sqlQuery.AppendSql(sql);

                sqlQuery.AppendSql(GenerateFrom(info));

                if (string.IsNullOrEmpty(info.SqlWhereCache))
                {
                    info.SqlWhereCache = GenerateWhere(info);
                }

                sqlQuery.AppendSql(info.SqlWhereCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), 
                    Resources.ErrorUnableToGenerateSQL);

                // ErrorTypes.QUERY_PARSING_ERROR, Resources.ErrorUnableToGenerateSQL, ex);
            }

            info.DataSetSqlString = sqlQuery.GetSql();

            // log for easy debug
            _logger.Info(
                string.Format(CultureInfo.InvariantCulture, Resources.InfoGeneratedSQLFormat1, info.DataSetSqlString));
            _logger.Info(Resources.InfoEndGenerateSql);
        }
    }
}