// -----------------------------------------------------------------------
// <copyright file="DataQueryEngineBase.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Org.Sdmxsource.Util;

namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Globalization;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.DataRetriever.Utils;

    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using System.Threading.Tasks;
    using System.Linq;
    using Estat.Sri.Mapping.Api.Constant;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Estat.Sri.Mapping.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Estat.Sri.DataRetriever.Model;

    /// <summary>
    ///     The base data query engine
    /// </summary>
    /// <typeparam name="TDataRetrievalInfo">The type of the data retrieval information.</typeparam>
    /// <typeparam name="TMappedValues">The type of the mapped values.</typeparam>
    internal abstract class DataQueryEngineBase<TDataRetrievalInfo, TMappedValues>
        where TDataRetrievalInfo : DataRetrievalInfo where TMappedValues : IMappedValues
    {
        /// <summary>
        ///     Handles the component mapping except for the TimeDimension when TRANSCODING is used
        /// </summary>
        /// <param name="reader">The IDataReader to read data from DDB</param>
        /// <param name="componentValues">The collection to store the component values</param>
        /// <param name="componentMappings">Component mappings list</param>
        /// <param name="info">The data retrieval information.</param>
        /// <returns>
        ///     True all components were mapped - false when an unmapped code was found
        /// </returns>
        protected static bool HandleComponentMapping(
            IDataReader reader,
            TMappedValues componentValues,
            IList<MappingInfo> componentMappings,
            TDataRetrievalInfo info)
        {
            var effectiveDimensionAtObservation = info.EffectiveDimensionAtObservation;
            var dimensionAtObservationMapping = info.DimensionAtObservationMapping;
            var dataStructureObject = info.BaseDataQuery.DataStructure;

            for (int index = 0; index < componentMappings.Count; index++)
            {
                var componentMapping = componentMappings[index];

                // first check if can map as array
                if (componentMapping.CanMapToArrayValue(out IComponentArrayMapper arrayMapper))
                {
                    var arrayVal = arrayMapper.MapComponentArray(reader);
                    if (arrayVal != null && arrayVal.Any())
                    {
                        componentValues.AddArray(index, arrayVal);
                        continue;
                    }
                    else
                    {
                        // the mapping of the code(s) was not possible
                        return false;
                    }
                }

                // else map as scalar/normal 
                var val = componentMapping.MappingBuilder.MapComponent(reader);
                if (val != null)
                {
                    componentValues.Add(index, val);
                    if (componentMapping.Component.Equals(dataStructureObject.FrequencyDimension))
                    {
                        componentValues.FrequencyValue = val;
                    }

                    if (componentMapping.MappingBuilder.Equals(dimensionAtObservationMapping)
                        || componentMapping.Component.Id.Equals(effectiveDimensionAtObservation))
                    {
                        componentValues.DimensionAtObservationValue = val;
                    }
                }
                else
                {
                    // SDMXRI-137
                    if (componentMapping.Attribute == null || componentMapping.Attribute.Mandatory)
                    {
                        return false;
                    }

                    componentValues.Add(index, string.Empty);
                }
            }

            if (reader != null)
            {
                var annotationContainer = LocalDataAnnotationScope.AnnotationContainer;
                if (annotationContainer != null)
                {
                    annotationContainer.SetValues(reader);
                }
            }

            if (info.ShouldIncludeHistory)
            {
                componentValues.HistoricMappedValues.UpdateFromReader(reader);
            }

            return true;
        }

        /// <summary>
        ///     Handles the component mappings
        /// </summary>
        /// <param name="reader">
        ///     The IDataReader to read data from DDB
        /// </param>
        /// <param name="info">
        ///     The info.
        /// </param>
        /// <param name="componentValues">
        ///     The collection to store the component values
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        /// <returns>
        ///     True all components were mapped - false when an unmapped code was found
        /// </returns>
        protected static bool HandleMappings(
            IDataReader reader, 
            TDataRetrievalInfo info, 
            TMappedValues componentValues, 
            IList<MappingInfo> mappings)
        {
            return HandleComponentMapping(reader, componentValues, mappings, info)
                   && HandleTimeDimensionMapping(reader, info, componentValues); // MAT-262
        }

        /// <summary>
        ///     Validates the maximum allowed observation.This is not related to (Complex)DataQuery First/Last observation or
        ///     Default
        ///     limit.
        ///     This is related to configurable allowed number of observations per request at server side.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="count">The count.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxResponseSizeExceedsLimitException">
        ///     Reached configured limit :
        ///     <see cref="DataRetrievalInfo.MaximumAllowedNumberObservations" /> observations
        /// </exception>
        protected static void ValidateMaxAllowedObservation(TDataRetrievalInfo info, int count)
        {
            if (info.MaximumAllowedNumberObservations > 0 && count >= info.MaximumAllowedNumberObservations)
            {
                throw new SdmxResponseSizeExceedsLimitException(
                    string.Format(
                        CultureInfo.InvariantCulture, 
                        "Reached configured limit : {0} observations", 
                        info.MaximumAllowedNumberObservations));
            }
        }

        /// <summary>
        ///     Create and return a <see cref="IMappedValues" /> implementation
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <returns>
        ///     a <see cref="IMappedValues" /> implementation
        /// </returns>
        protected abstract TMappedValues CreateMappedValues(TDataRetrievalInfo info, IDataReader reader);

        /// <summary>
        ///     Execute the <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="runAdditionalQueriesOnly">
        ///     Runs the additional queries only
        /// </param>
        protected void ExecuteDbCommand(TDataRetrievalInfo info, bool runAdditionalQueriesOnly = false)
        {
            // for PC-axis it doesn't support DbFactory.CreateCommand() and the enteprise libs seem to call it
            using (DbConnection connection = info.Settings.ConnectionBuilder.Build(info.MappingSet.DisseminationConnection))
            {
                // for pc-axis
                connection.Open();
                if (!info.ShouldIncludeHistory)
                {
                    this.RunAdditionalQueries(info, connection);
                }

                if (runAdditionalQueriesOnly)
                {
                    return;
                }

                using (DbCommand command = connection.CreateCommand(info.SqlString, info.Parameters))
                {
                    command.CommandTimeout = 0;
                    
                    using (new LocalDataAnnotationScope(info.MainComponentMappings, info.DimensionAtObservation ?? info.EffectiveDimensionAtObservation, info.Settings, info.TimeMapping))
                    using (IDataReader reader = command.ExecuteReaderAndLog())
                    {
                        var componentValues = this.CreateMappedValues(info, reader);
                        if (info.HasMaxObsPerSeries)
                        {
                            this.ReadDataMaxObsPerSeries(info, reader, componentValues, info.MainComponentMappings);
                        }
                        else if (info.Limit <= 0)
                        {
                            this.ReadData(info, reader, componentValues, info.MainComponentMappings);
                        }
                        else
                        {
                            this.ReadData(info, reader, componentValues, info.Limit, info.MainComponentMappings);
                        }

                        // Cancel any pending work of the IDataReader to avoid delays when closing it. 
                        // this requires an updated MySQL driver, 6.3.7 or later. See http://bugs.mysql.com/bug.php?id=60541
                        command.SafeCancel();
                    }
                }
            }
        }

        /// <summary>
        ///     Execute the <see cref="DataRetrievalInfo.SqlString" /> asynchronous
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="runAdditionalQueriesOnly">
        ///     Runs the additional queries only
        /// </param>
        protected async Task ExecuteDbCommandAsync(TDataRetrievalInfo info, bool runAdditionalQueriesOnly = false)
        {
            // for PC-axis it doesn't support DbFactory.CreateCommand() and the enteprise libs seem to call it
            using (DbConnection connection = info.Settings.ConnectionBuilder.Build(info.MappingSet.DisseminationConnection))
            using (DbCommand command = connection.CreateDisseminationDatabaseCommand(info.SqlString, info.Parameters))
            {
                if (command == null)
                {
                    return;
                }
                // for pc-axis
                await connection.OpenAsync();
                if (!info.ShouldIncludeHistory)
                {
                    this.RunAdditionalQueries(info, connection);
                }

                if (runAdditionalQueriesOnly)
                {
                    return;
                }
 
                command.CommandTimeout = 0;

                using (new LocalDataAnnotationScope(info.MainComponentMappings, info.DimensionAtObservation ?? info.EffectiveDimensionAtObservation, info.Settings, info.TimeMapping))
                using (var reader = await command.ExecuteReaderAsyncAndLog(CommandBehavior.Default).ConfigureAwait(false))
                {
                    var componentValues = this.CreateMappedValues(info, reader);
                    if (info.HasMaxObsPerSeries)
                    {
                        this.ReadDataMaxObsPerSeries(info, reader, componentValues, info.MainComponentMappings);
                    }
                    else if (info.Limit <= 0)
                    {
                        await this.ReadDataAsync(info, reader, componentValues, info.MainComponentMappings);
                    }
                    else
                    {
                        await this.ReadDataAsync(info, reader, componentValues, info.Limit, info.MainComponentMappings);
                    }

                    // Cancel any pending work of the IDataReader to avoid delays when closing it. 
                    // this requires an updated MySQL driver, 6.3.7 or later. See http://bugs.mysql.com/bug.php?id=60541
                    command.SafeCancel();
                }
                
            }
        }

        /// <summary>
        ///     Read data from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected virtual void ReadData(TDataRetrievalInfo info, IDataReader reader, TMappedValues componentValues, IList<MappingInfo> mappings)
        {
            int count = 0;
            long rowIndex = 0;

            //When no transcoding is defined, sql pagination is used, and the first row should start at the requested range, if given.
            bool hasTranscoding = info.ComponentMapping.Any(c => c.Value.HasTranscoding());
            if (info.Range?.Item1 != null && !hasTranscoding)
                rowIndex = info.Range.Item1.Value;

            while ((info.Range == null || rowIndex <= info.Range.Item2) && reader.Read())
            {
                if (info.Range == null || rowIndex >= info.Range.Item1)
                {
                    ValidateMaxAllowedObservation(info, count);

                    if (HandleMappings(reader, info, componentValues, mappings))
                    {
                        count += this.StoreResults(componentValues, info);
                    }

                    rowIndex++;
                }
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Read data asynchronous from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected virtual async Task ReadDataAsync(TDataRetrievalInfo info, DbDataReader reader, TMappedValues componentValues, IList<MappingInfo> mappings)
        {
            int count = 0;
            long rowIndex = 0;

            //When no transcoding is defined, sql pagination is used, and the first row should start at the requested range, if given.
            bool hasTranscoding = info.ComponentMapping.Any(c => c.Value.HasTranscoding());
            if (info.Range?.Item1 != null && !hasTranscoding)
                rowIndex = info.Range.Item1.Value;

            while ((info.Range == null || rowIndex <= info.Range.Item2) && await reader.ReadAsync(HeaderScope.RequestAborted))
            {
                if (info.Range == null || rowIndex >= info.Range.Item1)
                {
                    ValidateMaxAllowedObservation(info, count);
                    // this is where the componentValues get their row values
                    if (HandleMappings(reader, info, componentValues, mappings))
                    {
                        count += this.StoreResults(componentValues, info);
                        rowIndex++;
                    }
                }                     
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Read data from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected virtual void ReadData(TDataRetrievalInfo info, IDataReader reader, TMappedValues componentValues, int limit, IList<MappingInfo> mappings)
        {
            int count = 0;
            long rowIndex = 0;

            //When no transcoding is defined, sql pagination is used, and the first row should start at the requested range, if given.
            bool hasTranscoding = info.ComponentMapping.Any(c => c.Value.HasTranscoding());
            if (info.Range?.Item1 != null && !hasTranscoding)
                rowIndex = info.Range.Item1.Value;

            while ((info.Range == null || rowIndex <= info.Range.Item2) && reader.Read())
            {
                if (info.Range == null || rowIndex >= info.Range.Item1)
                {
                    ValidateMaxAllowedObservation(info, count);

                    if (HandleMappings(reader, info, componentValues, mappings))
                    {
                        count += this.StoreResults(componentValues, limit, info);
                    }
                }

                // we need to do the check in here to be sure that we reached the limit with accepted records. 
                // This was not important for SDMX v2.0 but it is for SDMX v2.1. In SDMX v2.1 we generate an error.
                // note that setting the limit to SQL query doesn't help because a row might be ignored (see below) or a row might have multiple observations
                if (count >= limit)
                {
                    // set the is truncated flag so we can determine 
                    info.IsTruncated = true;
                    break;
                }

                rowIndex++;
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Read data asynchronous from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected virtual async Task ReadDataAsync(TDataRetrievalInfo info, DbDataReader reader, TMappedValues componentValues, int limit, IList<MappingInfo> mappings)
        {
            int count = 0;
            long rowIndex = 0;

            //When no transcoding is defined, sql pagination is used, and the first row should start at the requested range, if given.
            bool hasTranscoding = info.ComponentMapping.Any(c => c.Value.HasTranscoding());
            if (info.Range?.Item1 != null && !hasTranscoding)
                rowIndex = info.Range.Item1.Value;

            while ((info.Range == null || rowIndex <= info.Range.Item2) && await reader.ReadAsync(HeaderScope.RequestAborted))
            {
                if (info.Range == null || rowIndex >= info.Range.Item1)
                {
                    ValidateMaxAllowedObservation(info, count);

                    if (HandleMappings(reader, info, componentValues, mappings))
                    {
                        count += this.StoreResults(componentValues, limit, info);
                    }
                }

                // we need to do the check in here to be sure that we reached the limit with accepted records. 
                // This was not important for SDMX v2.0 but it is for SDMX v2.1. In SDMX v2.1 we generate an error.
                // note that setting the limit to SQL query doesn't help because a row might be ignored (see below) or a row might have multiple observations
                if (count >= limit)
                {
                    // set the is truncated flag so we can determine 
                    info.IsTruncated = true;
                    break;
                }

                rowIndex++;
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Reads the data with max observation per series.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="reader">The reader.</param>
        /// <param name="componentValues">The component values.</param>
        /// <param name="mappings">The mappings.</param>
        protected virtual void ReadDataMaxObsPerSeries(TDataRetrievalInfo info, IDataReader reader, TMappedValues componentValues, IList<MappingInfo> mappings)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Execute any additional queries if overridden in a subclass. The base method does nothing
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="connection">
        ///     The <see cref="DbConnection" /> to the DDB
        /// </param>
        protected virtual void RunAdditionalQueries(TDataRetrievalInfo info, DbConnection connection)
        {
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected abstract int StoreResults(TMappedValues componentValues, TDataRetrievalInfo info);

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="limit">
        ///     The limit.
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected abstract int StoreResults(TMappedValues componentValues, int limit, TDataRetrievalInfo info);

        /// <summary>
        ///     Handles the time dimension mapping when transcoding is used
        /// </summary>
        /// <param name="reader">
        ///     The IDataReader to read data from DDB
        /// </param>
        /// <param name="info">
        ///     The info.
        /// </param>
        /// <param name="mappedValues">
        ///     The dictionary to store the time dimension value
        /// </param>
        /// <returns>
        ///     True all components were mapped - false when an unmapped code was found
        /// </returns>
        protected static bool HandleTimeDimensionMapping(
            IDataReader reader, 
            TDataRetrievalInfo info, 
            TMappedValues mappedValues)
        {
            if (info.TimeMapping != null)
            {
                string val = info.TimeTranscoder.MapComponent(reader, mappedValues.FrequencyValue);
                if (val != null)
                {
                    mappedValues.TimeValue = val;
                    if (info.IsTimePeriodAtObservation)
                    {
                        mappedValues.DimensionAtObservationValue = val;
                    }
                }
                else
                {
                    return false; // null value found at time dimension
                }
            }

            return true;
        }
    }
}