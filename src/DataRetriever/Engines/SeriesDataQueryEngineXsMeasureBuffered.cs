// -----------------------------------------------------------------------
// <copyright file="SeriesDataQueryEngineXsMeasureBuffered.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Common;
using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.DataRetriever.Utils;
    using Estat.Sri.DataRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

    /// <summary>
    ///     The series data query engine XS measure.
    /// </summary>
    internal class SeriesDataQueryEngineXsMeasureBuffered : SeriesDataQueryEngineBase, 
                                                            IDataQueryEngine<DataRetrievalInfoSeries>
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesDataQueryEngineXsMeasureBuffered _instance =
            new SeriesDataQueryEngineXsMeasureBuffered();

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesDataQueryEngineXsMeasureBuffered" /> class from being created.
        /// </summary>
        private SeriesDataQueryEngineXsMeasureBuffered()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesDataQueryEngineXsMeasureBuffered Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        public void ExecuteSqlQuery(DataRetrievalInfoSeries info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.BuildXSMeasures();
            this.ExecuteDbCommand(info);
        }

        /// <summary>
        ///     This method executes an SQL query asynchronous on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        public async Task ExecuteSqlQueryAsync(DataRetrievalInfoSeries info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.BuildXSMeasures();
            await this.ExecuteDbCommandAsync(info);
        }
        /// <summary>
        ///     Read data from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override void ReadData(DataRetrievalInfoSeries info, IDataReader reader, AdvancedMappedValues componentValues, int limit, IList<MappingInfo> mappings)
        {
            base.ReadData(info, reader, componentValues, limit, mappings);
            if (componentValues != null)
            {
                WriteXsMeasureCache(info, componentValues.XSMeasureCaches);
            }
        }


        /// <summary>
        ///     Read data asynchronous from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override async Task ReadDataAsync(DataRetrievalInfoSeries info, DbDataReader reader, AdvancedMappedValues componentValues, int limit, IList<MappingInfo> mappings)
        {
            await base.ReadDataAsync(info, reader, componentValues, limit, mappings);
            if (componentValues != null)
            {
                WriteXsMeasureCache(info, componentValues.XSMeasureCaches);
            }
        }

        /// <summary>
        ///     Read data from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override void ReadData(DataRetrievalInfoSeries info, IDataReader reader, AdvancedMappedValues componentValues, IList<MappingInfo> mappings)
        {
            base.ReadData(info, reader, componentValues, mappings);
            if (componentValues != null)
            {
                WriteXsMeasureCache(info, componentValues.XSMeasureCaches);
            }
        }

        /// <summary>
        ///     Read data asynchronous from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override async Task ReadDataAsync(DataRetrievalInfoSeries info, DbDataReader reader, AdvancedMappedValues componentValues, IList<MappingInfo> mappings)
        {
            await base.ReadDataAsync(info, reader, componentValues, mappings);
            if (componentValues != null)
            {
                WriteXsMeasureCache(info, componentValues.XSMeasureCaches);
            }
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected override int StoreResults(AdvancedMappedValues componentValues, DataRetrievalInfoSeries info)
        {
            var row = componentValues;
            if (row == null)
            {
                throw new ArgumentException("mappedValues not of AdvancedMappedValues type");
            }

            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            int maxMeasures = info.CrossSectionalMeasureMappings.Count;
            var count = WriteXsMeasures(info, maxMeasures, row);

            return count;
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="limit">
        ///     The limit.
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected override int StoreResults(AdvancedMappedValues componentValues, int limit, DataRetrievalInfoSeries info)
        {
            if (componentValues == null)
            {
                throw new ArgumentException("mappedValues not of AdvancedMappedValues type");
            }

            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            int maxMeasures = Math.Min(info.CrossSectionalMeasureMappings.Count, limit);
            int count = WriteXsMeasures(info, maxMeasures, componentValues);
            return count;
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="groupInformation">
        ///     The current group related information
        /// </param>
        /// <param name="row">
        ///     The map between components and their values
        /// </param>
        protected override void StoreTimeSeriesGroupResults(
            DataRetrievalInfoSeries info, 
            GroupInformation groupInformation, 
            AdvancedMappedValues row)
        {
            if (groupInformation.MeasureComponent == null)
            {
                base.StoreTimeSeriesGroupResults(info, groupInformation, row);
                return;
            }

            IAnnotation[] annotations = LocalDataAnnotationScope.GroupAnnotations;
            foreach (var measureDimensionQueryValue in info.CrossSectionalMeasureMappings)
            {
                info.SeriesWriter.StartGroup(groupInformation.ThisGroup.Id, annotations);

                // write group dimensions
                foreach (var dimensionValue in row.DimensionValues)
                {
                    info.SeriesWriter.WriteGroupKeyValue(dimensionValue.Key.Id, dimensionValue.Value);
                }

                var xsComponent = measureDimensionQueryValue.CrossSectionalMeasure;
                Debug.Assert(xsComponent != null, "Expected cross sectional measure but got " + measureDimensionQueryValue.Component);

                info.SeriesWriter.WriteGroupKeyValue(
                    groupInformation.MeasureComponent.Id, 
                    xsComponent.Code);

                // write group attributes
                WriteScalarAttributes(row.AttributeGroupValues, info);
            }
        }

        /// <summary>
        ///     Write the cached <paramref name="xsMeasures" /> .
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval info
        /// </param>
        /// <param name="xsMeasures">
        ///     The XS measures.
        /// </param>
        private static void WriteXsMeasureCache(
            DataRetrievalInfoSeries info, 
            IEnumerable<KeyValuePair<string, XsMeasureCache>> xsMeasures)
        {
            var annotations = GetSeriesAnnotations();
            foreach (var xsMeasure in xsMeasures)
            {
                // start series
                info.SeriesWriter.StartSeries(annotations);
                var xsMeasureCache = xsMeasure.Value;
                foreach (var dimensionValue in xsMeasureCache.CachedSeriesKey)
                {
                    info.SeriesWriter.WriteSeriesKeyValue(dimensionValue.Key.Id, dimensionValue.Value);
                }

                info.SeriesWriter.WriteSeriesKeyValue(info.MeasureComponent.Id, xsMeasureCache.XSMeasureCode);

                WriteScalarAttributes(xsMeasureCache.CachedSeriesAttributes, info);

                for (int index = 0; index < xsMeasureCache.XSMeasureCachedObservations.Count; index++)
                {
                    var cachedObservation = xsMeasureCache.XSMeasureCachedObservations[index];
                    var attributes = xsMeasureCache.Attributes[index];

                    info.SeriesWriter.WriteObservation(cachedObservation.Key, cachedObservation.Value, GetObsAnnotations());
                    WriteScalarAttributes(attributes, info);
                }
            }
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="maxMeasures">
        ///     The max number of measures to write
        /// </param>
        /// <param name="row">
        ///     The map between components and their values
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        private static int WriteXsMeasures(DataRetrievalInfoSeries info, int maxMeasures, AdvancedMappedValues row)
        {
            int count = 0;

            if (row.IsNewKey())
            {
                TryWriteDataSet(row, info);
                WriteXsMeasureCache(info, row.XSMeasureCaches);

                row.InitXsBuffer();
            }

            for (int i = 0; i < maxMeasures; i++)
            {
                var crossSectionalMeasureMapping = info.CrossSectionalMeasureMappings[i];
                var xsComponent = crossSectionalMeasureMapping.CrossSectionalMeasure;
                row.AddToBuffer(xsComponent);
                count++;
            }

            return count;
        }
    }
}