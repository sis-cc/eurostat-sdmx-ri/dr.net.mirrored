﻿// -----------------------------------------------------------------------
// <copyright file="CrossSectionalMeasuresDataQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Globalization;

    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    ///     Dissemination Data Query engine for XS DSD with XS Measures and Measure Dimension/Primary Measure mapped.
    /// </summary>
    internal class CrossSectionalMeasuresDataQueryEngine : CrossSectionalDataQueryEngineBase, 
                                                           IDataQueryEngine<DataRetrievalInfoXS>
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly CrossSectionalMeasuresDataQueryEngine _instance =
            new CrossSectionalMeasuresDataQueryEngine();

        /// <summary>
        ///     Prevents a default instance of the <see cref="CrossSectionalMeasuresDataQueryEngine" /> class from being created.
        /// </summary>
        private CrossSectionalMeasuresDataQueryEngine()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static CrossSectionalMeasuresDataQueryEngine Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     Write a primary measure observation
        /// </summary>
        /// <param name="row">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored.
        /// </returns>
        protected override int WriteObservation(MappedXsValues row, DataRetrievalInfoXS info)
        {
            if (row.MeasureDimensionValue == null)
            {
                throw new InvalidOperationException("Measure Dimension is null");
            }

            string tag;
            if (info.XSMeasureCodeToConcept.TryGetValue(row.MeasureDimensionValue.Value, out tag))
            {
                return WriteObservation(row, tag, row.PrimaryMeasureValue.Value, row.ObservationLevelAttributeValues, info);
            }

            throw new InvalidOperationException(
                string.Format(CultureInfo.InvariantCulture, "Unknown measure code {0}", row.MeasureDimensionValue.Value));
        }
    }
}