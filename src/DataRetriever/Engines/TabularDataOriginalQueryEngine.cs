﻿// -----------------------------------------------------------------------
// <copyright file="TabularDataOriginalQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Data;

    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    ///     The tabular data original query engine.
    /// </summary>
    internal class TabularDataOriginalQueryEngine : TabularDataQueryEngineBase, 
                                                    IDataQueryEngine<DataRetrievalInfoTabular>
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly TabularDataOriginalQueryEngine _instance = new TabularDataOriginalQueryEngine();

        /// <summary>
        ///     Prevents a default instance of the <see cref="TabularDataOriginalQueryEngine" /> class from being created.
        /// </summary>
        private TabularDataOriginalQueryEngine()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static TabularDataOriginalQueryEngine Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes it to the writer in
        ///     <see cref="DataRetrievalInfo" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        /// </exception>
        public void ExecuteSqlQuery(DataRetrievalInfoTabular info)
        {
            this.ExecuteDbCommand(info);
        }

        /// <summary>
        ///     This method executes an SQL query asynchronous on the dissemination database and writes it to the writer in
        ///     <see cref="DataRetrievalInfo" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        /// </exception>
        public async Task ExecuteSqlQueryAsync(DataRetrievalInfoTabular info)
        {
            await this.ExecuteDbCommandAsync(info);
        }
        /// <summary>
        ///     Create and return a <see cref="IMappedValues" /> implementation
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <returns>
        ///     a <see cref="IMappedValues" /> implementation
        /// </returns>
        protected override MappedValuesFlat CreateMappedValues(DataRetrievalInfoTabular info, IDataReader reader)
        {
            return new MappedValuesFlat(reader, info);
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        ///     -or-
        ///     <paramref name="componentValues" />
        ///     not
        ///     <see cref="MappedValuesFlat" />
        ///     type
        /// </exception>
        protected override int StoreResults(MappedValuesFlat componentValues, DataRetrievalInfoTabular info)
        {
            var mappedValues = componentValues;
            if (mappedValues == null)
            {
                throw new ArgumentException(Resources.ErrorComponentValuesNotMappedValuesFlaType);
            }

            var tabularInfo = info;
            if (tabularInfo == null)
            {
                throw new ArgumentException(Resources.ErrorInfoNotDataRetrievalInfoTabularType);
            }

            this.WriteData(mappedValues, tabularInfo);

            return 1;
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="limit">
        ///     The limit.
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        ///     -or-
        ///     <paramref name="componentValues" />
        ///     not
        ///     <see cref="MappedValuesFlat" />
        ///     type
        /// </exception>
        protected override int StoreResults(MappedValuesFlat componentValues, int limit, DataRetrievalInfoTabular info)
        {
            return this.StoreResults(componentValues, info);
        }

        /// <summary>
        ///     Write the columns
        /// </summary>
        /// <param name="mappedValues">
        ///     The mapped components and their values
        /// </param>
        /// <param name="tabularInfo">
        ///     The current data retrieval state
        /// </param>
        protected override void WriteColumns(MappedValuesFlat mappedValues, DataRetrievalInfoTabular tabularInfo)
        {
            base.WriteColumns(mappedValues, tabularInfo);

            foreach (var localColumn in mappedValues.GetLocalColumns())
            {
                tabularInfo.TabularWriter.WriteColumnAttribute(localColumn);
            }
        }

        /// <summary>
        ///     Write data to <see cref="DataRetrievalInfoTabular.TabularWriter" /> from the specified
        ///     <paramref name="tabularInfo" />
        /// </summary>
        /// <param name="mappedValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="tabularInfo">
        ///     The current Data Retrieval state
        /// </param>
        protected override void WriteData(MappedValuesFlat mappedValues, DataRetrievalInfoTabular tabularInfo)
        {
            base.WriteData(mappedValues, tabularInfo);

            foreach (var localColumn in mappedValues.GetLocalValues())
            {
                tabularInfo.TabularWriter.WriteCellAttributeValue(localColumn.Value);
            }
        }
    }
}