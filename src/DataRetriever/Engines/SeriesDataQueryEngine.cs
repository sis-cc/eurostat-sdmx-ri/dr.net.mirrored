// -----------------------------------------------------------------------
// <copyright file="SeriesDataQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;

namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.DataRetriever.Utils;
    using Estat.Sri.DataRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     The data query engine.
    /// </summary>
    internal class SeriesDataQueryEngine : SeriesDataQueryEngineBase, IDataQueryEngine<DataRetrievalInfoSeries>
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SeriesDataQueryEngine _instance = new SeriesDataQueryEngine();

        /// <summary>
        ///     Prevents a default instance of the <see cref="SeriesDataQueryEngine" /> class from being created.
        /// </summary>
        private SeriesDataQueryEngine()
        {
        }

        /// <summary>
        ///     Gets the singleton instance
        /// </summary>
        public static SeriesDataQueryEngine Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        public void ExecuteSqlQuery(DataRetrievalInfoSeries info)
        {
            this.ExecuteDbCommand(info, info.WriteMetadataAttributesOnly);
        }

        /// <summary>
        ///     This method executes an SQL query asynchronous on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        public async Task ExecuteSqlQueryAsync(DataRetrievalInfoSeries info)
        {
            await this.ExecuteDbCommandAsync(info, info.WriteMetadataAttributesOnly);
        }
        /// <summary>
        ///     Reads the data with max observation per series.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <param name="reader">The reader.</param>
        /// <param name="componentValues">The component values.</param>
        /// <param name="mappings">The mappings.</param>
        protected override void ReadDataMaxObsPerSeries(DataRetrievalInfoSeries info, IDataReader reader, AdvancedMappedValues componentValues, IList<MappingInfo> mappings)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            int count = 0;
            Predicate<int> isBelowLimit;
            var limit = info.Limit;
            if (limit > 0)
            {
                isBelowLimit = i => i < limit;
            }
            else
            {
                isBelowLimit = i => true;
            }

            using (var maxObsStatus = new MaxObsStatus(info, WriteObservation))
            {
                if (reader == null)
                {
                    throw new ArgumentNullException("reader");
                }

                while (!info.IsTruncated && reader.Read())
                {
                    if (HandleMappings(reader, info, componentValues, mappings))
                    {
                        ValidateMaxAllowedObservation(info, count);
                        if (!isBelowLimit(count))
                        {
                            info.IsTruncated = true;
                        }
                        else
                        {
                            count += this.StoreResultsMaxObs(componentValues, info, maxObsStatus);
                        }
                    }
                }
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected override int StoreResults(AdvancedMappedValues componentValues, DataRetrievalInfoSeries info)
        {
            var row = componentValues;
            if (row == null)
            {
                throw new ArgumentException("mappedValues not of AdvancedMappedValues type");
            }

            if (info.ShouldIncludeHistory)
            {
                var action = string.Equals(componentValues.HistoricMappedValues.UpdateStatus.Value, "Deleted", StringComparison.OrdinalIgnoreCase) 
                    ? "Delete" 
                    : "Replace";

                var newHistoricValues = row.NewHistoricValues(action);
                
                if (newHistoricValues)
                {
                    var validFrom = action != "Delete" && !string.IsNullOrWhiteSpace(componentValues.HistoricMappedValues.ValidFrom.Value) 
                        ? DateTime.Parse(componentValues.HistoricMappedValues.ValidFrom.Value) 
                        : DateTime.MinValue;

                    var validTo = action == "Delete" && !string.IsNullOrWhiteSpace(componentValues.HistoricMappedValues.ValidTo.Value) 
                        ? DateTime.Parse(componentValues.HistoricMappedValues.ValidTo.Value) 
                        : DateTime.MinValue;

                    var datasetHeader = new DatasetHeaderCore(
                        info.DatasetId,
                        DatasetAction.GetAction(action),
                        new DatasetStructureReferenceCore(string.Empty, info.BaseDataQuery.DataStructure.AsReference, null, null, info.BaseDataQuery.DimensionAtObservation),
                        null,
                        DateTime.MinValue,
                        DateTime.MinValue,
                        validFrom,
                        validTo,
                        -1,
                        null,
                        null);

                    info.SeriesWriter.StartDataset(info.BaseDataQuery.Dataflow, info.BaseDataQuery.DataStructure, datasetHeader, null);
                    WriteScalarAttributes(componentValues.AttributeDataSetValues, info);
                    WriteArrayAttributes(componentValues.AttributeDataSetArrayValues, info);
                }

                if (row.IsNewKey())
                {
                    WriteSeries(info, row);
                }
            }
            else if(info.CurrentObservationAction.EnumType == ObservationActionEnumType.Deleted)
            {
                if (row.AttributeDataSetValues.Any() && row.IsDatasetAttributesDifferent())
                {
                    if (row.StartedDataSet)
                    {
                        info.SeriesWriter.StartDataset(info.BaseDataQuery.Dataflow, info.BaseDataQuery.DataStructure, info.DatasetHeader, null);
                    }
                    
                    WriteScalarAttributes(componentValues.AttributeDataSetValues, info);
                    WriteArrayAttributes(componentValues.AttributeDataSetArrayValues, info);

                    row.StartedDataSet = true;
                }
                
                WriteSeries(info, row);
            }
            else if (row.IsNewKey())
            {
                TryWriteDataSet(row, info);
                WriteSeries(info, row);
            }
            return WriteObservation(info,componentValues);
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="limit">
        ///     The limit.
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state 
        /// </param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        protected override int StoreResults(AdvancedMappedValues componentValues, int limit, DataRetrievalInfoSeries info)
        {
            return this.StoreResults(componentValues, info);
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store.
        /// </summary>
        /// <param name="componentValues">The map between components and their values</param>
        /// <param name="info">The current Data Retrieval state</param>
        /// <param name="maxObsStatus">The max observation status.</param>
        /// <returns>
        ///     The number of observations stored
        /// </returns>
        /// <exception cref="System.NotImplementedException">This method needs to be implemented.</exception>
        protected virtual int StoreResultsMaxObs(
            AdvancedMappedValues componentValues, 
            DataRetrievalInfoSeries info, 
            MaxObsStatus maxObsStatus)
        {
            var row = componentValues;
            if (row == null)
            {
                throw new ArgumentException("mappedValues not of AdvancedMappedValues type");
            }

            // TODO change to Generic.
            var seriesInfo = info;

            if (row.IsNewKey())
            {
                maxObsStatus.ResetCount();
                TryWriteDataSet(row, seriesInfo);
                WriteSeries(seriesInfo, row);
            }

            return
                maxObsStatus.Add(componentValues);
        }
    }
}