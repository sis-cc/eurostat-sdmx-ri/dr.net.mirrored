// -----------------------------------------------------------------------
// <copyright file="TabularDataQueryEngineBase.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Data.Common;
using System.Threading.Tasks;
using Estat.Sri.DataRetriever;

namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    ///     The base class for Tabular output <see cref="IDataQueryEngine{T}" /> implementations.
    /// </summary>
    internal abstract class TabularDataQueryEngineBase : DataQueryEngineBase<DataRetrievalInfoTabular, MappedValuesFlat>
    {
        /// <summary>
        ///     Read data from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override void ReadData(DataRetrievalInfoTabular info, IDataReader reader, MappedValuesFlat componentValues, IList<MappingInfo> mappings)
        {
            this.WriteColumns(info, componentValues, mappings);
            base.ReadData(info, reader, componentValues, mappings);
        }

        /// <summary>
        ///     Read data asynchronous from DDB, perform mapping and transcoding and store it in the writer specified
        ///     <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override async Task ReadDataAsync(DataRetrievalInfoTabular info, DbDataReader reader, MappedValuesFlat componentValues, IList<MappingInfo> mappings)
        {
            this.WriteColumns(info, componentValues, mappings);
            await base.ReadDataAsync(info, reader, componentValues, mappings);
        }

        /// <summary>
        ///     Read data from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override void ReadData(DataRetrievalInfoTabular info, IDataReader reader, MappedValuesFlat componentValues, int limit, IList<MappingInfo> mappings)
        {
            this.WriteColumns(info, componentValues, mappings);
            base.ReadData(info, reader, componentValues, limit, mappings);
        }

        /// <summary>
        ///     Read data asynchronous from DDB up the specified number of observations, perform mapping and transcoding and store it in the
        ///     writer specified <see cref="DataRetrievalInfo" />
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="limit">
        ///     The maximum number of observations, should be greater than 0
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        protected override async Task ReadDataAsync(DataRetrievalInfoTabular info, DbDataReader reader, MappedValuesFlat componentValues, int limit, IList<MappingInfo> mappings)
        {
            this.WriteColumns(info, componentValues, mappings);
            await base.ReadDataAsync(info, reader, componentValues, limit, mappings);
        }
        /// <summary>
        ///     Write the columns
        /// </summary>
        /// <param name="mappedValues">
        ///     The mapped components and their values
        /// </param>
        /// <param name="tabularInfo">
        ///     The current data retrieval state
        /// </param>
        protected virtual void WriteColumns(MappedValuesFlat mappedValues, DataRetrievalInfoTabular tabularInfo)
        {
            tabularInfo.TabularWriter.StartColumns();
            foreach (var dimensionValue in mappedValues.DimensionValues)
            {
                tabularInfo.TabularWriter.WriteColumnKey(dimensionValue.Key.Id);
            }

            var timeDimension = tabularInfo.BaseDataQuery.DataStructure.TimeDimension;
            if (timeDimension != null)
            {
                tabularInfo.TabularWriter.WriteColumnKey(timeDimension.Id);
            }

            foreach (var measureValue in mappedValues.MeasureValues)
            {
                tabularInfo.TabularWriter.WriteColumnMeasure(measureValue.Key.Id);
            }

            foreach (var attributeValue in mappedValues.AttributeValues)
            {
                tabularInfo.TabularWriter.WriteColumnAttribute(attributeValue.Key.Id);
            }
        }

        /// <summary>
        ///     Write data to <see cref="DataRetrievalInfoTabular.TabularWriter" /> from the specified
        ///     <paramref name="tabularInfo" />
        /// </summary>
        /// <param name="mappedValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="tabularInfo">
        ///     The current Data Retrieval state
        /// </param>
        protected virtual void WriteData(MappedValuesFlat mappedValues, DataRetrievalInfoTabular tabularInfo)
        {
            tabularInfo.TabularWriter.StartRecord();
            foreach (var dimensionValue in mappedValues.DimensionValues)
            {
                tabularInfo.TabularWriter.WriteCellKeyValue(dimensionValue.Value);
            }

            if (tabularInfo.BaseDataQuery.DataStructure.TimeDimension != null)
            {
                tabularInfo.TabularWriter.WriteCellKeyValue(mappedValues.TimeValue);
            }

            foreach (var measureValue in mappedValues.MeasureValues)
            {
                tabularInfo.TabularWriter.WriteCellMeasureValue(measureValue.Value);
            }

            foreach (var attributeValue in mappedValues.AttributeValues)
            {
                tabularInfo.TabularWriter.WriteCellAttributeValue(attributeValue.Value);
            }
        }

        /// <summary>
        ///     Write the columns
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="componentValues">
        ///     The component Values.
        /// </param>
        /// <param name="mappings">
        ///     The collection of component mappings
        /// </param>
        /// <exception cref="ArgumentException">
        ///     <paramref name="info" />
        ///     not
        ///     <see cref="DataRetrievalInfoTabular" />
        ///     type
        ///     -or-
        ///     <paramref name="componentValues" />
        ///     not
        ///     <see cref="MappedValuesFlat" />
        ///     type
        /// </exception>
        private void WriteColumns(
            DataRetrievalInfoTabular info, 
            MappedValuesFlat componentValues, 
            IList<MappingInfo> mappings)
        {
            if (componentValues == null)
            {
                throw new ArgumentException(Resources.ErrorComponentValuesNotMappedValuesFlaType, "componentValues");
            }

            var tabularInfo = info;
            if (tabularInfo == null)
            {
                throw new ArgumentException(Resources.ErrorInfoNotDataRetrievalInfoTabularType, "info");
            }

            for (int i = 0; i < mappings.Count; i++)
            {
                componentValues.Add(i, null);
            }

            this.WriteColumns(componentValues, tabularInfo);
        }
    }
}