// -----------------------------------------------------------------------
// <copyright file="SeriesDataQueryEngineBase.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Engines
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.DataRetriever.Utils;
    using Estat.Sri.DataRetriever;
    using Estat.Sri.DataRetriever.Model;
    using Estat.Sri.MappingStoreRetrieval.Extensions;

    using log4net;
    using Newtonsoft.Json.Linq;
    using Org.Sdmx.Resources.SdmxMl.Schemas.V21.Query;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

    /// <summary>
    ///     The base class for DataQueryEngine used for Time series output
    /// </summary>
    internal abstract class SeriesDataQueryEngineBase : DataQueryEngineBase<DataRetrievalInfoSeries, AdvancedMappedValues>
    {
        /// <summary>
        ///     The _logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SeriesDataQueryEngineBase));

        /// <summary>
        ///     Write the SDMX compliant values for dataset attached attributes to the
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> if <see cref="AdvancedMappedValues.StartedDataSet" /> is true
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        protected static void TryWriteDataSet(AdvancedMappedValues componentValues, DataRetrievalInfoSeries info)
        {
            if (!componentValues.StartedDataSet)
            {
                WriteDataSetResults(componentValues, info);
                componentValues.StartedDataSet = true;
            }
        }

        /// <summary>
        /// Writes the scalar type measures. 
        /// These are writen inside the Obs xml element and must be written first.
        /// </summary>
        /// <param name="measures">The array measures.</param>
        /// <param name="info"></param>
        protected static void WriteScalarMeasures(IEnumerable<ComponentValue> measures, DataRetrievalInfoSeries info)
        {
            foreach (var measure in measures)
            {
                info.SeriesWriter.WriteMeasureValue(measure.Key.Id, measure.Value);
            }
        }

        /// <summary>
        /// Writes the array type measures.
        /// These are written as separate xml elements and must be written last.
        /// </summary>
        /// <param name="measures">The array measures.</param>
        /// <param name="info"></param>
        protected static void WriteArrayMeasures(IEnumerable<ComponentValue> measures, DataRetrievalInfoSeries info)
        {
            foreach (var measure in measures)
            {
                info.SeriesWriter.WriteComplexMeasureValue(BuildArrayValues(measure));
            }
        }

        /// <summary>
        /// The common writer flow for measures.
        /// The actual write method must be provided.
        /// </summary>
        /// <param name="measures">The measures to write.</param>
        /// <param name="info"></param>
        /// <param name="writeMethod">The action that writes each measure.</param>
        protected static void WriteMeasures(IEnumerable<ComponentValue> measures, DataRetrievalInfoSeries info,
            Action<ComponentValue> writeMethod)
        {     
            if (info.BaseDataQuery.Measures == MeasuresEnumType.None)
            {
                return;
            }

            bool isSpecific = info.BaseDataQuery.Measures == MeasuresEnumType.Specific;
            IList<string> specificIds = info.BaseDataQuery.SpecificMeasures;

            // write  measures
            foreach (var keyValuePair in measures)
            {
                var Msr = (IMeasure)keyValuePair.Key;

                // write specifc attributes
                if (isSpecific && !specificIds.Contains(Msr.Id))
                {
                    continue;
                }

                writeMethod(keyValuePair);
            }
        }

        /// <summary>
        /// Writes the array type attributes. 
        /// These are written as separate xml elements and must be written last.
        /// </summary>
        /// <param name="attributes">All the attributes. The array ones are distinguished from the method itself.</param>
        /// <param name="info"></param>
        protected static void WriteArrayAttributes(IEnumerable<ComponentValue> attributes, DataRetrievalInfoSeries info)
        {
            foreach (var attribute in attributes)
            {
                var componentEntity = (IAttributeObject)attribute.Key;
                // SODIHD-1272 write optional attribute only if it is not empty.
                if (componentEntity.Mandatory || attribute.ArrayValue.Any())
                {
                    info.SeriesWriter.WriteComplexAttributeValue(BuildArrayValues(attribute));
                }
            }
        }

        private static KeyValueImpl BuildArrayValues(ComponentValue arrayComponent)
        {
            List<IComplexNodeValue> complexNodes = new();
            foreach (string arrayValue in arrayComponent.ArrayValue)
            {
                complexNodes.Add(new ComplexNodeValue(arrayValue));
            }
            return new KeyValueImpl(arrayComponent.Key.Id, complexNodes);
        }

        /// <summary>
        ///     Writes the attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <param name="info">The info.</param>
        protected static void WriteScalarAttributes(IEnumerable<ComponentValue> attributes, DataRetrievalInfoSeries info)
        {
            foreach (var keyValuePair in attributes)
            {
                var componentEntity = (IAttributeObject)keyValuePair.Key;
                var value = keyValuePair.Value;

                // SODIHD-1272 write optional attribute only if it is not empty.
                if (componentEntity.Mandatory || !string.IsNullOrEmpty(value))
                {
                    info.SeriesWriter.WriteAttributeValue(componentEntity.Id, value);
                }
            }
        }

        /// <summary>
        ///     Write observation element.
        /// </summary>
        /// <param name="info">The current Data Retrieval state</param>
        /// <param name="dimensionAtObsValue">The dimension at observation value.</param>
        /// <param name="value">The observation value</param>
        /// <param name="attributeObservationValues">The attribute observation values.</param>
        /// <returns>
        ///     The number of observations stored. Is is always 1
        /// </returns>
        protected static int WriteObservation(
            DataRetrievalInfoSeries info,
            AdvancedMappedValues componentValues)
        {
            if (!info.WriteObservations)
            {
                return 1;
            }

            if (info.Settings.SdmxSchemaVersion == SdmxSchemaEnumType.VersionThree)
            {
                // write dimension at observation and obs value
                info.SeriesWriter.WriteObservation(componentValues.DimensionAtObservationValue, GetObsAnnotations());

                // write non-array measures and attributes
                WriteScalarMeasures(componentValues.MeasureValues, info);
                WriteScalarAttributes(componentValues.AttributeObservationValues, info);

                // write array measures and attributes
                WriteArrayMeasures(componentValues.MeasureArrayValues, info);
                WriteArrayAttributes(componentValues.AttributeObservationArrayValues, info);
            }
            else
            {
                // write dimension at observation and obs value
                
                if (componentValues.PrimaryMeasureValue == null)
                {
                    _logger.Error("Primary measure is null for non-SDMX 3.0.0");
                    _logger.ErrorFormat("Dataflow: {0}, {1}", info.BaseDataQuery.Dataflow, info.Settings.SdmxSchemaVersion);
                }
                if (info.IsAllDimensions)
                {
                    info.SeriesWriter.WriteObservation(info.EffectiveDimensionAtObservation, componentValues.DimensionAtObservationValue, componentValues.PrimaryMeasureValue?.Value, GetObsAnnotations());
                }
                else
                {
                    info.SeriesWriter.WriteObservation(componentValues.DimensionAtObservationValue, componentValues.PrimaryMeasureValue?.Value, GetObsAnnotations());
                }

                // write observation attributes
                WriteScalarAttributes(componentValues.AttributeObservationValues, info);
            }

            return 1;
        }

        /// <summary>
        ///     Write a series element.
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="row">
        ///     The map between components and their values
        /// </param>
        protected static void WriteSeries(DataRetrievalInfoSeries info, AdvancedMappedValues row)
        {
            // start series
            var dataWriterEngine = info.SeriesWriter;
            var annotations = GetSeriesAnnotations();

            dataWriterEngine.StartSeries(annotations);

            var dimensionAtObservation = info.DimensionAtObservation;
            var effectiveDimensionAtObservation = info.EffectiveDimensionAtObservation;
            var isAllDimensions = info.IsAllDimensions;

            // write dimensions
            for (int index = 0; index < row.DimensionValues.Count; index++)
            {
                var dimensionValue = row.DimensionValues[index];
                var id = dimensionValue.Key.Id;
                if (!id.Equals(dimensionAtObservation)
                    && (!isAllDimensions || !id.Equals(effectiveDimensionAtObservation)))
                {
                    dataWriterEngine.WriteSeriesKeyValue(id, dimensionValue.Value);
                }
            }

            // write time period if it is not the dimension at observation
            if (!info.IsTimePeriodAtObservation && info.TimeTranscoder != null ||
                info.Groups.Any(x => x.Value.ThisGroup.DimensionRefs.Contains(DimensionObject.TimeDimensionFixedId)))
            {
                var id = info.BaseDataQuery.DataStructure.TimeDimension.Id;
                if (!isAllDimensions || !id.Equals(effectiveDimensionAtObservation))
                {
                    dataWriterEngine.WriteSeriesKeyValue(id, row.TimeValue);
                }
            }

            // write series attributes
            WriteScalarAttributes(row.AttributeSeriesValues, info);
            WriteArrayAttributes(row.AttributeSeriesArrayValues, info);
        }

        /// <summary>
        ///     Create and return a <see cref="IMappedValues" /> implementation
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="reader">
        ///     The <see cref="IDataReader" /> to read data from DDB
        /// </param>
        /// <returns>
        ///     a <see cref="IMappedValues" /> implementation
        /// </returns>
        protected override AdvancedMappedValues CreateMappedValues(DataRetrievalInfoSeries info, IDataReader reader)
        {
            if (info == null)
            {
                throw new ArgumentException(Resources.ErrorTypeNotDataRetrievalInfoSeries, "info");
            }

            return new AdvancedMappedValues(info) { StartedDataSet = info.DataSetAttributes.Count > 0 };
        }

        /// <summary>
        ///     Execute any additional queries if overridden in a subclass. This override executes queries for DataSet and Groups
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="connection">
        ///     The <see cref="DbConnection" /> to the DDB
        /// </param>
        protected override void RunAdditionalQueries(DataRetrievalInfoSeries info, DbConnection connection)
        {
            if (info == null)
            {
                throw new ArgumentException(Resources.ErrorTypeNotDataRetrievalInfoSeries, "info");
            }

            // handle dataset attributes
            if (info.WriteAttributes && info.DataSetAttributes.Count > 0)
            {
                ExecuteDataSetAttributeSqlQuery(info, connection);
            }

            if (info.WriteAttributes)
            {
                // check if there are groups and mapped group attributes
                foreach (var groupInformation in info.Groups)
                {
                    this.ExecuteGroupSqlQuery(groupInformation.Value, info, connection);
                }
            }

            if (info.WriteMetadata)
            {
                this.ExecuteMetadataSqlQuery(info, connection);
            }
        }

        /// <summary>
        ///     Store the SDMX compliant data for each component entity in the store
        /// </summary>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        /// <param name="groupInformation">
        ///     The current group related information
        /// </param>
        /// <param name="row">
        ///     The map between components and their values
        /// </param>
        protected virtual void StoreTimeSeriesGroupResults(
            DataRetrievalInfoSeries info, 
            GroupInformation groupInformation, 
            AdvancedMappedValues row)
        {
            IAnnotation[] annotations = LocalDataAnnotationScope.GroupAnnotations;

            info.SeriesWriter.StartGroup(groupInformation.ThisGroup.Id, annotations);

            // write group dimensions
            foreach (var dimensionValue in row.DimensionValues)
            {
                info.SeriesWriter.WriteGroupKeyValue(dimensionValue.Key.Id, dimensionValue.Value);
            }

            if (groupInformation.ThisGroup.DimensionRefs.Contains(DimensionObject.TimeDimensionFixedId))
            {
                info.SeriesWriter.WriteGroupKeyValue(DimensionObject.TimeDimensionFixedId, row.TimeValue);
            }

            // write group attributes
            WriteScalarAttributes(row.AttributeGroupValues, info);
            WriteArrayAttributes(row.AttributeGroupArrayValues, info);
        }

        /// <summary>
        /// Gets the series annotations.
        /// </summary>
        /// <returns></returns>
        protected static IAnnotation[] GetSeriesAnnotations()
        {
            return LocalDataAnnotationScope.SeriesAnnotations;
        }
        
        /// <summary>
        /// Gets the series annotations.
        /// </summary>
        /// <returns></returns>
        protected static IAnnotation[] GetObsAnnotations()
        {
            return LocalDataAnnotationScope.ObsAnnotations;
        }

        /// <summary>
        ///     This method executes an SQL query for retrieving the dataset attributes on the dissemination database and writes it
        ///     to <see cref="DataRetrievalInfoSeries.SeriesWriter" />
        /// </summary>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        /// <param name="connection">
        ///     The connection to the dissemination database
        /// </param>
        private static void ExecuteDataSetAttributeSqlQuery(DataRetrievalInfoSeries info, DbConnection connection)
        {
            if (string.IsNullOrEmpty(info.DataSetSqlString))
            {
                // handle case where all dataset attributes are constants.
                if (info.DataSetAttributes.All(mapping => !string.IsNullOrWhiteSpace(mapping.Mapping.ConstantValue)))
                {
                    var componentValues = new AdvancedMappedValues(info, info.DataSetAttributes);
                    if (HandleComponentMapping(null, componentValues, info.DataSetAttributes, info))
                    {
                        WriteDataSetResults(componentValues, info);
                    }
                }
                else
                {
                    Debug.Assert(
                        info.DataSetAttributes.Count == 0, 
                        "No dataset SQL string generated and there are non constant dataset attributes");
                }

                return;
            }

            using (DbCommand command = connection.CreateDisseminationDatabaseCommand(info.DataSetSqlString, info.Parameters))
            {
                if(command == null)
                {
                    return;
                }
                // for pc-axis
                command.CommandTimeout = 0;
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (new LocalDataAnnotationScope(info.DataSetAttributes, info.Settings, true))
                using (IDataReader reader = command.ExecuteReaderAndLog())
                {
                    stopwatch.Stop();
                    _logger.Info(
                        string.Format(
                            CultureInfo.InvariantCulture, 
                            "Execute DataSet Attribute SQL Reader: {0}", 
                            stopwatch.Elapsed));
                    bool found = false;

                    while (!found && reader.Read())
                    {
                        var componentValues = new AdvancedMappedValues(info, info.DataSetAttributes);
                        if (HandleComponentMapping(reader, componentValues, info.DataSetAttributes, info))
                        {
                            WriteDataSetResults(componentValues, info);
                            found = true;
                            
                        }
                    }

                    // To avoid populating the IDataReader.RecordsAffected on IDataReader close which can take some time on some cases (typec)
                    // this requires an updated MySQL driver, 6.3.7 or later. See http://bugs.mysql.com/bug.php?id=60541
                    command.SafeCancel();
                }
            }
        }

        /// <summary>
        ///     Check if the specified <paramref name="targetGroup" /> contains the <paramref name="componentValues" />
        /// </summary>
        /// <param name="targetGroup">
        ///     The target group
        /// </param>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <param name="componentValues">
        ///     The component values
        /// </param>
        /// <returns>
        ///     A value indicating whether the key is already processed or not.
        /// </returns>
        private static bool ProcessedKeySet(
            GroupInformation targetGroup, 
            DataRetrievalInfoSeries info, 
            AdvancedMappedValues componentValues)
        {
            var current = new ReadOnlyKey(componentValues, info.GroupNameTable);

            if (!targetGroup.KeySet.ContainsKey(current))
            {
                targetGroup.KeySet.Add(current, null);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Write the SDMX compliant values for dataset attached attributes to the
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" />
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        private static void WriteDataSetResults(AdvancedMappedValues componentValues, DataRetrievalInfoSeries info)
        {
            WriteScalarAttributes(componentValues.AttributeDataSetValues, info);
            WriteArrayAttributes(componentValues.AttributeDataSetArrayValues, info);
        }

        /// <summary>
        ///     This method executes an SQL query for a group on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" /> The SQL query is located inside
        ///     <paramref name="groupInformation" /> at <see cref="GroupInformation.SQL" /> . The group information is located in
        ///     <paramref name="groupInformation" />
        /// </summary>
        /// <param name="groupInformation">
        ///     The current Time Series Group information
        /// </param>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        /// <param name="connection">
        ///     The connection to the dissemination database
        /// </param>
        private void ExecuteGroupSqlQuery(
            GroupInformation groupInformation, 
            DataRetrievalInfoSeries info, 
            DbConnection connection)
        {
            if (string.IsNullOrEmpty(groupInformation.SQL))
            {
                return;
            }
            int count = 0;

            using (DbCommand command = connection.CreateDisseminationDatabaseCommand(groupInformation.SQL, info.Parameters))
            {
                if(command == null)
                {
                    return;
                }
                command.CommandTimeout = 0;
                var stopwatch = new Stopwatch();
                stopwatch.Start();
                using (new LocalDataAnnotationScope(groupInformation.ComponentMappings, info.Settings, false))
                using (IDataReader reader = command.ExecuteReaderAndLog())
                {
                    stopwatch.Stop();
                    _logger.Info(
                        string.Format(CultureInfo.InvariantCulture, "Execute Group SQL Reader: {0}", stopwatch.Elapsed));

                    while (reader.Read())
                    {
                        var componentValues = new AdvancedMappedValues(info, groupInformation.ComponentMappings);

                        if (groupInformation.ThisGroup.DimensionRefs.Contains(DimensionObject.TimeDimensionFixedId))
                        {
                            HandleTimeDimensionMapping(reader, info, componentValues);
                        }

                        if (HandleComponentMapping(reader, componentValues, groupInformation.ComponentMappings, info)
                            && !ProcessedKeySet(groupInformation, info, componentValues))
                        {
                            this.StoreTimeSeriesGroupResults(info, groupInformation, componentValues);
                            count++;
                        }
                    }
                }
            }
            info.RecordsRead = count;
        }

        /// <summary>
        ///     This method executes an SQL query for metadata attributes on the dissemination database and writes it to
        ///     <see cref="DataRetrievalInfoSeries.SeriesWriter" />
        /// </summary>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        /// <param name="connection">
        ///     The connection to the dissemination database
        /// </param>
        private void ExecuteMetadataSqlQuery(DataRetrievalInfoSeries info, DbConnection connection)
        {
            // Only write actual metadata values if WriteMetadataAttributes == true, otherwise only the availability information using annotation
            var sqlString = info.WriteMetadataAttributes ? info.MetadataSqlString : info.MetadataAvailabilitySqlString;

            if (string.IsNullOrEmpty(sqlString))
            {
                return;
            }

            int count = 0;

            using (DbCommand command = connection.CreateDisseminationDatabaseCommand(sqlString, info.Parameters))
            {
                if (command == null)
                {
                    return;
                }

                command.CommandTimeout = 0;
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                var mappingInfoList = new List<MappingInfo>(info.MainComponentMappings.Where(x => x.Dimension != null));

                if (info.WriteMetadataAttributes)
                {
                    mappingInfoList.AddRange(info.MetadataAttributes);
                }

                using (new LocalDataAnnotationScope(mappingInfoList, info.Settings, false))
                {
                    using (IDataReader reader = command.ExecuteReaderAndLog())
                    {
                        stopwatch.Stop();
                        _logger.Info(string.Format(CultureInfo.InvariantCulture, "Execute Group SQL Reader: {0}", stopwatch.Elapsed));

                        while (reader.Read())
                        {
                            var componentValues = new AdvancedMappedValues(info, mappingInfoList);

                            if (HandleComponentMapping(reader, componentValues, mappingInfoList, info) && HandleTimeDimensionMapping(reader, info, componentValues))
                            {
                                WriteMetadataResults(componentValues, info);
                                count++;
                            }
                        }
                    }
                }
            }

            info.RecordsRead = count;
        }

        /// <summary>
        ///     Write the Metadata.
        /// </summary>
        /// <param name="componentValues">
        ///     The map between components and their values
        /// </param>
        /// <param name="info">
        ///     The current Data Retrieval state
        /// </param>
        private static void WriteMetadataResults(AdvancedMappedValues componentValues, DataRetrievalInfoSeries info)
        {
            var dataWriterEngine = info.SeriesWriter;

            dataWriterEngine.StartGroup("dimensionGroup", !info.WriteMetadataAttributes ? GetAvailabilityAnnotation().ToArray() : null);

            foreach (var componentValue in componentValues.DimensionValues.Where(x => !string.IsNullOrEmpty(x.Value)))
            {
                dataWriterEngine.WriteGroupKeyValue(componentValue.Key.Id, componentValue.Value);
            }

            if (componentValues.TimeDimensionValue != null && !string.IsNullOrEmpty(componentValues.TimeDimensionValue.Value))
            {
                dataWriterEngine.WriteGroupKeyValue(componentValues.TimeDimensionValue.Key.Id, componentValues.TimeDimensionValue.Value);
            }

            if (info.WriteMetadataAttributes)
            {
                foreach (var componentValue in componentValues.AttributeMetadataValues.Where(x => !string.IsNullOrEmpty(x.Value)))
                {
                    dataWriterEngine.WriteAttributeValue(componentValue.Key.GetFullIdPath(false), componentValue.Value);
                }
            }
        }

        private static List<IAnnotation> GetAvailabilityAnnotation()
        {
            return new List<IAnnotation>()
            {
                new AnnotationObjectCore(new Org.Sdmx.Resources.SdmxMl.Schemas.V10.Common.AnnotationType()
                    {
                        AnnotationType1 = "HAS_METADATA"
                    },
                    null)
            };
        }
    }
}