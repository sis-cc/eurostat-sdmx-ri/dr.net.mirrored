// -----------------------------------------------------------------------
// <copyright file="IDataQueryEngine.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Nsi.DataRetriever.Engines
{
    using Estat.Nsi.DataRetriever.Model;

    /// <summary>
    ///     The i data query engine.
    /// </summary>
    /// <typeparam name="T">The DR state type.</typeparam>
    internal interface IDataQueryEngine<in T>
        where T : DataRetrievalInfo
    {
        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes it to the writer in
        ///     <see cref="DataRetrievalInfo" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        void ExecuteSqlQuery(T info);

        /// <summary>
        ///     This method executes an SQL query asynchronous on the dissemination database and writes it to the writer in
        ///     <see cref="DataRetrievalInfo" /> . The SQL query is located inside <paramref name="info" /> at
        ///     <see cref="DataRetrievalInfo.SqlString" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="info" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="info">
        ///     The current DataRetrieval state
        /// </param>
        Task ExecuteSqlQueryAsync(T info);
    }
}