// -----------------------------------------------------------------------
// <copyright file="DataRetrieverCore.cs" company="EUROSTAT">
//   Date Created : 2009-10-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Nsi.DataRetriever
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Estat.Nsi.DataRetriever.Builders;
    using Estat.Nsi.DataRetriever.Engines;
    using Estat.Nsi.DataRetriever.Manager;
    using Estat.Nsi.DataRetriever.Model;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.DataRetriever;
    using Estat.Sri.DataRetriever.Utils;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Exceptions;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval;
    using Estat.Sri.SdmxXmlConstants;
    using Estat.Sri.TabularWriters.Engine;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     This is the main class of the "Data Retriever" building block as identified in Eurostat's "SDMX Reference
    ///     Architecture". It can be used with any "Mapping Store" complying with the database design specified there.
    /// </summary>
    /// <example>
    ///     Code example for using Data Retriever and the Stream CompactWriter from SDMX IO in C#
    ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetriever.cs" lang="cs">
    /// </code>
    /// </example>
    //// TODO refactor this
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Exists for historic reasons. Scheduled for refactoring in 2017.")]
    public class DataRetrieverCore : IDataRetrieverTabular, ISdmxDataRetrievalWithWriter, ISdmxDataRetrievalWithCrossWriter, IAdvancedSdmxDataRetrievalWithWriter
    {
        #region Static Fields

        /// <summary>
        ///     The Header builder
        /// </summary>
        private static readonly HeaderBuilder _headerBuilder = HeaderBuilder.Instance;

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DataRetrieverCore));

        /// <summary>
        ///     The SQL Builder instance. This is responsible for building/generating the various SQL queries for DDB for
        ///     TimeSeries
        ///     data
        /// </summary>
        private static readonly ISqlBuilder _sqlBuilder = SeriesSqlBuilder.Instance;

        /// <summary>
        ///     The SQL Builder instance. This is responsible for building/generating the various SQL queries for DDB for
        ///     CrossSectional data
        /// </summary>
        private static readonly ISqlBuilder _sqlBuilderTabular = TabularSqlBuilder.Instance;

        /// <summary>
        ///     The SQL Builder instance. This is responsible for building/generating the various SQL queries for DDB for
        ///     CrossSectional data
        /// </summary>
        private static readonly ISqlBuilder _sqlComplexBuilder = ComplexSqlBuilder.Instance;

        /// <summary>
        ///     The SQL Builder instance. This is responsible for building/generating the various SQL queries for DDB for
        ///     CrossSectional data
        /// </summary>
        private static readonly ISqlBuilder _sqlXsBuilder = CrossSectionalSqlBuilder.Instance;
        #endregion

        #region Fields

        /// <summary>
        /// The data retriever settings
        /// </summary>
        private readonly DataRetrieverSettings _dataRetrieverSettings;

        /// <summary>
        ///     This <see cref="IHeader" /> stores the default header
        /// </summary>
        private readonly IHeader _defaultHeader;

        /// <summary>
        ///     The _max number observations
        /// </summary>
        private readonly int _maxNumberObservations;

        /// <summary>
        /// The action builder
        /// </summary>
        private readonly ActionBuilder _actionBuilder = new ActionBuilder();

        /// <summary>
        /// The dataset action builder
        /// </summary>
        private readonly IBuilder<DatasetAction, ObservationAction> _datasetActionBuilder = new DataSetActionBuilder();

        /// <summary>
        /// The component mapping manager
        /// </summary>
        private readonly IComponentMappingManager _componentMappingManager;

        /// <summary>
        /// The component mapping validation manager
        /// </summary>
        private readonly IComponentMappingValidationManager _componentMappingValidationManager;

        /// <summary>
        /// The store identifier
        /// </summary>
        private readonly string _storeId;

        /// <summary>
        /// The annotation mapping builder
        /// </summary>
        private readonly AnnotationMappingBuilder _annotationMappingBuilder = new AnnotationMappingBuilder();

        private ReportingPeriodManager _reportingPeriodManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrieverCore" /> class.
        /// </summary>
        /// <param name="dataRetrieverSettings">The data retriever settings.</param>
        /// <exception cref="System.ArgumentNullException">
        /// connectionStringSettings
        /// or
        /// defaultHeader
        /// </exception>
        public DataRetrieverCore(DataRetrieverSettings dataRetrieverSettings)
        {
            if (dataRetrieverSettings == null)
            {
                throw new ArgumentNullException(nameof(dataRetrieverSettings));
            }

            this._dataRetrieverSettings = dataRetrieverSettings;

            this._maxNumberObservations = dataRetrieverSettings.MaxNumberObservations;

            if (dataRetrieverSettings.ComponentMappingManager == null)
            {
                throw new ArgumentException("ComponentMappingManager not set", nameof(dataRetrieverSettings));
            }

            this._componentMappingManager = dataRetrieverSettings.ComponentMappingManager;

            this._componentMappingValidationManager = dataRetrieverSettings.ComponentMappingValidationManager;

            if (dataRetrieverSettings.ConnectionBuilder == null)
            {
                throw new ArgumentException("ConnectionBuilder not set", nameof(dataRetrieverSettings));
            }

            if (string.IsNullOrWhiteSpace(dataRetrieverSettings.StoreId))
            {
                throw new ArgumentException("StoreId not set", nameof(dataRetrieverSettings));
            }

            this._storeId = dataRetrieverSettings.StoreId;

            if (dataRetrieverSettings.DefaultHeader != null)
            {
                this._defaultHeader = dataRetrieverSettings.DefaultHeader;
            }
            else
            {
                throw new ArgumentException("DefaultHeader not set", nameof(dataRetrieverSettings));
            }

            this._reportingPeriodManager = new ReportingPeriodManager();
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes the output to <paramref name="writer" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="disseminationDbSql" />
        ///     is null
        ///     -or-
        ///     <paramref name="writer" />
        ///     is null
        ///     -or-
        ///     <paramref name="query" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="query">
        ///     The query bean for which data will be requested.
        /// </param>
        /// <param name="disseminationDbSql">
        ///     The SQL statement to be executed
        /// </param>
        /// <param name="limit">
        ///     Set to a positive integer to limit the number of observations returned, else set to 0 to return all observations
        /// </param>
        /// <param name="writer">
        ///     The <see cref="ITabularWriter" /> (e.g. CSV, SQLite e.t.c.) writer
        /// </param>
        /// <example>
        ///     An example using this method in C#
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverManySteps.cs" lang="cs" />
        /// </example>
        public void ExecuteSqlQuery(IDataQuery query, string disseminationDbSql, int limit, ITabularWriter writer)
        {
            if (disseminationDbSql == null)
            {
                throw new ArgumentNullException("disseminationDbSql");
            }
            
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.Initialize(query);

                // build the data retrieval state
                var info = new DataRetrievalInfoTabular(mappingSet, query, _dataRetrieverSettings, writer)
                               {
                                   Limit = limit, 
                                   SqlString = disseminationDbSql, 
                                   DefaultHeader = this._defaultHeader, 
                                   MaximumAllowedNumberObservations = this._maxNumberObservations
                               };

                // check if mapping set is complete.
                this.ValidateMappingSet(mappingSet, query.DataStructure);

                BuildParametersForExecuteSql(info);

                // Execute sql from info.SqlString using Tabular Data Query Engine
                TabularDataQueryEngine queryEngine = TabularDataQueryEngine.Instance;
                queryEngine.ExecuteSqlQuery(info);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), 
                    Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);

                // ErrorTypes.DDB_CONNECTION_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model,
                // ex);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);

                // ErrorTypes.WRITING_OUTPUT_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce,
                // ex);
            }
        }

        /// <summary>
        ///     This method executes an asynchronous SQL query on the dissemination database and writes the output to <paramref name="writer" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="disseminationDbSql" />
        ///     is null
        ///     -or-
        ///     <paramref name="writer" />
        ///     is null
        ///     -or-
        ///     <paramref name="query" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="query">
        ///     The query bean for which data will be requested.
        /// </param>
        /// <param name="disseminationDbSql">
        ///     The SQL statement to be executed
        /// </param>
        /// <param name="limit">
        ///     Set to a positive integer to limit the number of observations returned, else set to 0 to return all observations
        /// </param>
        /// <param name="writer">
        ///     The <see cref="ITabularWriter" /> (e.g. CSV, SQLite e.t.c.) writer
        /// </param>
        /// <example>
        ///     An example using this method in C#
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverManySteps.cs" lang="cs" />
        /// </example>
        public async Task ExecuteSqlQueryAsync(IDataQuery query, string disseminationDbSql, int limit, ITabularWriter writer)
        {
            if (disseminationDbSql == null)
            {
                throw new ArgumentNullException("disseminationDbSql");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.Initialize(query);

                // build the data retrieval state
                var info = new DataRetrievalInfoTabular(mappingSet, query, _dataRetrieverSettings, writer)
                {
                    Limit = limit,
                    SqlString = disseminationDbSql,
                    DefaultHeader = this._defaultHeader,
                    MaximumAllowedNumberObservations = this._maxNumberObservations
                };

                // check if mapping set is complete.
                this.ValidateMappingSet(mappingSet, query.DataStructure);

                BuildParametersForExecuteSql(info);

                // Execute sql from info.SqlString using Tabular Data Query Engine
                TabularDataQueryEngine queryEngine = TabularDataQueryEngine.Instance;
                await queryEngine.ExecuteSqlQueryAsync(info).ConfigureAwait(false);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);

                // ErrorTypes.DDB_CONNECTION_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model,
                // ex);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);

                // ErrorTypes.WRITING_OUTPUT_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce,
                // ex);
            }
        }

        /// <summary>
        ///     This method executes an SQL query on the dissemination database and writes the output to <paramref name="writer" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="disseminationDbSql" />
        ///     is null
        ///     -or-
        ///     <paramref name="writer" />
        ///     is null
        ///     -or-
        ///     <paramref name="query" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="query">
        ///     The query bean for which data will be requested.
        /// </param>
        /// <param name="disseminationDbSql">
        ///     The SQL statement to be executed
        /// </param>
        /// <param name="limit">
        ///     Set to a positive integer to limit the number of observations returned, else set to 0 to return all observations
        /// </param>
        /// <param name="writer">
        ///     The <see cref="ITabularWriter" /> (e.g. CSV, SQLite e.t.c.) writer
        /// </param>
        /// <example>
        ///     An example using this method in C#
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverManySteps.cs" lang="cs" />
        /// </example>
        public void ExecuteSqlQuery(IComplexDataQuery query, string disseminationDbSql, int limit, ITabularWriter writer)
        {
            if (disseminationDbSql == null)
            {
                throw new ArgumentNullException("disseminationDbSql");
            }
            
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.InitializeAdvanced(query);

                // build the data retrieval state
                var info = new DataRetrievalInfoTabular(mappingSet, query, this._dataRetrieverSettings, writer)
                               {
                                   Limit = limit, 
                                   SqlString = disseminationDbSql, 
                                   DefaultHeader = this._defaultHeader, 
                                   MaximumAllowedNumberObservations = this._maxNumberObservations
                               };

                // check if mapping set is complete. TODO Remove this when SRA-166 is implemented
                this.ValidateMappingSet(mappingSet, query.DataStructure);
                BuildParametersForExecuteSql(info);

                // Execute sql from info.SqlString using Tabular Data Query Engine
                TabularDataQueryEngine queryEngine = TabularDataQueryEngine.Instance;
                queryEngine.ExecuteSqlQuery(info);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), 
                    Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
        }

        /// <summary>
        ///     This method executes an asynchronous SQL query on the dissemination database and writes the output to <paramref name="writer" />
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="disseminationDbSql" />
        ///     is null
        ///     -or-
        ///     <paramref name="writer" />
        ///     is null
        ///     -or-
        ///     <paramref name="query" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="query">
        ///     The query bean for which data will be requested.
        /// </param>
        /// <param name="disseminationDbSql">
        ///     The SQL statement to be executed
        /// </param>
        /// <param name="limit">
        ///     Set to a positive integer to limit the number of observations returned, else set to 0 to return all observations
        /// </param>
        /// <param name="writer">
        ///     The <see cref="ITabularWriter" /> (e.g. CSV, SQLite e.t.c.) writer
        /// </param>
        /// <example>
        ///     An example using this method in C#
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverManySteps.cs" lang="cs" />
        /// </example>
        public async Task ExecuteSqlQueryAsync(IComplexDataQuery query, string disseminationDbSql, int limit, ITabularWriter writer)
        {
            if (disseminationDbSql == null)
            {
                throw new ArgumentNullException("disseminationDbSql");
            }
           
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.InitializeAdvanced(query);

                // build the data retrieval state
                var info = new DataRetrievalInfoTabular(mappingSet, query, this._dataRetrieverSettings, writer)
                {
                    Limit = limit,
                    SqlString = disseminationDbSql,
                    DefaultHeader = this._defaultHeader,
                    MaximumAllowedNumberObservations = this._maxNumberObservations
                };

                // check if mapping set is complete. TODO Remove this when SRA-166 is implemented
                this.ValidateMappingSet(mappingSet, query.DataStructure);
                BuildParametersForExecuteSql(info);

                // Execute sql from info.SqlString using Tabular Data Query Engine
                TabularDataQueryEngine queryEngine = TabularDataQueryEngine.Instance;
                await queryEngine.ExecuteSqlQueryAsync(info).ConfigureAwait(false);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(
                    ex,
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError),
                    Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
        }

        /// <summary>
        ///     This method generates the SQL SELECT statement for the dissemination database that will return the data for the
        ///     incoming Query.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="query" />
        ///     is null
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///      <see cref="SdmxErrorCode" />
        /// </exception>
        /// <param name="query">
        ///     The <see cref="IDataQuery" /> modeling an SDMX-ML Query
        /// </param>
        /// <returns>
        ///     The generated SQL query.
        /// </returns>
        /// <example>
        ///     An example using this method in C#
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverManySteps.cs" lang="cs" />
        /// </example>
        public string GenerateSqlQuery(IDataQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.Initialize(query);

                // build the data retrieval state
                var info = new DataRetrievalInfo(mappingSet, query, this._dataRetrieverSettings) { MaximumAllowedNumberObservations = this._maxNumberObservations };

                // check if mapping set is complete. TODO Remove this when SRA-166 is implemented
                this.ValidateMappingSet(mappingSet, query.DataStructure);

                // Generate sql using Tabular SQL Builder and put the sql query into info.SqlString
                GenerateSql(info, _sqlBuilderTabular);

                // return the sql query
                return info.SqlString;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), Resources.DataRetriever_GenerateSqlQuery_Could_not_generate_sql_query);
            }
        }

        /// <summary>
        ///     Generates the SQL query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The string query</returns>
        /// <exception cref="ArgumentNullException">The query</exception>
        /// <exception cref="DataRetrieverException">DR exception</exception>
        public string GenerateSqlQuery(IComplexDataQuery query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            try
            {
                // Get mapping set
                var mappingSet = this.InitializeAdvanced(query);

                // build the data retrieval state
                var info = new DataRetrievalInfo(mappingSet, query, this._dataRetrieverSettings) { MaximumAllowedNumberObservations = this._maxNumberObservations };

                // Generate sql using Tabular SQL Builder and put the sql query into info.SqlString
                GenerateSql(info, _sqlBuilderTabular);

                // return the sql query
                return info.SqlString;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError), Resources.DataRetriever_GenerateSqlQuery_Could_not_generate_sql_query);
            }
        }

        /// <summary>
        ///     Retrieve data from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public method of
        ///     the DataRetriever class. It is called with a populated QueryBean (containing essentially an SDMX-ML Query) and a
        ///     database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input QueryBean for the Dataflow that data are requested and fetches the appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="ArgumentNullException">
        ///     dataQuery
        ///     or
        ///     dataWriter
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <example>
        ///     An example using this method in C# with <see cref="CompactDataWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetriever.cs" lang="cs"></code>
        ///     An example using this method in C# with <see cref="GenericDataWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverGeneric.cs" lang="cs"></code>
        /// </example>
        public void GetData(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.Initialize(dataQuery);

                dataWriter = GetReportingPeriodWriter(dataQuery, dataWriter, mappingSet);
                var info = new DataRetrievalInfoSeries(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter)
                               {
                                   DefaultHeader = this._defaultHeader, 
                                   MaximumAllowedNumberObservations =
                                       this._maxNumberObservations
                               };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);

                var messageHeader = WriteHeader(dataWriter, info);

                var observationActions = this._actionBuilder.Build(info);

                this.GenerateAndExecuteSql(dataQuery, dataWriter, observationActions, info, messageHeader);
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
            finally
            {
                // close output
                dataWriter.Close();
                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
        }

        /// <summary>
        ///     Retrieve data asynchronous from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public method of
        ///     the DataRetriever class. It is called with a populated QueryBean (containing essentially an SDMX-ML Query) and a
        ///     database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input QueryBean for the Dataflow that data are requested and fetches the appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="ArgumentNullException">
        ///     dataQuery
        ///     or
        ///     dataWriter
        /// </exception>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <example>
        ///     An example using this method in C# with <see cref="CompactDataWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetriever.cs" lang="cs"></code>
        ///     An example using this method in C# with <see cref="GenericDataWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverGeneric.cs" lang="cs"></code>
        /// </example>
        public async Task GetDataAsync(IDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            FooterMessageCore footerMessage = null;

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.Initialize(dataQuery);

                dataWriter = GetReportingPeriodWriter(dataQuery, dataWriter, mappingSet);
                var info = new DataRetrievalInfoSeries(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter)
                {
                    DefaultHeader = this._defaultHeader,
                    MaximumAllowedNumberObservations =
                        this._maxNumberObservations
                };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);

                var messageHeader = WriteHeader(dataWriter, info);

                var observationActions = this._actionBuilder.Build(info);

                await this.GenerateAndExecuteSqlAsync(dataQuery, dataWriter, observationActions, info, messageHeader);
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                footerMessage = new FooterMessageCore(
                    e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), 
                    Severity.Error, 
                    new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null));

                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                footerMessage = new FooterMessageCore(
                    e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), 
                    Severity.Error, 
                    new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null));

                throw;
            }
            catch (DataRetrieverException e)
            {
                footerMessage = new FooterMessageCore(
                    e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                    Severity.Error,
                    new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null));

                throw;
            }
            catch (SdmxException e)
            {
                footerMessage = new FooterMessageCore(
                    e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                    Severity.Error,
                    new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null));

                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                footerMessage = new FooterMessageCore(
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError).ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                    Severity.Error,
                    new TextTypeWrapperImpl("en", Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model, null));

                throw new DataRetrieverException(ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), 
                    Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException e)
            {
                footerMessage = new FooterMessageCore(
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError).ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                    Severity.Error,
                    new TextTypeWrapperImpl("en", Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model + e.Message, null));

                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                footerMessage = new FooterMessageCore(
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError).ClientErrorCode.ToString(CultureInfo.InvariantCulture),
                    Severity.Error,
                    new TextTypeWrapperImpl("en", Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model, null));

                throw new DataRetrieverException(ex, 
                    SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), 
                    Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
            finally
            {
                // close output
                if (footerMessage != null)
                {
                    dataWriter.Close(footerMessage);
                }
                else
                {
                    dataWriter.Close();
                }
                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
        }

        /// <summary>
        ///     Retrieve data from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public method of
        ///     the DataRetriever class. It is called with a populated QueryBean (containing essentially an SDMX-ML Query) and a
        ///     database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input QueryBean for the Dataflow that data are requested and fetches the appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="ArgumentNullException">
        ///     dataQuery
        ///     or
        ///     dataWriter
        /// </exception>
        /// <exception cref="SdmxSemmanticException"></exception>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <example>
        ///     An example using this method in C# with <see cref="ICrossSectionalWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverCrossSectional.cs" lang="cs"></code>
        /// </example>
        public void GetData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.Initialize(dataQuery);

                var info = new DataRetrievalInfoXS(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter) { DefaultHeader = this._defaultHeader, MaximumAllowedNumberObservations = this._maxNumberObservations };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);
                var messageHeader = WriteHeader(dataWriter, info);
                ICrossSectionalDataStructureObject crossDsd = dataQuery.DataStructure as ICrossSectionalDataStructureObject;
                if (crossDsd == null)
                {
                    throw new SdmxSemmanticException(string.Format(CultureInfo.InvariantCulture, "Requested SDMX v2.0 CrossSectional data for Dataflow '{0}' that uses a non SDMX v2.0 Cross-Sectional DSD '{1}'", dataQuery.Dataflow.Urn, dataQuery.DataStructure.Urn));
                }

                // (SRA-345) 
                // DR the info from I*DataQuery. DimensionAtObservation to IDataWriterEngine.StartDataSet  
                IDatasetStructureReference dsr = new DatasetStructureReferenceCore(string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);
                IDatasetHeader header = new DatasetHeaderCore(messageHeader.DatasetId, messageHeader.Action, dsr, this._dataRetrieverSettings.StructureUsage);

                dataWriter.StartDataset(dataQuery.Dataflow, crossDsd, header);

                GenerateSql(info, _sqlXsBuilder);

                // execute sql query.
                this.ExecuteSql(info, CrossSectionalQueryEngineManager.Instance.GetQueryEngine(info));
                CheckNumberOfRecordsStored(info);
                // close output
                dataWriter.Close();
                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString(), ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString(), ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
        }

        /// <summary>
        ///     Retrieve data asynchronous from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public method of
        ///     the DataRetriever class. It is called with a populated QueryBean (containing essentially an SDMX-ML Query) and a
        ///     database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input QueryBean for the Dataflow that data are requested and fetches the appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="ArgumentNullException">
        ///     dataQuery
        ///     or
        ///     dataWriter
        /// </exception>
        /// <exception cref="SdmxSemmanticException"></exception>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <example>
        ///     An example using this method in C# with <see cref="ICrossSectionalWriterEngine" />
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverCrossSectional.cs" lang="cs"></code>
        /// </example>
        public async Task GetDataAsync(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.Initialize(dataQuery);

                var info = new DataRetrievalInfoXS(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter) { DefaultHeader = this._defaultHeader, MaximumAllowedNumberObservations = this._maxNumberObservations };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);
                var messageHeader = WriteHeader(dataWriter, info);
                ICrossSectionalDataStructureObject crossDsd = dataQuery.DataStructure as ICrossSectionalDataStructureObject;
                if (crossDsd == null)
                {
                    throw new SdmxSemmanticException(string.Format(CultureInfo.InvariantCulture, "Requested SDMX v2.0 CrossSectional data for Dataflow '{0}' that uses a non SDMX v2.0 Cross-Sectional DSD '{1}'", dataQuery.Dataflow.Urn, dataQuery.DataStructure.Urn));
                }

                // (SRA-345) 
                // DR the info from I*DataQuery. DimensionAtObservation to IDataWriterEngine.StartDataSet  
                IDatasetStructureReference dsr = new DatasetStructureReferenceCore(string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);
                IDatasetHeader header = new DatasetHeaderCore(messageHeader.DatasetId, messageHeader.Action, dsr, HeaderUtils.GetStructureUsage(messageHeader, this._dataRetrieverSettings.StructureUsage));

                dataWriter.StartDataset(dataQuery.Dataflow, crossDsd, header);

                GenerateSql(info, _sqlXsBuilder);

                // execute sql query.
                await this.ExecuteSqlAsync(info, CrossSectionalQueryEngineManager.Instance.GetQueryEngine(info));
                CheckNumberOfRecordsStored(info);
                // close output
                dataWriter.Close();
                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString(), ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString(), ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
        }

        /// <summary>
        ///     Retrieve data from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public
        ///     method of the DataRetriever class. It is called with a populated IComplexDataQuery (containing essentially an
        ///     SDMX-ML Query) and a database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input IComplexDataQuery for the Dataflow that data are requested and fetches the
        ///             appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <param name="dataQuery">
        ///     The query bean for which data will be requested
        /// </param>
        /// <param name="dataWriter">
        ///     The Cross Sectional writer
        /// </param>
        public void GetData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }
            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.InitializeAdvanced(dataQuery);

                dataWriter = this.GetReportingPeriodWriter(dataQuery, dataWriter, mappingSet);
                var info = new DataRetrievalInfoComplex(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter) { DefaultHeader = this._defaultHeader, MaximumAllowedNumberObservations = this._maxNumberObservations };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);

                var messageHeader = WriteHeader(dataWriter, info);

                var header = this.BuildDatasetHeader(dataQuery, mappingSet, info, messageHeader);

                var annotations = this.BuildMappingAnnotations(mappingSet);

                // MAT-676 provide null  dataflow to StartDataset to avoid dataset namespaces changes after  v1.1.4 sync.
                dataWriter.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, header, annotations);

                this.GenerateAndExecuteSql(info);

                CheckNumberOfRecordsStored(info);
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
            finally
            {
                // close output
                dataWriter.Close();

                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
        }

        /// <summary>
        ///     Retrieve data asynchronous from a DDB and write it to the specified <paramref name="dataWriter" /> This is the main public
        ///     method of the DataRetriever class. It is called with a populated IComplexDataQuery (containing essentially an
        ///     SDMX-ML Query) and a database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input IComplexDataQuery for the Dataflow that data are requested and fetches the
        ///             appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="dataWriter" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="dataQuery" />
        ///     is null
        ///     -or-
        ///     <paramref name="dataWriter" />
        ///     is null
        /// </exception>
        /// <param name="dataQuery">
        ///     The query bean for which data will be requested
        /// </param>
        /// <param name="dataWriter">
        ///     The Cross Sectional writer
        /// </param>
        public async Task GetDataAsync(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException("dataWriter");
            }

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + dataWriter.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.InitializeAdvanced(dataQuery);

                dataWriter = this.GetReportingPeriodWriter(dataQuery, dataWriter, mappingSet);
                var info = new DataRetrievalInfoComplex(mappingSet, dataQuery, this._dataRetrieverSettings, dataWriter)
                {
                    DefaultHeader = this._defaultHeader,
                    MaximumAllowedNumberObservations = this._maxNumberObservations
                };
                this.ValidateMappingSet(mappingSet, dataQuery.DataStructure);

                var messageHeader = WriteHeader(dataWriter, info);

                var header = this.BuildDatasetHeader(dataQuery, mappingSet, info, messageHeader);

                var annotations = this.BuildMappingAnnotations(mappingSet);

                // MAT-676 provide null  dataflow to StartDataset to avoid dataset namespaces changes after  v1.1.4 sync.
                dataWriter.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, header, annotations);

                await this.GenerateAndExecuteSqlAsync(info);

                CheckNumberOfRecordsStored(info);
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (SdmxResponseSizeExceedsLimitException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (SdmxResponseTooLargeException e)
            {
                dataWriter.Close(new FooterMessageCore(e.SdmxErrorCode.ClientErrorCode.ToString(CultureInfo.InvariantCulture), Severity.Error, new TextTypeWrapperImpl("en", e.SdmxErrorCode.ErrorString + " : " + e.Message, null)));
                throw;
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);
            }
            finally
            {
                // close output
                dataWriter.Close();

                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
        }

        private IDataWriterEngine GetReportingPeriodWriter(IBaseDataQuery dataQuery, IDataWriterEngine dataWriter, IComponentMappingContainer mappingSet)
        {
            var reportingYearStartDate = mappingSet.ComponentMappings.FirstOrDefault(item => item.Value.Component != null && item.Value.Component.ObjectId == AttributeObject.Repyearstart).Value;

            // TODO verify again what this annotation is still needed or if it has been obsolete by Advance Time transcoding
            bool shouldCalculateReportingPeriod = dataQuery.Dataflow.Annotations.Any(AnnotationIsOfTypeReportingPeriod);
            if (reportingYearStartDate != null && (shouldCalculateReportingPeriod || this._dataRetrieverSettings.SdmxSchemaVersion == SdmxSchemaEnumType.VersionTwo))
            {
                dataWriter = this._reportingPeriodManager.GetEngine(shouldCalculateReportingPeriod, this._dataRetrieverSettings.SdmxSchemaVersion, mappingSet.TimeDimensionMapping.HasTranscoding(), dataWriter);
            }
            return dataWriter;
        }

        private static bool AnnotationIsOfTypeReportingPeriod(IAnnotation mutableObject)
        {
            CustomAnnotationType type;
            Enum.TryParse(mutableObject.Type, out type);
            return type == CustomAnnotationType.ReportingPeriod;
        }

        /// <summary>
        ///     Retrieve data from a DDB and write it to the specified <paramref name="writer" /> This is the main public method of
        ///     the DataRetriever class. It is called with a populated QueryBean (containing essentially an SDMX-ML Query) and a
        ///     database Connection to a "Mapping Store" database. This method is responsible for:
        ///     <list type="bullet">
        ///         <item>
        ///             Retrieving the <see cref="MappingSetEntity" /> (the class containing the performed mappings), according to
        ///             the provided Dataflow ID, from the "Mapping Store". Mapping Sets are defined on a Dataflow basis. Thus,
        ///             this method checks the input QueryBean for the Dataflow that data are requested and fetches the appropriate
        ///             <see cref="MappingSetEntity" />. If no <see cref="MappingSetEntity" /> exists, an exception (
        ///             <see cref="DataRetrieverException" />) is thrown.
        ///         </item>
        ///         <item>
        ///             Calling the method generating the appropriate SQL for the dissemination database.
        ///         </item>
        ///         <item>
        ///             Calling the method that executes the generated SQL and uses the
        ///             <paramref name="writer" />
        ///             to write the output.
        ///         </item>
        ///     </list>
        ///     <note type="note">
        ///         The "Data Retriever" expects exactly one Dataflow clause under the DataWhere clause, exactly one
        ///         DataFlowBean within the DataWhereBean (which in turn resides inside the incoming QueryBean).
        ///     </note>
        /// </summary>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     <paramref name="query" />
        ///     is null
        ///     -or-
        ///     <paramref name="writer" />
        ///     is null
        /// </exception>
        /// <param name="query">
        ///     The query bean for which data will be requested
        /// </param>
        /// <param name="writer">
        ///     The <see cref="ITabularWriter" /> (e.g. CSV, SQLite e.t.c.) writer
        /// </param>
        /// <param name="showOriginal">
        ///     A value indicating whether to show original (DDB) values
        /// </param>
        /// <example>
        ///     An example using this method in C# (Without original columns)
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverTabular.cs" lang="cs" />
        ///     An example using this method in C# (With original columns)
        ///     <code source="ReUsingExamples\DataRetriever\ReUsingDataRetrieverTabularWithOriginal.cs" lang="cs" />
        /// </example>
        public void RetrieveData(IDataQuery query, ITabularWriter writer, bool showOriginal)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }

            try
            {
                _logger.Info(Resources.InfoDataRetrieverBBInvoked);
                _logger.Info(Resources.InfoOutput + writer.GetType().Name);

                // validate input and initialize the mappingset entitiy
                var mappingSet = this.Initialize(query);

                var info = new DataRetrievalInfoTabular(mappingSet, query, this._dataRetrieverSettings, writer) { DefaultHeader = this._defaultHeader, MaximumAllowedNumberObservations = this._maxNumberObservations };
                this.ValidateMappingSet(mappingSet, query.DataStructure);

                GenerateSql(info, _sqlBuilderTabular);

                // execute sql query
                IDataQueryEngine<DataRetrievalInfoTabular> dataQueryEngine = showOriginal ? (IDataQueryEngine<DataRetrievalInfoTabular>)TabularDataOriginalQueryEngine.Instance : TabularDataQueryEngine.Instance;
                this.ExecuteSql(info, dataQueryEngine);
                CheckNumberOfRecordsStored(info);
                // close output
                writer.Close();
                _logger.Info(Resources.InfoEndDataRetrieverBBInvoked);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (SdmxException)
            {
                throw;
            }
            catch (DbException ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model);

                // ErrorTypes.DDB_CONNECTION_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_executing_generated_SQL_and_populating_SDMX_model,
                // ex);
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce);

                // ErrorTypes.WRITING_OUTPUT_ERROR,
                // Resources.DataRetriever_ExecuteSqlQuery_Error_during_writing_responce,
                // ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Generate the SQL string using <paramref name="sqlBuilder" /> with <paramref name="info" />
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state.
        /// </param>
        /// <param name="sqlBuilder">
        ///     The SQL builder.
        /// </param>
        private static void GenerateSql(DataRetrievalInfo info, ISqlBuilder sqlBuilder)
        {
            // Generate sql query
            _logger.Info(Resources.InfoStartGeneratingSQLDDB);
            sqlBuilder.GenerateSql(info);
            _logger.Info(Resources.InfoEndGeneratingSQLDDB);
        }

        /// <summary>
        ///     Write the SDMX header (from <paramref name="info" /> ) to <paramref name="writer" />
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="info">The info.</param>
        /// <returns>The <see cref="IHeader" />.</returns>
        private static IHeader WriteHeader(IWriterEngine writer, DataRetrievalInfo info)
        {
            // retrieve header information
            _logger.Info(Resources.DataRetriever_RetrieveData_Start_Retrieving_header_information);
            IHeader header = _headerBuilder.Build(info);
            _logger.Info(Resources.DataRetriever_RetrieveData_End_Retrieving_header_information);

            writer.WriteHeader(header);
            return header;
        }

        /// <summary>
        /// Builds the parameters for execute SQL.
        /// </summary>
        /// <param name="info">The information.</param>
        private static void BuildParametersForExecuteSql(DataRetrievalInfoTabular info)
        {
            var mappingSetEntity = info.MappingSet;
            if (mappingSetEntity.UpdateStatusMappingBuilder != null)
            {
                var statusMapping = mappingSetEntity.UpdateStatusMappingBuilder;
                var generateWhere = statusMapping.GenerateWhere(info.CurrentObservationAction);
                if (generateWhere != null)
                {
                    info.Parameters.AddAll(generateWhere.Parameters);
                }
            }
        }

        /// <summary>
        /// Builds the mapping annotations.
        /// </summary>
        /// <param name="mappingSet">The mapping set.</param>
        /// <returns>The mapping annotations if enabled; otherwise null</returns>
        private IAnnotation[] BuildMappingAnnotations(IComponentMappingContainer mappingSet)
        {
            IAnnotation[] annotations = null;
            if (this._dataRetrieverSettings.OutputLocalDataAsAnnotation)
            {
                annotations = this._annotationMappingBuilder.Build(mappingSet).ToArray();
            }

            return annotations;
        }

        /// <summary>
        /// Check if the <see cref="IComponentMappingContainer" /> is complete
        /// </summary>
        /// <param name="mappingContainer">The mapping container.</param>
        /// <param name="dsd">The DSD.</param>
        /// <exception cref="IncompleteMappingSetException">The mapping set is not valid, some mappings are missing</exception>
        /// <exception cref="DataRetrieverException">Incomplete mapping set. Please check if all dimensions, measure(s) and mandatory attributes are mapped</exception>
        private void ValidateMappingSet(IComponentMappingContainer mappingContainer, IDataStructureObject dsd)
        {
            if (_componentMappingValidationManager == null)
            {
                return;
            }

            // check if mapping is complete first
            var result = _componentMappingValidationManager.IsValid(mappingContainer, dsd);

            if (result.Status != StatusType.Success)
            {
                throw new IncompleteMappingSetException(Resources.ErrorIncompleteMappingSet + ":\n" + string.Join("\n", result.Messages));
            }
        }

        /// <summary>
        ///     This method executes an SQL query on the dissemination database
        /// </summary>
        /// <typeparam name="T">the type</typeparam>
        /// <param name="info">The current Data Retrieval state</param>
        /// <param name="queryEngine">The query engine.</param>
        /// <exception cref="DataRetrieverException">No records found exception</exception>
        /// <exception cref="SdmxResponseTooLargeException">Response too large exception</exception>
        private void ExecuteSql<T>(T info, IDataQueryEngine<T> queryEngine) where T : DataRetrievalInfo
        {
            _logger.Info(Resources.InfoStartExecutingSql);
            var task = Task.Run(async () => await queryEngine.ExecuteSqlQueryAsync(info));
            task.Wait();
            _logger.Info(Resources.InfoEndExecutingSql);
        }

        /// <summary>
        ///     This method executes an SQL query asynchronous on the dissemination database
        /// </summary>
        /// <typeparam name="T">the type</typeparam>
        /// <param name="info">The current Data Retrieval state</param>
        /// <param name="queryEngine">The query engine.</param>
        /// <exception cref="DataRetrieverException">No records found exception</exception>
        /// <exception cref="SdmxResponseTooLargeException">Response too large exception</exception>
        private async Task ExecuteSqlAsync<T>(T info, IDataQueryEngine<T> queryEngine) where T : DataRetrievalInfo
        {
            _logger.Info(Resources.InfoStartExecutingSql);
            await queryEngine.ExecuteSqlQueryAsync(info);
            _logger.Info(Resources.InfoEndExecutingSql);
        }

        private void CheckNumberOfRecordsStored(DataRetrievalInfo info) 
        {
            if (HeaderScope.ReturnDisseminationDbSql) {
                return;
            }
            var sdmxTwoPointOneBehaviour = info.ComplexQuery != null ||
                                           this._dataRetrieverSettings.SdmxSchemaVersion ==
                                           SdmxSchemaEnumType.VersionTwoPointOne;

            if (sdmxTwoPointOneBehaviour)
            {
                if (info.TotalRecords == 0)
                {
                    throw new DataRetrieverException(Resources.NoRecordsFound,
                        SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound));
                }

                if (info.IsTruncated)
                {
                    throw new SdmxResponseTooLargeException(string.Format(CultureInfo.InvariantCulture, "Reached limit : {0}",
                        info.Limit));
                }
            }
        }

        /// <summary>
        ///     Generates the and execute SQL.
        /// </summary>
        /// <param name="info">The information.</param>
        private void GenerateAndExecuteSql(DataRetrievalInfoSeries info)
        {
            if (!info.WriteMetadataAttributesOnly)
            {
                // Generate sql query
                GenerateSql(info, _sqlBuilder);
                GenerateSql(info, SeriesDataSetSqlBuilder.Instance);
                GenerateSql(info, SeriesGroupSqlBuilder.Instance);
            }

            if (info.WriteMetadata && info.MappingSet.MetadataDataset != null)
            {
                GenerateSql(info, SeriesMetadataSqlBuilder.Instance);
            }

            // execute sql query
            this.ExecuteSql(info, SeriesQueryEngineManager.Instance.GetQueryEngine(info));
        }



        /// <summary>
        ///     Generates the and execute SQL asynchronously.
        /// </summary>
        /// <param name="info">The information.</param>
        private async Task GenerateAndExecuteSqlAsync(DataRetrievalInfoSeries info)
        {
            if (!info.WriteMetadataAttributesOnly)
            {
                // Generate sql query
                GenerateSql(info, _sqlBuilder);
                GenerateSql(info, SeriesDataSetSqlBuilder.Instance);
                GenerateSql(info, SeriesGroupSqlBuilder.Instance);
            }

            if (info.WriteMetadata && info.MappingSet.MetadataDataset != null)
            {
                GenerateSql(info, SeriesMetadataSqlBuilder.Instance);
            }

            // execute sql query
            await this.ExecuteSqlAsync(info, SeriesQueryEngineManager.Instance.GetQueryEngine(info));
        }
        
        /// <summary>
        ///     Generates the and execute SQL.
        /// </summary>
        /// <param name="info">The information.</param>
        private void GenerateAndExecuteSql(DataRetrievalInfoComplex info)
        {
            // Generate sql query
            GenerateSql(info, _sqlComplexBuilder);

            GenerateSql(info, SeriesDataSetSqlBuilder.Instance);

            GenerateSql(info, SeriesGroupSqlBuilder.Instance);

            // execute sql query
            this.ExecuteSql(info, SeriesQueryEngineManager.Instance.GetQueryEngine(info));
        }

        /// <summary>
        ///     Generates the and execute SQL asynchronously.
        /// </summary>
        /// <param name="info">The information.</param>
        private async Task GenerateAndExecuteSqlAsync(DataRetrievalInfoComplex info)
        {
            // Generate sql query
            GenerateSql(info, _sqlComplexBuilder);

            GenerateSql(info, SeriesDataSetSqlBuilder.Instance);

            GenerateSql(info, SeriesGroupSqlBuilder.Instance);

            // execute sql query
            await this.ExecuteSqlAsync(info, SeriesQueryEngineManager.Instance.GetQueryEngine(info));
        }

        /// <summary>
        ///     This method initializes the Data Retriever. It performs some validation the Query, and retrieves from the Mapping
        ///     Store the mapping set (if any) of the dataflow of the QueryBean
        ///     <note type="note">
        ///         The
        ///         <see cref="IDataStructureObject" />
        ///         method should be called only after
        ///         this method has been called.
        ///     </note>
        /// </summary>
        /// <exception cref="DataRetrieverException">
        ///     See the
        ///      <see cref="SdmxErrorCode" />
        ///     for more details
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     ConnectionStringSettings is null
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        ///     query is null
        /// </exception>
        /// <param name="query">
        ///     The QueryBean containing the SDMX query
        /// </param>
        /// <returns>
        ///     The mapping set for the dataflow in <paramref name="query" />
        /// </returns>
        private IComponentMappingContainer Initialize(IDataQuery query)
        {
            _logger.Info(Resources.DataRetriever_RetrieveData_Start_Initializing_Data_Retriever);
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            // Get the dataflow from the query
            IDataflowObject dataFlow = query.Dataflow;

            // get the mapping set and the keyfamilybean
            var mappingSet = this.RetrieveMappingSet(dataFlow, query.DataStructure, query.MetadataStructure);

            // MAT-395
            if (mappingSet == null)
            {
                throw new DataRetrieverException(string.Format(CultureInfo.CurrentCulture, Resources.NoMappingForDataflowFormat1, dataFlow.Id), SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound));

                // ErrorTypes.NO_MAPPING_SET,
                // string.Format(CultureInfo.CurrentCulture, Resources.NoMappingForDataflowFormat1, dataFlow.Id));
            }

            _logger.Info(Resources.DataRetriever_RetrieveData_End_Data_Retriever_initialization);
            return mappingSet;
        }

        /// <summary>
        ///     Initializes the advanced.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>The mapping set entity</returns>
        /// <exception cref="ArgumentNullException">Missing query argument</exception>
        /// <exception cref="DataRetrieverException">The no mapping for data flow exception</exception>
        private IComponentMappingContainer InitializeAdvanced(IComplexDataQuery query)
        {
            _logger.Info(Resources.DataRetriever_RetrieveData_Start_Initializing_Data_Retriever);
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            // Get the dataflow from the query
            IDataflowObject dataFlow = query.Dataflow;

            // get the mapping set and the keyfamilybean
            var mappingSet = this.RetrieveMappingSet(dataFlow, query.DataStructure);

            // MAT-395
            if (mappingSet == null)
            {
                throw new DataRetrieverException(string.Format(CultureInfo.CurrentCulture, Resources.NoMappingForDataflowFormat1, dataFlow.Id), SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound));
            }

            // mappingSet = FilterMappingSet(query, mappingSet);
            _logger.Info(Resources.DataRetriever_RetrieveData_End_Data_Retriever_initialization);
            return mappingSet;
        }

        /// <summary>
        /// Generates the and execute SQL.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <param name="observationActions">The observation actions.</param>
        /// <param name="info">The information.</param>
        /// <param name="messageHeader">The message header.</param>
        private void GenerateAndExecuteSql(IDataQuery dataQuery, IDataWriterEngine dataWriter, IList<ObservationAction> observationActions, DataRetrievalInfoSeries info, IHeader messageHeader)
        {
            foreach (var observationAction in observationActions)
            {
                info.CurrentObservationAction = observationAction;
                info.Parameters.Clear();
                info.SqlWhereCache = null;
                info.DataSetSqlString = null;
                info.SqlString = null;
                info.DatasetId = messageHeader.DatasetId;


                if (!info.ShouldIncludeHistory)
                {
                    var datasetAction = this._datasetActionBuilder.Build(observationAction);
                    // (SRA-345) 
                    // DR the info from I*DataQuery. DimensionAtObservation to IDataWriterEngine.StartDataSet  
                    IDatasetStructureReference dsr = new DatasetStructureReferenceCore(string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);

                    var annotations = this.BuildMappingAnnotations(info.MappingSet);

                    dataWriter.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, new DatasetHeaderCore(messageHeader.DatasetId, datasetAction, dsr, HeaderUtils.GetStructureUsage(messageHeader,this._dataRetrieverSettings.StructureUsage)), annotations);
                }
                

                this.GenerateAndExecuteSql(info);
            }

            CheckNumberOfRecordsStored(info);
        }

        /// <summary>
        /// Generates the and execute SQL asynchronous.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <param name="observationActions">The observation actions.</param>
        /// <param name="info">The information.</param>
        /// <param name="messageHeader">The message header.</param>
        private async Task GenerateAndExecuteSqlAsync(IDataQuery dataQuery, IDataWriterEngine dataWriter, IList<ObservationAction> observationActions, DataRetrievalInfoSeries info, IHeader messageHeader)
        {
            foreach (var observationAction in observationActions)
            {
                info.CurrentObservationAction = observationAction;
                info.Parameters.Clear();
                info.SqlWhereCache = null;
                info.DataSetSqlString = null;
                info.SqlString = null;
                info.DatasetId = messageHeader.DatasetId;

                IDataQueryV2 dataQueryV2 = dataQuery as IDataQueryV2;
                if (dataQueryV2 != null)
                {
                    var iterator = dataQueryV2.StartIterator();
                    while (iterator.MoveNext())
                    {
                        info.DatasetId = dataQueryV2.DataStructure.Id;
                        await GenerateAndExecuteSqlForObservationAsync(dataQueryV2, dataWriter, observationAction, info, messageHeader);
                    }
                }
                else
                {
                    await GenerateAndExecuteSqlForObservationAsync(dataQuery, dataWriter, observationAction, info, messageHeader);
                }
            }

            CheckNumberOfRecordsStored(info);
        }

        /// <summary>
        /// Generates the and execute SQL for an observation action asynchronous.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <param name="observationAction">The observation action.</param>
        /// <param name="info">The information.</param>
        /// <param name="messageHeader">The message header.</param>
        private async Task GenerateAndExecuteSqlForObservationAsync(IDataQuery dataQuery, IDataWriterEngine dataWriter,
            ObservationAction observationAction, DataRetrievalInfoSeries info, IHeader messageHeader)
        {
            HeaderScope.RequestAborted.ThrowIfCancellationRequested();

            if (!info.ShouldIncludeHistory)
            {
                var datasetAction = messageHeader.AdditionalAttribtues.Count == 0 || String.IsNullOrEmpty(messageHeader.AdditionalAttribtues["DataSetAction"])
                    ? this._datasetActionBuilder.Build(observationAction)
                    : DatasetAction.GetAction(messageHeader.AdditionalAttribtues["DataSetAction"]);
                
                var structureUsage = messageHeader.AdditionalAttribtues.Count == 0 || String.IsNullOrEmpty(messageHeader.AdditionalAttribtues["StructureUsage"]) 
                    ? this._dataRetrieverSettings.StructureUsage
                    : SdmxStructureType.ParseClass(messageHeader.AdditionalAttribtues["StructureUsage"]).EnumType;

                // (SRA-345) 
                // DR the info from I*DataQuery. DimensionAtObservation to IDataWriterEngine.StartDataSet  
                IDatasetStructureReference dsr = new DatasetStructureReferenceCore(
                    string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);

                var annotations = this.BuildMappingAnnotations(info.MappingSet);

                info.DatasetHeader = new DatasetHeaderCore(messageHeader.DatasetId, datasetAction, dsr, structureUsage);

                dataWriter.StartDataset(dataQuery.Dataflow, dataQuery.DataStructure, info.DatasetHeader, annotations);
            }

            await this.GenerateAndExecuteSqlAsync(info);
        }

        /// <summary>
        /// Returns the mapping set of the specified <paramref name="dataFlow" /> .
        /// </summary>
        /// <param name="dataFlow">The Query dataflow</param>
        /// <param name="dsd">The DSD.</param>
        /// <param name="msd">The optional MSD.</param>
        /// <returns>
        /// The Mapping Set
        /// </returns>
        /// <exception cref="DataRetrieverException">See the  <see cref="SdmxErrorCode" /></exception>
        private IComponentMappingContainer RetrieveMappingSet(IDataflowObject dataFlow, IDataStructureObject dsd, IMetadataStructureDefinitionObject msd = null)
        {
            IComponentMappingContainer mappingSet;
            try
            {
                mappingSet = _componentMappingManager.GetBuilders(_storeId, dataFlow, dsd, msd, _dataRetrieverSettings.IsPit);
            }
            catch (DataRetrieverException)
            {
                throw;
            }
            catch (DbException ex)
            {
                string error = Resources.DataRetriever_Initialize_Could_not_retrieve_Mappings_from__Mapping_Store___Cause__ + ex.Message;
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), error);

                // ErrorTypes.MS_CONNECTION_ERROR, error, ex);
            }
            catch (ResourceNotFoundException ex)
            {
                string error = Resources.DataRetriever_Initialize_Error_while_retrieving_Mappings_from__Mapping_Store___Cause__ + ex.Message;
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.NoResultsFound), error);
            }
            catch (Exception ex)
            {
                string error = Resources.DataRetriever_Initialize_Error_while_retrieving_Mappings_from__Mapping_Store___Cause__ + ex.Message;
                throw new DataRetrieverException(ex, SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.InternalServerError), error);

                // ErrorTypes.MS_CONNECTION_ERROR, error, ex);
            }

            return mappingSet;
        }

        /// <summary>
        /// Builds the dataset header.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="mappingSet">The mapping set.</param>
        /// <param name="info">The information.</param>
        /// <param name="messageHeader">The message header.</param>
        /// <returns>THe dataset header</returns>
        private IDatasetHeader BuildDatasetHeader(IComplexDataQuery dataQuery, IComponentMappingContainer mappingSet, DataRetrievalInfoComplex info, IHeader messageHeader)
        {
            // (SRA-345) 
            // DR the info from I*DataQuery. DimensionAtObservation to IDataWriterEngine.StartDataSet  
            IDatasetStructureReference dsr = new DatasetStructureReferenceCore(string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);
            var datasetActionInDb = messageHeader.GetAdditionalAttribtue(nameof(ElementNameTable.DataSetAction));

            
            var observationAction = this._actionBuilder.Build(mappingSet, dataQuery).First();
            info.CurrentObservationAction = observationAction;
            var datasetAction = this._datasetActionBuilder.Build(observationAction);

            IDatasetHeader header = new DatasetHeaderCore(messageHeader.DatasetId, datasetAction, dsr, HeaderUtils.GetStructureUsage(messageHeader, this._dataRetrieverSettings.StructureUsage));
            
            return header;
        }

        #endregion
    }
}