﻿// -----------------------------------------------------------------------
// <copyright file="MappedValuesFlat.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     Holds the mapped and transcoded values without any attachment level information.
    /// </summary>
    internal class MappedValuesFlat : MappedValuesBase, IMappedValues
    {
        /// <summary>
        ///     Attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeValues = new List<ComponentValue>();

        /// <summary>
        ///     Dimension values
        /// </summary>
        private readonly List<ComponentValue> _dimensionValues = new List<ComponentValue>();

        /// <summary>
        ///     Primary /XS values values
        /// </summary>
        private readonly List<ComponentValue> _measureValues = new List<ComponentValue>();

        /// <summary>
        ///     The current data record reference.
        /// </summary>
        private readonly IDataRecord _record;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappedValuesFlat" /> class.
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        public MappedValuesFlat(DataRetrievalInfo info)
        {
            this.SetComponentOrder(info.MainComponentMappings);
            this.SetTimeDimensionComponent(info.BaseDataQuery.DataStructure.TimeDimension);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappedValuesFlat" /> class.
        /// </summary>
        /// <param name="record">
        ///     The current data record reference. used to retrieve the local columns
        /// </param>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        public MappedValuesFlat(IDataRecord record, DataRetrievalInfo info)
            : this(info)
        {
            this._record = record;
        }

        /// <summary>
        ///     Gets the attribute values
        /// </summary>
        public IEnumerable<ComponentValue> AttributeValues
        {
            get
            {
                return this._attributeValues;
            }
        }

        /// <summary>
        ///     Gets the Dimension values
        /// </summary>
        public IEnumerable<ComponentValue> DimensionValues
        {
            get
            {
                return this._dimensionValues;
            }
        }

        /// <summary>
        ///     Gets the Primary /XS values values
        /// </summary>
        public IEnumerable<ComponentValue> MeasureValues
        {
            get
            {
                return this._measureValues;
            }
        }

        /// <summary>
        ///     Returns an enumeration of the local column names
        /// </summary>
        /// <returns>
        ///     an enumeration of the local column names
        /// </returns>
        public IEnumerable<string> GetLocalColumns()
        {
            if (this._record == null)
            {
                yield break;
            }

            for (int i = 0; i < this._record.FieldCount; i++)
            {
                yield return this._record.GetName(i);
            }
        }

        /// <summary>
        ///     Returns an enumeration of a key value pairs of local column name and value
        /// </summary>
        /// <returns>
        ///     an enumeration of a key value pairs of local column name and value
        /// </returns>
        public IEnumerable<KeyValuePair<string, string>> GetLocalValues()
        {
            if (this._record == null)
            {
                yield break;
            }

            for (int i = 0; i < this._record.FieldCount; i++)
            {
                string key = this._record.GetName(i);
                string value = this._record.IsDBNull(i)
                                   ? string.Empty
                                   : Convert.ToString(this._record.GetValue(i), CultureInfo.InvariantCulture);

                yield return new KeyValuePair<string, string>(key, value);
            }
        }

        /// <summary>
        ///     Initialize the internal order of the components based on the specified <paramref name="componentMappings" />
        /// </summary>
        /// <param name="componentMappings">
        ///     The component mappings
        /// </param>
        private void SetComponentOrder(IEnumerable<MappingInfo> componentMappings)
        {
            foreach (var componentMapping in componentMappings)
            {
                var componentValue = new ComponentValue(componentMapping.Component);
                this.ComponentValues.Add(componentValue);
                switch (componentMapping.Component.StructureType.EnumType)
                {
                    case SdmxStructureEnumType.Dimension:
                    case SdmxStructureEnumType.MeasureDimension:
                        this._dimensionValues.Add(componentValue);
                        break;
                    case SdmxStructureEnumType.DataAttribute:
                        this._attributeValues.Add(componentValue);
                        break;
                    case SdmxStructureEnumType.PrimaryMeasure:
                    case SdmxStructureEnumType.CrossSectionalMeasure:
                        this._measureValues.Add(componentValue);
                        break;
                }
            }
        }
    }
}