// -----------------------------------------------------------------------
// <copyright file="MappingInfo.cs" company="EUROSTAT">
//   Date Created : 2017-09-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    /// A container for holding all what is needed to map a component
    /// </summary>
    internal class MappingInfo
    {
        private readonly bool _canMapToAraryValue;

        private readonly IComponentArrayMapper _arrayMapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingInfo"/> class.
        /// </summary>
        /// <param name="mappingBuilder">The mapping builder.</param>
        /// <param name="mapping">The mapping.</param>
        /// <param name="component">The component.</param>
        public MappingInfo(IComponentMappingBuilder mappingBuilder, ComponentMappingEntity mapping, IComponent component)
        {
            this.MappingBuilder = mappingBuilder;
            this.Mapping = mapping;
            this.Component = component;
            this.Attribute = component as IAttributeObject;
            if (this.Attribute == null)
            {
               this.Dimension = component as IDimension;
                if (this.Dimension == null)
                {
                    this.PrimaryMeasure = component as IPrimaryMeasure;
                    if (this.PrimaryMeasure == null)
                    {
                        this.CrossSectionalMeasure = component as ICrossSectionalMeasure;
                    }
                }
            }

            // can it be mapped to array value
            if (Mapping.Component.ValueType == ComponentValueType.Array)
            {
                if (!(Component.StructureType.EnumType == SdmxStructureEnumType.DataAttribute ||
                      Component.StructureType.EnumType == SdmxStructureEnumType.Measure))
                {
                    // component value type is array, but component type is not data attribute or measure
                    throw new SdmxException($"{Component.Id}: array value only valid for attributes and measures.");
                }
                if (MappingBuilder is not IComponentArrayMapper arrayMapper)
                {
                    // component value type is array, but mapping builder is not
                    throw new SdmxException($"{Component.Id}: array type component must have a mapper of type IComponentArrayMapper");
                }
                _arrayMapper = arrayMapper;
                _canMapToAraryValue = true;
            }
        }

        /// <summary>
        /// Gets the <see cref="Component"/> as an primary measure object. It can be null
        /// </summary>
        /// <value>
        /// To primary measure.
        /// </value>
        public IPrimaryMeasure PrimaryMeasure { get; }

        /// <summary>
        /// Gets the mapping builder.
        /// </summary>
        /// <value>
        /// The mapping builder.
        /// </value>
        public IComponentMappingBuilder MappingBuilder { get; }

        /// <summary>
        /// Gets the mapping.
        /// </summary>
        /// <value>
        /// The mapping.
        /// </value>
        public ComponentMappingEntity Mapping { get; }

        /// <summary>
        /// Gets the component.
        /// </summary>
        /// <value>
        /// The component.
        /// </value>
        public IComponent Component { get; }

        /// <summary>
        /// Gets the <see cref="Component"/> as an attribute object. It can be null
        /// </summary>
        /// <value>
        /// As attribute object.
        /// </value>
        public IAttributeObject Attribute { get; }

        /// <summary>
        /// Gets the <see cref="Component"/> as a dimension. It can be null
        /// </summary>
        /// <value>
        /// As a dimension.
        /// </value>
        public IDimension Dimension { get; }

        /// <summary>
        /// Gets the cross sectional measure.
        /// </summary>
        /// <value>
        /// The cross sectional measure.
        /// </value>
        public ICrossSectionalMeasure CrossSectionalMeasure { get;  }

        /// <summary>
        /// Returns whether this component mapping info has all requirements to map to an array value or not.
        /// Outputs the <see cref="IComponentArrayMapper"/> if so, or <c>null</c>.
        /// </summary>
        public bool CanMapToArrayValue(out IComponentArrayMapper arrayMapper)
        {
            arrayMapper = _arrayMapper;
            return _canMapToAraryValue;
        }
    }
}