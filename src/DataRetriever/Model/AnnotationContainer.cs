﻿// -----------------------------------------------------------------------
// <copyright file="AnnotationContainer.cs" company="EUROSTAT">
//   Date Created : 2018-01-03
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;

    using Estat.Nsi.DataRetriever.Builders;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;
    using Org.Sdmxsource.Util;

    internal class AnnotationContainer
    {
        /// <summary>
        /// The annotation builder
        /// </summary>
        private readonly LocalDataAnnotationBuilder _annotationBuilder = new LocalDataAnnotationBuilder();

        /// <summary>
        /// The column name to annotation
        /// </summary>
        private readonly Dictionary<string, IAnnotationMutableObject> _columnNameToAnnotation = new Dictionary<string, IAnnotationMutableObject>(StringComparer.Ordinal);
        
        /// <summary>
        /// The series annotations
        /// </summary>
        private readonly IList<IAnnotationMutableObject> _seriesAnnotations = new List<IAnnotationMutableObject>();

        /// <summary>
        /// The dataset annotations
        /// </summary>
        private readonly IList<IAnnotationMutableObject> _datasetAnnotations = new List<IAnnotationMutableObject>();

        /// <summary>
        /// The obs annotations
        /// </summary>
        private readonly IList<IAnnotationMutableObject> _obsAnnotations = new List<IAnnotationMutableObject>();

        /// <summary>
        /// The group annotations
        /// </summary>
        private readonly IList<IAnnotationMutableObject> _groupAnnotations = new List<IAnnotationMutableObject>();

        /// <summary>
        /// Initializes a new instance of the <see cref="AnnotationContainer" /> class.
        /// </summary>
        /// <param name="componentMappings">The component mappings.</param>
        /// <param name="dimensionAtObservation">The dimension at observation.</param>
        /// <param name="timeDimensionMappingEntity">The time dimension mapping entity.</param>
        public AnnotationContainer(IList<MappingInfo> componentMappings, string dimensionAtObservation, TimeDimensionMappingEntity timeDimensionMappingEntity)
        {
            var dsd = (IDataStructureObject)componentMappings.Select(x => x.Component?.MaintainableParent).First(c => c != null);
            var groupDimensions = new HashSet<string>(dsd.Groups.SelectMany(x => x.DimensionRefs), StringComparer.Ordinal);
            
            foreach (var componentMapping in componentMappings)
            {
                var componentValue = componentMapping.Component;
                switch (componentMapping.Component.StructureType.EnumType)
                {
                    case SdmxStructureEnumType.Dimension:
                    case SdmxStructureEnumType.MeasureDimension:
                        if (!componentValue.Id.Equals(dimensionAtObservation))
                        {
                            BuildSeriesAnnotation(componentMapping);
                        }

                        if (groupDimensions.Contains(componentValue.Id))
                        {
                            BuildGroupAnnotation(componentMapping);
                        }

                        break;
                    case SdmxStructureEnumType.DataAttribute:
                        {
                            var dataAttribute = componentMapping.Attribute;

                            switch (dataAttribute.AttachmentLevel)
                            {
                                case AttributeAttachmentLevel.DataSet:
                                    BuildDataSetAnnotation(componentMapping);
                                    break;
                                case AttributeAttachmentLevel.Group:
                                    this.BuildGroupAnnotation(componentMapping);

                                    break;
                                case AttributeAttachmentLevel.DimensionGroup:
                                    if (dataAttribute.DimensionReferences.Contains(dimensionAtObservation))
                                    {
                                        BuildObsAnnotation(componentMapping);
                                    }
                                    else
                                    {
                                        BuildSeriesAnnotation(componentMapping);
                                    }

                                    break;
                                case AttributeAttachmentLevel.Observation:
                                    BuildObsAnnotation(componentMapping);
                                    break;
                            }
                        }

                        break;
                    case SdmxStructureEnumType.PrimaryMeasure:
                    case SdmxStructureEnumType.CrossSectionalMeasure:
                        BuildObsAnnotation(componentMapping);
                        break;
                }
            }

            if (timeDimensionMappingEntity != null)
            {
                if (string.IsNullOrWhiteSpace(dimensionAtObservation) || DimensionObject.TimeDimensionFixedId.Equals(dimensionAtObservation))
                {
                    BuildAnnotation(this._obsAnnotations, timeDimensionMappingEntity);
                }
                else
                {
                    BuildAnnotation(this._seriesAnnotations, timeDimensionMappingEntity);
                }
            }
        }

        /// <summary>
        /// Gets the series annotations.
        /// </summary>
        /// <value>
        /// The series annotations.
        /// </value>
        public IEnumerable<IAnnotation> SeriesAnnotations => this._seriesAnnotations.Select(a => new AnnotationObjectCore(a, null));

        /// <summary>
        /// Gets the dataset annotations.
        /// </summary>
        /// <value>
        /// The dataset annotations.
        /// </value>
        public IEnumerable<IAnnotation> DatasetAnnotations => this._datasetAnnotations.Select(a => new AnnotationObjectCore(a, null));

        /// <summary>
        /// Gets the obs annotations.
        /// </summary>
        /// <value>
        /// The obs annotations.
        /// </value>
        public IEnumerable<IAnnotation> ObsAnnotations => this._obsAnnotations.Select(a => new AnnotationObjectCore(a, null));

        /// <summary>
        /// Gets the group annotations.
        /// </summary>
        /// <value>
        /// The group annotations.
        /// </value>
        public IEnumerable<IAnnotation> GroupAnnotations => this._groupAnnotations.Select(a => new AnnotationObjectCore(a, null));
        
        /// <summary>
        /// Sets the values.
        /// </summary>
        /// <param name="reader">The reader.</param>
        public void SetValues(IDataReader reader)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                string key = reader.GetName(i);
                IAnnotationMutableObject annotation;
                if (this._columnNameToAnnotation.TryGetValue(key, out annotation))
                {
                    string value = Convert.ToString(reader.GetValue(i), CultureInfo.InvariantCulture);
                    annotation.Text[0].Value = value;
                }
            }
        }

        /// <summary>
        /// Builds the series annotation.
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void BuildSeriesAnnotation(MappingInfo mappingInfo)
        {
            this.BuildAnnotation(this._seriesAnnotations, mappingInfo.Mapping);
        }

        /// <summary>
        /// Builds the group annotation.
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void BuildGroupAnnotation(MappingInfo mappingInfo)
        {
            this.BuildAnnotation(this._groupAnnotations, mappingInfo.Mapping);
        }

        /// <summary>
        /// Builds the obs annotation.
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void BuildObsAnnotation(MappingInfo mappingInfo)
        {
            this.BuildAnnotation(this._obsAnnotations, mappingInfo.Mapping);
        }

        /// <summary>
        /// Builds the data set annotation.
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void BuildDataSetAnnotation(MappingInfo mappingInfo)
        {
            this.BuildAnnotation(this._datasetAnnotations, mappingInfo.Mapping);
        }

        /// <summary>
        /// Builds the annotation.
        /// </summary>
        /// <param name="annotationMutableObjects">The annotation mutable objects.</param>
        /// <param name="mappingInfoMapping"></param>
        private void BuildAnnotation(IList<IAnnotationMutableObject> annotationMutableObjects, IMappingEntity mappingInfoMapping)
        {
            if (!ObjectUtil.ValidCollection(mappingInfoMapping.GetColumns()))
            {
                return;
            }

            foreach (var column in mappingInfoMapping.GetColumns())
            {
                IAnnotationMutableObject annotation;
                if (!this._columnNameToAnnotation.TryGetValue(column.Name, out annotation))
                {
                    annotation = this._annotationBuilder.Build(column);
                    this._columnNameToAnnotation.Add(column.Name, annotation);
                }

                if (!annotationMutableObjects.Contains(annotation))
                {
                    annotationMutableObjects.Add(annotation);
                }
            }
        }
    }
}