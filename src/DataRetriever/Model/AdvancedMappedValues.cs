// -----------------------------------------------------------------------
// <copyright file="AdvancedMappedValues.cs" company="EUROSTAT">
//   Date Created : 2023-03-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using Estat.Nsi.DataRetriever.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// Inherits from <see cref="MappedValues"/> and adds:
    /// 1. separation of scalar from array components
    /// 2. pre-filtered components based on request info
    /// </summary>
    internal class AdvancedMappedValues : MappedValues
    {
        #region Fields

        /// <summary>
        ///     dataset attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeDataSetValues = new List<ComponentValue>();

        /// <summary>
        ///     dataset attribute array values
        /// </summary>
        private readonly List<ComponentValue> _attributeDataSetArrayValues = new List<ComponentValue>();

        /// <summary>
        ///     measure values
        /// </summary>
        private readonly List<ComponentValue> _measureValues = new List<ComponentValue>();

        /// <summary>
        ///     measure array values
        /// </summary>
        private readonly List<ComponentValue> _measureArrayValues = new List<ComponentValue>();

        /// <summary>
        ///     group attribute values. It should contain only the attributes of a specific group.
        /// </summary>
        private readonly List<ComponentValue> _attributeGroupValues = new List<ComponentValue>();

        /// <summary>
        ///     group attribute array values. It should contain only the attributes of a specific group.
        /// </summary>
        private readonly List<ComponentValue> _attributeGroupArrayValues = new List<ComponentValue>();

        /// <summary>
        ///     OBS attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeObservationValues = new List<ComponentValue>();

        /// <summary>
        ///     OBS attribute array values
        /// </summary>
        private readonly List<ComponentValue> _attributeObservationArrayValues = new List<ComponentValue>();

        /// <summary>
        ///     series attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeSeriesValues = new List<ComponentValue>();

        /// <summary>
        ///     series attributearray  values
        /// </summary>
        private readonly List<ComponentValue> _attributeSeriesArrayValues = new List<ComponentValue>();

        #endregion

        #region properties

        /// <summary>
        ///     Gets the scalar attributes attached to DataSet level
        /// </summary>
        public override IEnumerable<ComponentValue> AttributeDataSetValues
        {
            get
            {
                return this._attributeDataSetValues;
            }
        }

        /// <summary>
        ///     Gets the array attributes attached to DataSet level
        /// </summary>
        public IEnumerable<ComponentValue> AttributeDataSetArrayValues
        {
            get
            {
                return this._attributeDataSetArrayValues;
            }
        }

        /// <summary>
        ///     Gets the scalar measures values. 
        /// </summary>
        /// <remarks>
        ///     SDMX 3.0.0 compliant. 
        ///     The primary measure in previous versions will be returned
        ///     if no other measures are found.
        ///     TODO: This property should eventually replace PrimaryMeasure property.
        /// </remarks>
        public override IEnumerable<ComponentValue> MeasureValues
        {
            get
            {
                return this._measureValues;
            }
        }

        /// <summary>
        ///     Gets the measures array values. 
        /// </summary>
        public IEnumerable<ComponentValue> MeasureArrayValues
        {
            get
            {
                return _measureArrayValues;
            }
        }

        /// <summary>
        ///     Gets scalar group attribute values. To avoid extra checks, only attributes and dimensions of a specific group should be
        ///     added at <see cref="MappedValuesBase.Add" /> . So this should contain only the attributes of a specific group.
        /// </summary>
        public override IEnumerable<ComponentValue> AttributeGroupValues
        {
            get
            {
                return this._attributeGroupValues;
            }
        }

        /// <summary>
        ///     Gets group attribute array values. To avoid extra checks, only attributes and dimensions of a specific group should be
        ///     added at <see cref="MappedValuesBase.Add" /> . So this should contain only the attributes of a specific group.
        /// </summary>
        public IEnumerable<ComponentValue> AttributeGroupArrayValues
        {
            get
            {
                return this._attributeGroupArrayValues;
            }
        }

        /// <summary>
        ///     Gets the scalar Observation attribute values
        /// </summary>
        public override IEnumerable<ComponentValue> AttributeObservationValues
        {
            get
            {
                return this._attributeObservationValues;
            }
        }

        /// <summary>
        ///     Gets the Observation attribute array values
        /// </summary>
        public IEnumerable<ComponentValue> AttributeObservationArrayValues
        {
            get
            {
                return this._attributeObservationArrayValues;
            }
        }

        /// <summary>
        ///     Gets the scalar series attribute values
        /// </summary>
        public override IEnumerable<ComponentValue> AttributeSeriesValues
        {
            get
            {
                return this._attributeSeriesValues;
            }
        }

        /// <summary>
        ///     Gets the series attribute array values
        /// </summary>
        public IEnumerable<ComponentValue> AttributeSeriesArrayValues
        {
            get
            {
                return this._attributeSeriesArrayValues;
            }
        }

        #endregion

        public AdvancedMappedValues(DataRetrievalInfoSeries info) 
            : base(info)
        {
        }

        public AdvancedMappedValues(DataRetrievalInfoSeries info, IEnumerable<MappingInfo> componentMappings)
            : base(info, componentMappings)
        {
        }


        #region methods

        protected override void AddAttribute(ComponentValue componentValue, AttributeAttachmentLevel attachmentLevel, DataRetrievalInfo info)
        {
            if (!((DataRetrievalInfoSeries)info).WriteAttributes)
            {
                return;
            } 

            bool isSpecificAttributes = info.BaseDataQuery.Attributes == AttributesEnumType.Specific;
            IList<string> specificAttributeIds = info.BaseDataQuery.SpecificAttributes;
            AttributesEnumType requestedLevel = info.BaseDataQuery.Attributes.EnumType;

            switch (attachmentLevel)
            {
                case AttributeAttachmentLevel.DataSet:
                    AddAttributeToCache(componentValue,
                        AttributesEnumType.Dataset, requestedLevel,
                        _attributeDataSetValues, _attributeDataSetArrayValues, 
                        isSpecificAttributes, specificAttributeIds);
                    break;
                case AttributeAttachmentLevel.Group:
                    AddAttributeToCache(componentValue, 
                        AttributesEnumType.Dsd, requestedLevel,
                        _attributeGroupValues, _attributeGroupArrayValues,
                        isSpecificAttributes, specificAttributeIds);
                    break;
                case AttributeAttachmentLevel.DimensionGroup:
                    if (IsDimensionObsReference(componentValue, info))
                    {
                        AddAttributeToCache(componentValue,
                            AttributesEnumType.Obs, requestedLevel,
                            _attributeObservationValues, _attributeObservationArrayValues,
                            isSpecificAttributes, specificAttributeIds);
                    }
                    else
                    {
                        AddAttributeToCache(componentValue, 
                            AttributesEnumType.Series, requestedLevel,
                            _attributeSeriesValues, _attributeSeriesArrayValues,
                            isSpecificAttributes, specificAttributeIds);
                    }
                    break;
                case AttributeAttachmentLevel.Observation:
                    AddAttributeToCache(componentValue,
                        AttributesEnumType.Obs, requestedLevel,
                        _attributeObservationValues, _attributeObservationArrayValues,
                        isSpecificAttributes, specificAttributeIds);
                    break;
            }
        }

        protected override void AddMeasure(ComponentValue componentValue, DataRetrievalInfo info)
        {
            if (info.Settings.SdmxSchemaVersion == SdmxSchemaEnumType.VersionThree)
            {
                if (info.BaseDataQuery.Measures != MeasuresEnumType.None)
                {
                    AddComponentToCache(componentValue, _measureValues, _measureArrayValues,
                        info.BaseDataQuery.Measures == MeasuresEnumType.Specific,
                        info.BaseDataQuery.SpecificMeasures);
                }
            }
            else
            {
                base.AddMeasure(componentValue, info);
            }
        }

        private bool AcceptsAttributeLevel(AttributesEnumType attributeLevel, AttributesEnumType requestedLevel)
        {
            switch (requestedLevel)
            {
                case AttributesEnumType.Dataset:
                case AttributesEnumType.Obs:
                case AttributesEnumType.Series:
                case AttributesEnumType.Msd:
                    return attributeLevel == requestedLevel;
                case AttributesEnumType.None:
                    return false;
                case AttributesEnumType.All:
                case AttributesEnumType.Specific:
                case AttributesEnumType.Dsd:
                default:
                    return true;
            }
        }

        private void AddAttributeToCache(ComponentValue component,
            AttributesEnumType attributeLevel, AttributesEnumType requestedLevel,
            IList<ComponentValue> targetScalarList, IList<ComponentValue> targetArrayList,
            bool isSpecific, IList<string> specificIds)
        {
            if (AcceptsAttributeLevel(attributeLevel, requestedLevel))
            {
                AddComponentToCache(component,
                    targetScalarList, targetArrayList,
                    isSpecific, specificIds);
            }
        }

        private void AddComponentToCache(ComponentValue component,
            IList<ComponentValue> targetScalarList, IList<ComponentValue> targetArrayList,
            bool isSpecific, IList<string> specificIds)
        {
            if (isSpecific && !specificIds.Contains(component.Key.Id))
            {
                return;
            }
            if (component.IsArray)
            {
                targetArrayList.Add(component);
            }
            else
            {
                targetScalarList.Add(component);
            }
        }

        #endregion
    }
}
