// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfoTabular.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace Estat.Nsi.DataRetriever.Model
{
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.TabularWriters.Engine;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    ///     The current data retrieval state for tabular output
    /// </summary>
    internal class DataRetrievalInfoTabular : DataRetrievalInfo
    {
        /// <summary>
        ///     Writer provided for tabular to write the retrieved data.
        /// </summary>
        private readonly ITabularWriter _tabularWriter;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataRetrievalInfoTabular" /> class.
        /// </summary>
        /// <param name="mappingSet">
        ///     The mapping set of the dataflow found in the sdmx query
        /// </param>
        /// <param name="query">
        ///     The current SDMX Query object
        /// </param>
        /// <param name="settings">
        ///     The Mapping Store connection string settings
        /// </param>
        /// <param name="tabularWriter">
        ///     The tabular Writer.
        /// </param>
        public DataRetrievalInfoTabular(
            IComponentMappingContainer mappingSet, 
            IDataQuery query, 
            DataRetrieverSettings settings, 
            ITabularWriter tabularWriter)
            : base(mappingSet, query, settings)
        {
            this._tabularWriter = tabularWriter;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataRetrievalInfoTabular" /> class.
        /// </summary>
        /// <param name="mappingSet">
        ///     The mapping set of the dataflow found in the sdmx query
        /// </param>
        /// <param name="query">
        ///     The current SDMX Query object
        /// </param>
        /// <param name="settings">
        ///     The Mapping Store connection string settings
        /// </param>
        /// <param name="tabularWriter">
        ///     The tabular Writer.
        /// </param>
        public DataRetrievalInfoTabular(
            IComponentMappingContainer mappingSet, 
            IComplexDataQuery query,
            DataRetrieverSettings settings, 
            ITabularWriter tabularWriter)
            : base(mappingSet, query, settings)
        {
            this._tabularWriter = tabularWriter;
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has first n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has first n observations; otherwise, <c>false</c>.
        /// </value>
        public override bool HasFirstNObservations
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has last n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has last n observations; otherwise, <c>false</c>.
        /// </value>
        public override bool HasLastNObservations
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets the writer provided for tabular to write the retrieved data. If null the
        ///     <see
        ///         cref="DataRetrievalInfoSeries.SeriesWriter" />
        ///     or <see cref="DataRetrievalInfoXS.XSWriter" /> should be set instead.
        /// </summary>
        public ITabularWriter TabularWriter
        {
            get
            {
                return this._tabularWriter;
            }
        }

        public Tuple<long?, long?> Range => this._settings.Range;
    }
}