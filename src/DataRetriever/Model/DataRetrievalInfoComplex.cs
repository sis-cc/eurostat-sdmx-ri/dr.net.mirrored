﻿// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfoComplex.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;

    /// <summary>
    ///     The data retrieval complex class
    /// </summary>
    /// <seealso cref="Estat.Nsi.DataRetriever.Model.DataRetrievalInfoSeries" />
    internal class DataRetrievalInfoComplex : DataRetrievalInfoSeries
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrievalInfoComplex" /> class.
        /// </summary>
        /// <param name="mappingSet">The mapping set of the dataflow found in the sdmx query</param>
        /// <param name="query">The current SDMX Query object</param>
        /// <param name="settings">The settings.</param>
        /// <param name="complexWriter">The tabular Writer.</param>
        public DataRetrievalInfoComplex(
            IComponentMappingContainer mappingSet, 
            IComplexDataQuery query,
            DataRetrieverSettings settings, 
            IDataWriterEngine complexWriter)
            : base(mappingSet, query, settings, complexWriter)
        {
            this.Limit = query.DefaultLimit.HasValue ? query.DefaultLimit.Value : 0;
        }
    }
}