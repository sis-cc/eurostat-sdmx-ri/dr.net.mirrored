// -----------------------------------------------------------------------
// <copyright file="MappedXsValues.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     The mapped XS values.
    /// </summary>
    internal class MappedXsValues : MappedValuesBase, IMappedValues
    {
        /// <summary>
        ///     The data set level index
        /// </summary>
        private const int DataSetIndex = 0;

        /// <summary>
        ///     The group level index
        /// </summary>
        private const int GroupIndex = 1;

        /// <summary>
        ///     The observation level index
        /// </summary>
        private const int ObservationIndex = 3;

        /// <summary>
        ///     The section level index
        /// </summary>
        private const int SectionIndex = 2;

        /// <summary>
        ///     An array of collections used to store the attributes of different cross sectional levels.
        ///  The index used is 0 for Datasets, 1 for Group, 2 for Section and finally 3 for observation
        ///     The attachment level index, <see cref="DataSetIndex" /> <see cref="GroupIndex"/>, <see cref="SectionIndex"/> and <see cref="ObservationIndex"/> .
        /// </summary>
        private readonly List<ComponentValue>[] _attributeValues = new List<ComponentValue>[4];

        /// <summary>
        ///     An array of collections used to store the dimensions of different cross sectional levels. 
        ///     The index used is 0 for Datasets, 1 for Group, 2 for Section and finally 3 for observation
        ///     The attachment level index, <see cref="DataSetIndex" /> <see cref="GroupIndex"/>, <see cref="SectionIndex"/> and <see cref="ObservationIndex"/> .
        /// </summary>
        private readonly List<ComponentValue>[] _dimensionValues = new List<ComponentValue>[4];

        /// <summary>
        ///     An array of collections used to store the dimensions of different cross sectional levels. 
        ///  The index used is 0 for Datasets, 1 for Group, 2 for Section and finally 3 for observation
        ///     The attachment level index, <see cref="DataSetIndex" /> <see cref="GroupIndex"/>, <see cref="SectionIndex"/> and <see cref="ObservationIndex"/> .
        /// </summary>
        private readonly List<ComponentValue>[] _keyValues = new List<ComponentValue>[4];

        /// <summary>
        ///     The previous key values
        /// </summary>
        private readonly string[][] _previousKeyValues = new string[5][];

        /// <summary>
        ///     A value indicating whether a specific level has started at all.
        /// </summary>
        private readonly bool[] _startedLevel = new bool[4];

        /// <summary>
        ///     CrossSectional measure values
        /// </summary>
        private readonly List<ComponentValue> _xsMeasureValues = new List<ComponentValue>();

        /// <summary>
        /// The XS measures and attribute values
        /// </summary>
        private readonly Dictionary<string, Tuple<ComponentValue, IList<ComponentValue>, string>> _xsMeasuresAndAttributeValues;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappedXsValues" /> class.
        /// </summary>
        /// <param name="info">
        ///     The info.
        /// </param>
        public MappedXsValues(DataRetrievalInfo info)
        {
            for (int i = 0; i < _dimensionValues.Length; i++)
            {
                this._dimensionValues[i] = new List<ComponentValue>();
                this._attributeValues[i] = new List<ComponentValue>();
                this._keyValues[i] = new List<ComponentValue>();
            }

            var crossSectionalDataStructureObject = info.BaseDataQuery.DataStructure as ICrossSectionalDataStructureObject;
            if (crossSectionalDataStructureObject == null)
            {
                throw new DataRetrieverException("Mapping XS measures without XS DSD is not supported:" + info.BaseDataQuery.DataStructure.Urn);
            }

            var componentIndexMap = BuildComponentIdWithIndexMap(crossSectionalDataStructureObject);

            this.SetComponentOrder(info.MainComponentMappings, componentIndexMap);
            if (info.TimeTranscoder != null)
            {
                var timeDimensionComponent = this.SetTimeDimensionComponent(crossSectionalDataStructureObject.TimeDimension);
                this.HandleTimeDimension(timeDimensionComponent, componentIndexMap);
            }

            this._xsMeasuresAndAttributeValues = this.BuildAttachmentMeasureLookup(crossSectionalDataStructureObject);

            for (int i = 0; i < this._keyValues.Length; i++)
            {
                List<ComponentValue> keyValue = this._keyValues[i];
                this._previousKeyValues[i] = new string[keyValue.Count];
            }
        }

        /// <summary>
        ///     Gets the Attributes attached to data set level
        /// </summary>
        public IEnumerable<ComponentValue> DataSetLevelAttributeValues
        {
            get
            {
                return this._attributeValues[DataSetIndex];
            }
        }

        /// <summary>
        ///     Gets the Dimensions attached to data set level
        /// </summary>
        public IEnumerable<ComponentValue> DataSetLevelDimensionValues
        {
            get
            {
                return this._dimensionValues[DataSetIndex];
            }
        }

        /// <summary>
        ///     Gets the Attributes attached to group level
        /// </summary>
        public IEnumerable<ComponentValue> GroupLevelAttributeValues
        {
            get
            {
                return this._attributeValues[GroupIndex];
            }
        }

        /// <summary>
        ///     Gets the Dimensions attached to group level
        /// </summary>
        public IEnumerable<ComponentValue> GroupLevelDimensionValues
        {
            get
            {
                return this._dimensionValues[GroupIndex];
            }
        }

        /// <summary>
        ///     Gets the Attributes attached to group level
        /// </summary>
        public IEnumerable<ComponentValue> ObservationLevelAttributeValues
        {
            get
            {
                return this._attributeValues[ObservationIndex];
            }
        }

        /// <summary>
        ///     Gets the Dimensions attached to group level
        /// </summary>
        public IEnumerable<ComponentValue> ObservationLevelDimensionValues
        {
            get
            {
                return this._dimensionValues[ObservationIndex];
            }
        }

        /// <summary>
        ///     Gets the Attributes attached to section level
        /// </summary>
        public IEnumerable<ComponentValue> SectionAttributeValues
        {
            get
            {
                return this._attributeValues[SectionIndex];
            }
        }

        /// <summary>
        ///     Gets the Dimensions attached to section level
        /// </summary>
        public IEnumerable<ComponentValue> SectionLevelDimensionValues
        {
            get
            {
                return this._dimensionValues[SectionIndex];
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether dataset has been parsed.
        /// </summary>
        public bool StartedDataSet
        {
            get
            {
                return this._startedLevel[DataSetIndex];
            }

            set
            {
                this._startedLevel[DataSetIndex] = value;
            }
        }

        /// <summary>
        /// Gets the XS measure value with attributes.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <returns>A tuple with XS Measure value, the list of observation attributes values attached to this measure and finally the cross sectional measure code </returns>
        /// <exception cref="DataRetrieverException">Missing mapping for cross sectional measure</exception>
        public Tuple<ComponentValue, IList<ComponentValue>, string> GetXSMeasureValueWithAttributes(IComponent component)
        {
            Tuple<ComponentValue, IList<ComponentValue>, string> tuple;
            if (this._xsMeasuresAndAttributeValues.TryGetValue(component.Id, out tuple))
            {
                return tuple;
            }

            throw new DataRetrieverException("Missing mapping for cross sectional measure");
        }

        /// <summary>
        ///     Gets a value indicating whether this is a new group key or it is the same as the last one.
        /// </summary>
        /// <returns>
        ///     a value indicating whether this is a new group key or it is the same as the last one.
        /// </returns>
        public bool IsNewGroupKey()
        {
            bool newKeyAtLevel = this.IsNewKeyAtLevel(GroupIndex);
            if (newKeyAtLevel)
            {
                this._startedLevel[SectionIndex] = false;
            }

            return newKeyAtLevel;
        }

        /// <summary>
        ///     Gets a value indicating whether this is a new group key or it is the same as the last one.
        /// </summary>
        /// <returns>
        ///     a value indicating whether this is a new group key or it is the same as the last one.
        /// </returns>
        public bool IsNewSectionKey()
        {
            return this.IsNewKeyAtLevel(SectionIndex);
        }

        /// <summary>
        /// Handle Time Dimension components
        /// </summary>
        /// <param name="keyedValue">The time dimension <see cref="ComponentValue" /></param>
        /// <param name="componentIdToIndexMap">The component identifier to index map.</param>
        private void HandleTimeDimension(ComponentValue keyedValue, Dictionary<string, int> componentIdToIndexMap)
        {
            int index;
            if (componentIdToIndexMap.TryGetValue(keyedValue.Key.Id, out index))
            {
                this._keyValues[index].Add(keyedValue);
                this._dimensionValues[index].Add(keyedValue);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the <see cref="ComponentValue.Value" /> in <see cref="_keyValues" /> equal to
        ///     <see cref="_previousKeyValues" /> at the specified <paramref name="index" />
        /// </summary>
        /// <param name="index">
        ///     The attachment level index, <see cref="DataSetIndex" /> <see cref="GroupIndex"/>, <see cref="SectionIndex"/> and <see cref="ObservationIndex"/> .
        /// </param>
        /// <returns>
        ///     a value indicating whether the <see cref="ComponentValue.Value" /> in <see cref="_keyValues" /> equal to
        ///     <see cref="_previousKeyValues" /> at the specified <paramref name="index" />
        /// </returns>
        private bool IsNewKeyAtLevel(int index)
        {
            List<ComponentValue> currentKeyValues = this._keyValues[index];
            string[] previousKeyValues = this._previousKeyValues[index];

            if (!this._startedLevel[index])
            {
                CopyValues(currentKeyValues, previousKeyValues);
                this._startedLevel[index] = true;
                return true;
            }

            bool ret = !EqualKeyValues(currentKeyValues, previousKeyValues);
            if (ret)
            {
                CopyValues(currentKeyValues, previousKeyValues);
            }

            return ret;
        }

        /// <summary>
        /// Builds the component identifier with index map.
        /// </summary>
        /// <param name="crossDsd">
        /// The cross DSD.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary{String, Int32}"/>.
        /// </returns>
        private Dictionary<string, int> BuildComponentIdWithIndexMap(ICrossSectionalDataStructureObject crossDsd)
        {
            var map = new Dictionary<string, int>(StringComparer.Ordinal);
            foreach (var component in crossDsd.GetCrossSectionalAttachDataSet(true))
            {
                map.Add(component.Id, DataSetIndex);
            }

            foreach (var component in crossDsd.GetCrossSectionalAttachGroup(true))
            {
                map.Add(component.Id, GroupIndex);
            }

            foreach (var component in crossDsd.GetCrossSectionalAttachSection(true))
            {
                map.Add(component.Id, SectionIndex);
            }

            foreach (var component in crossDsd.GetCrossSectionalAttachObservation())
            {
                map.Add(component.Id, ObservationIndex);
            }

            foreach (var crossSectionalMeasure in crossDsd.CrossSectionalMeasures)
            {
                map.Add(crossSectionalMeasure.Id, ObservationIndex);
            }

            map.Add(crossDsd.PrimaryMeasure.Id, ObservationIndex);

            // without level
            foreach (var source in crossDsd.Components.Where(component => !map.ContainsKey(component.Id)))
            {
                var dimension = source as IDimension;
                if (dimension != null)
                {

                    if (dimension.Equals(crossDsd.TimeDimension) || dimension.Equals(crossDsd.FrequencyDimension))
                    {
                        map.Add(source.Id, GroupIndex);
                    }
                    else if (crossDsd.IsMeasureDimension(dimension))
                    {
                        map.Add(source.Id, ObservationIndex);
                    }
                }
            }

            return map;
        }

        /// <summary>
        /// Builds the attachment measure lookup.
        /// </summary>
        /// <param name="dataStructureObject">The data structure object.</param>
        /// <returns>The tuples</returns>
        private Dictionary<string, Tuple<ComponentValue, IList<ComponentValue>, string>> BuildAttachmentMeasureLookup(ICrossSectionalDataStructureObject dataStructureObject)
        {
            var tuples = new Dictionary<string, Tuple<ComponentValue, IList<ComponentValue>, string>>(StringComparer.Ordinal);
            if (_xsMeasureValues.Count == 0)
            {
                return tuples;
            }
 
            foreach (var crossSectionalMeasure in dataStructureObject.CrossSectionalMeasures)
            {
                Tuple<ComponentValue, IList<ComponentValue>, string> tuple;
                if (!tuples.TryGetValue(crossSectionalMeasure.Id, out tuple))
                {
                    var xsMeasureValue = _xsMeasureValues.First(value => value.Key.Id.Equals(crossSectionalMeasure.Id));
                    tuple = Tuple.Create(xsMeasureValue, (IList<ComponentValue>)new List<ComponentValue>(), crossSectionalMeasure.Code);
                    tuples.Add(crossSectionalMeasure.Id, tuple);
                }
            }

            foreach (var componentValue in _attributeValues[ObservationIndex])
            {
                var crossSectionalMeasures = dataStructureObject.GetAttachmentMeasures((IAttributeObject)componentValue.Key);
                foreach (var crossSectionalMeasure in crossSectionalMeasures)
                {
                    var tuple = tuples[crossSectionalMeasure.Id];
                    tuple.Item2.Add(componentValue);
                }
            }

            return tuples;
        }

        /// <summary>
        /// Initialize the internal order of the components based on the specified <paramref name="componentMappings" />
        /// </summary>
        /// <param name="componentMappings">The component mappings</param>
        /// <param name="componentIndexMap">The component index map.</param>
        private void SetComponentOrder(IList<MappingInfo> componentMappings, Dictionary<string, int> componentIndexMap)
        {
            foreach (var componentMapping in componentMappings)
            {
                int index;
                if (componentIndexMap.TryGetValue(componentMapping.Component.Id, out index))
                {
                    var componentValue = new ComponentValue(componentMapping.Component);
                    this.ComponentValues.Add(componentValue);
                    switch (componentMapping.Component.StructureType.EnumType)
                    {
                        case SdmxStructureEnumType.Dimension:
                            this._keyValues[index].Add(componentValue);
                            this._dimensionValues[index].Add(componentValue);
                            break;
                        case SdmxStructureEnumType.MeasureDimension:
                            this.MeasureDimensionValue = componentValue;
                            break;
                            case SdmxStructureEnumType.DataAttribute:
                            this._attributeValues[index].Add(componentValue);
                            break;
                            case SdmxStructureEnumType.PrimaryMeasure:
                            this.PrimaryMeasureValue = componentValue;
                            break;
                        case SdmxStructureEnumType.CrossSectionalMeasure:
                            this._xsMeasureValues.Add(componentValue);
                            break;
                    }
                }
            }
        }
    }
}