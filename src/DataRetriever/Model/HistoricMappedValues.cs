using System;
using System.Collections.Generic;
using System.Data;

namespace Estat.Sri.DataRetriever.Model
{
    internal class HistoricMappedValues
    {
        public KeyValuePair<string, string> UpdateStatus { get; set; }
        public KeyValuePair<string, string> ValidFrom { get; set; }
        public KeyValuePair<string, string> ValidTo { get; set; }

        public void UpdateFromReader(IDataReader reader)
        {
            if (!string.IsNullOrWhiteSpace(UpdateStatus.Key))
            {
                UpdateStatus = new KeyValuePair<string, string>(UpdateStatus.Key,reader[UpdateStatus.Key].ToString());
            }

            if (!string.IsNullOrWhiteSpace(ValidFrom.Key))
            {
                ValidFrom = new KeyValuePair<string, string>(ValidFrom.Key,reader[ValidFrom.Key].ToString());
            }

            if (!string.IsNullOrWhiteSpace(ValidTo.Key))
            {
                ValidTo = new KeyValuePair<string, string>(ValidTo.Key, reader[ValidTo.Key].ToString());
            }
             
        }
    }
}