// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfo.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;

    using Estat.Nsi.DataRetriever.Builders;
    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util.Extensions;

    /// <summary>
    ///     This class holds the current state of the data retrieval
    /// </summary>
    internal class DataRetrievalInfo
    {
        /// <summary>
        ///     The _all dimensions
        /// </summary>
        private static readonly string _allDimensions =
            Org.Sdmxsource.Sdmx.Api.Constants.DimensionAtObservation.GetFromEnum(DimensionAtObservationEnumType.All)
                .Value;

        /// <summary>
        ///     The logger
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DataRetrievalInfo));

        /// <summary>
        ///     The Mapping Store connection string settings
        /// </summary>
        protected readonly DataRetrieverSettings _settings;

        /// <summary>
        ///     Cross Sectional measures
        /// </summary>
        private readonly List<MappingInfo> _crossSectionalMeasureMappings = new List<MappingInfo>();

        /// <summary>
        ///     A value indicating whether the time period is the dimension at observation.
        /// </summary>
        private readonly bool _isTimePeriodAtObservation;

        /// <summary>
        ///     The mapping set of the dataflow found in the SDMX query
        /// </summary>
        private readonly IComponentMappingContainer _mappingSet;

        /// <summary>
        ///     The Measure dimension SDMX Query values if any
        /// </summary>
        private readonly List<string> _measureDimensionQueryValues = new List<string>();

        /// <summary>
        ///     The _schema version
        /// </summary>
        private readonly SdmxSchemaEnumType _schemaVersion;

        /// <summary>
        ///     This field holds the XSMeasureMapping which are used in case they are mapped instead of Measure Dimension
        /// </summary>
        private readonly Dictionary<string, MappingInfo> _xsMeasureMappings = new Dictionary<string, MappingInfo>(StringComparer.Ordinal);

        /// <summary>
        /// The parameters
        /// </summary>
        private readonly IList<DbParameter> _parameters = new List<DbParameter>();

        /// <summary>
        /// The mapping information identifier map
        /// </summary>
        private readonly Dictionary<string, MappingInfo> _mappingInfoIdMap = new Dictionary<string, MappingInfo>(StringComparer.Ordinal);

        /// <summary>
        /// The component mapping entities
        /// </summary>
        private Dictionary<string, IMappingEntity> _componentMappingEntities;

        /// <summary>
        ///     The list of component mappings
        /// </summary>
        private MappingInfo[] _componentMappingList;

        /// <summary>
        ///     The _dimension at observation mapping
        /// </summary>
        private IComponentMappingBuilder _dimensionAtObservationMapping;

        /*
        /// <summary>
        /// The "Data Structure Definition" used for the produced Dataset model.
        /// It is a <see cref="Estat.Sdmx.Model.Structure.KeyFamilyBean"/> object
        /// as defined in the package sdmx_model.
        /// </summary>
        private KeyFamilyBean _keyFamilyBean;
*/

        /// <summary>
        ///     This field holds if the no of records read from DDB
        /// </summary>
        private int _recordsRead;

        /// <summary>
        ///     Holds the final SQL string
        /// </summary>
        private string _sqlString;

        /// <summary>
        ///     Holds the final metadata SQL string
        /// </summary>
        private string _metadataSqlString;

        /// <summary>
        ///     Holds the final metadata availability SQL string
        /// </summary>
        private string _metadataAvailabilitySqlString;

        /// <summary>
        ///     Value indicating whether the confidentiality status of observations should be verified.
        /// </summary>
        private readonly bool _validateConfidentialityStatusValues;

        /// <summary>
        ///     Value indicating whether the user is authorized to read confidential observations.
        /// </summary>
        private readonly bool _canReadConfidentialObservations;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataRetrievalInfo" /> class.
        /// </summary>
        /// <param name="mappingSet">
        ///     The mapping set of the dataflow found in the sdmx query
        /// </param>
        /// <param name="query">
        ///     The current SDMX Query object
        /// </param>
        /// <param name="settings">
        ///     The Mapping Store connection string settings
        /// </param>
        public DataRetrievalInfo(
            IComponentMappingContainer mappingSet, 
            IDataQuery query,
            DataRetrieverSettings settings)
        {
            this._schemaVersion = settings.SdmxSchemaVersion;
            this._settings = settings;
            if (mappingSet == null)
            {
                throw new ArgumentNullException("mappingSet");
            }

            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            this._mappingSet = mappingSet;
            this.Query = query;
            this._settings = settings;
            this._schemaVersion = settings.SdmxSchemaVersion;

            // default may be overriden elsewhere
            this.CurrentObservationAction = ObservationAction.GetFromEnum(ObservationActionEnumType.Active);
            // in some cases nsiws seems to set the schema version to csv, json instead of 2.1 or 3.0 (to be fixed)
            if (!_schemaVersion.IsOneOf(SdmxSchemaEnumType.VersionTwo, SdmxSchemaEnumType.Edi))
            {
                this._isTimePeriodAtObservation =
                    DimensionObject.TimeDimensionFixedId.Equals(query.DimensionAtObservation);
                if (!this._isTimePeriodAtObservation)
                {
                    this.DimensionAtObservation = query.DimensionAtObservation;
                }

                this.Limit = 0; // REST does not support default limit.
            }
            else
            {
                this.Limit = query.FirstNObservations.HasValue ? query.FirstNObservations.Value : 0;
                this._isTimePeriodAtObservation = true;
            }

            this.BuildMappings();
            this.EffectiveDimensionAtObservation = this.BuildEffectiveDimensionAtObservation();
            
            if (settings.ConfidentialityStatusValues != null &&
                settings.ConfidentialityStatusValues.Any() &&
                query.DataStructure.Attributes.FirstOrDefault(x => x.Id.Equals("CONF_STATUS", StringComparison.OrdinalIgnoreCase)) != null)
            {
                _validateConfidentialityStatusValues = true;
                _canReadConfidentialObservations = Thread.CurrentPrincipal.IsInRole("CanReadPitData");
            }
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataRetrievalInfo" /> class.
        /// </summary>
        /// <param name="mappingSet">
        ///     The mapping set of the dataflow found in the SDMX query
        /// </param>
        /// <param name="query">
        ///     The current SDMX Query object
        /// </param>
        /// <param name="settings">
        ///     The Mapping Store connection string settings
        /// </param>
        public DataRetrievalInfo(
            IComponentMappingContainer mappingSet, 
            IComplexDataQuery query,
            DataRetrieverSettings settings)
        {
            if (mappingSet == null)
            {
                throw new ArgumentNullException("mappingSet");
            }

            if (settings == null)
            {
                throw new ArgumentNullException("settings");
            }

            this._schemaVersion = SdmxSchemaEnumType.VersionTwoPointOne;
            this._mappingSet = mappingSet;
            this.ComplexQuery = query;
            this.CurrentObservationAction = new ActionBuilder().Build(mappingSet, query).First();
            this._settings = settings;
            this.Limit = query.DefaultLimit.HasValue ? query.DefaultLimit.Value : 0;
            this._isTimePeriodAtObservation = DimensionObject.TimeDimensionFixedId.Equals(query.DimensionAtObservation);
            if (!this._isTimePeriodAtObservation)
            {
                this.DimensionAtObservation = query.DimensionAtObservation;
            }

            this.BuildMappings();
            this.EffectiveDimensionAtObservation = this.BuildEffectiveDimensionAtObservation();
        }

        /// <summary>
        ///     Gets the main component
        /// </summary>
        public virtual IList<MappingInfo> MainComponentMappings
        {
            get
            {
                return this._componentMappingList;
            }
        }

        /// <summary>
        ///     Gets the base data query.
        /// </summary>
        /// <value>
        ///     The base data query.
        /// </value>
        public IBaseDataQuery BaseDataQuery
        {
            get
            {
                return (IBaseDataQuery)this.Query ?? this.ComplexQuery;
            }
        }

        /// <summary>
        ///     Gets the current SDMX Query object
        /// </summary>
        public IComplexDataQuery ComplexQuery { get; private set; }

        /// <summary>
        ///     Gets the map between component concept id and the MappingInfo. Used in getting the MappingInfo from SDMX-ML
        ///     Query clauses
        /// </summary>
        public Dictionary<string, MappingInfo> ComponentIdMap
        {
            get
            {
                return _mappingInfoIdMap;
            }
        }

        /// <summary>
        ///     Gets a dictionary to find the mapping of each DSD component
        /// </summary>
        public IDictionary<string, IMappingEntity> ComponentMapping
        {
            get
            {
                return this._componentMappingEntities;
            }
        }

        /// <summary>
        ///     Gets the Mapping Store DataBase Connection String Settings.
        /// </summary>
        public DataRetrieverSettings Settings
        {
            get
            {
                return this._settings;
            }
        }

        /// <summary>
        ///     Gets the current Cross Sectional Measure mappings
        /// </summary>
        public IList<MappingInfo> CrossSectionalMeasureMappings
        {
            get
            {
                return this._crossSectionalMeasureMappings;
            }
        }

        /// <summary>
        ///     Gets or sets the default SDMX Header
        /// </summary>
        public IHeader DefaultHeader { get; set; }

        /// <summary>
        ///     Gets or sets the dataset header
        /// </summary>
        public IDatasetHeader DatasetHeader { get; set; }

        /// <summary>
        ///     Gets the dimension at observation
        /// </summary>
        public string DimensionAtObservation { get; private set; }

        /// <summary>
        ///     Gets the dimension at observation mapping
        /// </summary>
        public IComponentMappingBuilder DimensionAtObservationMapping
        {
            get
            {
                return this._dimensionAtObservationMapping;
            }
        }

        /// <summary>
        ///     Gets the effective dimension at observation.
        /// </summary>
        /// <value>
        ///     The effective dimension at observation.
        /// </value>
        public string EffectiveDimensionAtObservation { get; private set; }

        /// <summary>
        ///     Gets the first N observations per series.
        /// </summary>
        /// <value>
        ///     The first N observations per series.
        /// </value>
        public int FirstNObservations
        {
            get
            {
                return this.BaseDataQuery.FirstNObservations.HasValue ? this.BaseDataQuery.FirstNObservations.Value : -1;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has first and last n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has first and last n observations; otherwise, <c>false</c>.
        /// </value>
        public bool HasFirstAndLastNObservations
        {
            get
            {
                return this.HasFirstNObservations && this.HasLastNObservations;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has first n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has first n observations; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasFirstNObservations
        {
            get
            {
                return (this._schemaVersion == SdmxSchemaEnumType.VersionTwoPointOne || this._schemaVersion == SdmxSchemaEnumType.VersionThree) && this.FirstNObservations > -1;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has last n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has last n observations; otherwise, <c>false</c>.
        /// </value>
        public virtual bool HasLastNObservations
        {
            get
            {
                return (this._schemaVersion == SdmxSchemaEnumType.VersionTwoPointOne || this._schemaVersion == SdmxSchemaEnumType.VersionThree) && this.LastNObservations > -1;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has max OBS per series.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has max OBS per series; otherwise, <c>false</c>.
        /// </value>
        public bool HasMaxObsPerSeries
        {
            get
            {
                return this.HasFirstNObservations || this.HasLastNObservations;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance is all dimensions.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is all dimensions; otherwise, <c>false</c>.
        /// </value>
        public bool IsAllDimensions
        {
            get
            {
                return _allDimensions.Equals(this.DimensionAtObservation);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the time period is the dimension at observation.
        /// </summary>
        public bool IsTimePeriodAtObservation
        {
            get
            {
                return this._isTimePeriodAtObservation
                       || this.EffectiveDimensionAtObservation.Equals(DimensionObject.TimeDimensionFixedId);
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the current dataset is truncated
        /// </summary>
        /// <remarks>
        ///     TODO This is not currently supported.
        /// </remarks>
        public bool IsTruncated { get; set; }

        /// <summary>
        ///     Gets the last N observations per series.
        /// </summary>
        /// <value>
        ///     The last N observations per series.
        /// </value>
        public int LastNObservations
        {
            get
            {
                return this.BaseDataQuery.LastNObservations.HasValue ? this.BaseDataQuery.LastNObservations.Value : -1;
            }
        }

        /// <summary>
        ///     Gets or sets the current limit. Defaults to <see cref="Query" /> 's <see cref="IComplexDataQuery.DefaultLimit" />
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        ///     Gets the mapping set of the dataflow found in the sdmx query
        /// </summary>
        public IComponentMappingContainer MappingSet
        {
            get
            {
                return this._mappingSet;
            }
        }

        /// <summary>
        ///     Gets or sets the maximum allowed number observations. This is not related to (Complex)DataQuery First/Last
        ///     observation or
        ///     Default limit.
        ///     This is related to configurable allowed number of observations per request at server side.
        ///     Set to zero (0) to disable it.
        /// </summary>
        /// <value>The maximum allowed number observations.</value>
        public int MaximumAllowedNumberObservations { get; set; }

        /// <summary>
        ///     Gets the Measure Component
        /// </summary>
        public IDimension MeasureComponent { get; private set; }

        /// <summary>
        ///     Gets the current SDMX Query object
        /// </summary>
        public IDataQuery Query { get; private set; }

        /// <summary>
        ///     Gets or sets a value indicating whether the no of records read from DDB
        /// </summary>
        /// <remarks>
        ///     TODO This is not currently supported.
        /// </remarks>
        public int RecordsRead
        {
            get
            {
                return this._recordsRead;
            }

            set
            {
                this._recordsRead = value;
                TotalRecords += this._recordsRead;
            }
        }

        /// <summary>
        /// Gets or sets the total records writen for all datasets
        /// </summary>
        public int TotalRecords { get; set; }

        /// <summary>
        ///     Gets or sets the final SQL string
        /// </summary>
        public string SqlString
        {
            get
            {
                return this._sqlString;
            }

            set
            {
                _logger.DebugFormat("DDB SQL Set to: {0}", value);
                this._sqlString = value;
            }
        }

        /// <summary>
        ///     Gets or sets the final metadata SQL string
        /// </summary>
        public string MetadataSqlString
        {
            get
            {
                return this._metadataSqlString;
            }

            set
            {
                _logger.DebugFormat("DDB Metadata SQL Set to: {0}", value);
                this._metadataSqlString = value;
            }
        }

        /// <summary>
        ///     Gets or sets the final metadata availability SQL string
        /// </summary>
        public string MetadataAvailabilitySqlString
        {
            get
            {
                return this._metadataAvailabilitySqlString;
            }

            set
            {
                _logger.DebugFormat("DDB Metadata availability SQL Set to: {0}", value);
                this._metadataAvailabilitySqlString = value;
            }
        }

        /// <summary>
        ///     Gets the object used to store the mapping of the time dimension
        /// </summary>
        public TimeDimensionMappingEntity TimeMapping
        {
            get
            {
                return this._mappingSet.TimeDimensionMapping;
            }
        }

        /// <summary>
        ///     Gets the object that is used for transcoding the time fields
        /// </summary>
        public ITimeDimensionFrequencyMappingBuilder TimeTranscoder
        {
            get
            {
                return this._mappingSet.TimeDimensionBuilder;
            }
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IList<DbParameter> Parameters
        {
            get
            {
                return this._parameters;
            }
        }

        /// <summary>
        /// Gets or sets the current observation action
        /// </summary>
        public ObservationAction CurrentObservationAction { get; set; }

        /// <summary>
        ///     Gets a value indicating whether the confidentiality status of observations should be verified.
        /// </summary>
        public bool ValidateConfidentialityStatusValues
        {
            get
            {
                return this._validateConfidentialityStatusValues;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether the user is authorized to read confidential observations.
        /// </summary>
        public bool CanReadConfidentialObservations
        {
            get
            {
                return this._canReadConfidentialObservations;
            }
        }

        /// <summary>
        ///     Getter for the list of available XS Measures in case the XS Measures are mapped
        /// </summary>
        /// <returns>
        ///     A list of available XS Measures Mappings or null in case they were not mapped
        /// </returns>
        public IList<MappingInfo> BuildXSMeasures()
        {
            if (this._crossSectionalMeasureMappings.Count > 0)
            {
                return this._crossSectionalMeasureMappings;
            }

            List<MappingInfo> collection = null;
            if (this.MeasureComponent != null)
            {
                if (this._measureDimensionQueryValues.Count == 0)
                {
                    this._measureDimensionQueryValues.AddRange(
                        GetFromQueryDimensionValues(this.Query, this.MeasureComponent));
                }

                collection = new List<MappingInfo>();
                if (this._measureDimensionQueryValues.Count > 0)
                {
                    // only in case the measure dimension is not mapped and there are conditions for measure dimension
                    foreach (string queryValue in this._measureDimensionQueryValues)
                    {
                        MappingInfo mapping;
                        if (this._xsMeasureMappings.TryGetValue(queryValue, out mapping))
                        {
                            collection.Add(mapping);
                        }
                    }
                }
                else
                {
                    collection.AddRange(this._xsMeasureMappings.Values);
                }

                // cache results
                this._crossSectionalMeasureMappings.AddRange(collection);
            }

            return collection;
        }

        /// <summary>
        ///     Gets the list of <paramref name="dimension" /> values inside a <paramref name="query" />
        /// </summary>
        /// <param name="query">
        ///     The SDMX Model query
        /// </param>
        /// <param name="dimension">
        ///     The <see cref="IDimension" /> of a dimension
        /// </param>
        /// <returns>
        ///     the list of <paramref name="dimension" /> values inside a <paramref name="query" />
        /// </returns>
        private static IEnumerable<string> GetFromQueryDimensionValues(IDataQuery query, IDimension dimension)
        {
            var xsMeasureValues = new List<string>();

            foreach (IDataQuerySelectionGroup sg in query.SelectionGroups)
            {
                if (sg.HasSelectionForConcept(dimension.Id))
                {
                    var selection = sg.GetSelectionsForConcept(dimension.Id);
                    if (selection.HasMultipleValues)
                    {
                        xsMeasureValues.AddRange(selection.Values);
                    }
                    else
                    {
                        xsMeasureValues.Add(selection.Value);
                    }
                }
            }

            return xsMeasureValues;
        }

        /// <summary>
        ///     Builds the effective dimension at observation.
        /// </summary>
        /// <returns>The effective dimension at observation.</returns>
        private string BuildEffectiveDimensionAtObservation()
        {
            if (this._isTimePeriodAtObservation)
            {
                return DimensionObject.TimeDimensionFixedId;
            }

            if (_allDimensions.Equals(this.DimensionAtObservation))
            {
                if (this.TimeMapping != null)
                {
                    return DimensionObject.TimeDimensionFixedId;
                }

                return this.BaseDataQuery.DataStructure.GetDimensions().Last().Id;
            }

            return this.DimensionAtObservation;
        }

        /// <summary>
        ///     Builds the component to Mapping dictionary. Before building it the methods make sure that the dictionary is empty
        /// </summary>
        private void BuildMappings()
        {
            var dataStructure = this.BaseDataQuery.DataStructure;
            var metaDataStructure = this.BaseDataQuery.MetadataStructure;
            var metadataAttributeList = metaDataStructure?.GetMetadataAttributes();
            var measureComponent = dataStructure.GetDimensions(SdmxStructureEnumType.MeasureDimension).FirstOrDefault();

            // we set the MeasureComponent only if MeasureDimension is not mapped
            if (measureComponent != null)
            {
                if (!this._mappingSet.ComponentMappings.ContainsKey(measureComponent.Id))
                {
                    this.MeasureComponent = measureComponent;
                }
            }

            IComponentMappingBuilder dimensionAtObservationMapping;
            var dimensionAtObservation = this.DimensionAtObservation;
            if (dimensionAtObservation != null && this._mappingSet.ComponentMappingBuilders.TryGetValue(dimensionAtObservation, out dimensionAtObservationMapping))
            {
                this._dimensionAtObservationMapping = dimensionAtObservationMapping;
            }

            foreach (var componentMappingBuilder in this._mappingSet.ComponentMappingBuilders)
            {
                var component = dataStructure.GetComponent(componentMappingBuilder.Key);
                ComponentMappingEntity mapping;
                if (component != null && this._mappingSet.ComponentMappings.TryGetValue(componentMappingBuilder.Key, out mapping))
                {
                    this._mappingInfoIdMap.Add(component.Id, new MappingInfo(componentMappingBuilder.Value, mapping, component));
                }

                if (metaDataStructure != null)
                {
                    var metadataAttribute = metadataAttributeList.FirstOrDefault(x => x.GetFullIdPath(false).Equals(componentMappingBuilder.Key));

                    ComponentMappingEntity metadataMapping;
                    if (metadataAttribute != null && this._mappingSet.ComponentMappings.TryGetValue(componentMappingBuilder.Key, out metadataMapping))
                    {
                        this._mappingInfoIdMap.Add(metadataAttribute.GetFullIdPath(false), new MappingInfo(componentMappingBuilder.Value, metadataMapping, metadataAttribute));
                    }
                }
            }

            Debug.Assert(_mappingInfoIdMap.Count == _mappingSet.ComponentMappingBuilders.Count, "Possible bug in MAAPI ComponentMappingManager, the IComponentMappingContainer.ComponentMappingBuilder doesn't have the same length as the mappings");

            this._componentMappingList = _mappingInfoIdMap.Values.ToArray();

            var cross = dataStructure as ICrossSectionalDataStructureObject;
            if (cross != null)
            {
                foreach (var crossSectionalMeasure in cross.CrossSectionalMeasures)
                {
                    MappingInfo xsMeausure;
                    if (_mappingInfoIdMap.TryGetValue(crossSectionalMeasure.Id, out xsMeausure))
                    {
                        this._xsMeasureMappings.Add(crossSectionalMeasure.Code, xsMeausure);
                    }
                }
            }

            _componentMappingEntities = new Dictionary<string, IMappingEntity>(this._mappingSet.ComponentMappings.ToDictionary(pair => pair.Key, pair => (IMappingEntity)pair.Value), StringComparer.Ordinal);
            if (dataStructure.TimeDimension != null)
            {
                _componentMappingEntities.Add(dataStructure.TimeDimension.Id, _mappingSet.TimeDimensionMapping);
            }
        }

        public Tuple<long?, long?> Range => this._settings.Range;

        /// <summary>
        /// Gets a value indicating whether [should include history].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [should include history]; otherwise, <c>false</c>.
        /// </value>
        public bool ShouldIncludeHistory => Query != null && Query.IncludeHistory && MappingSet.HasHistoricMappings;
    }
}