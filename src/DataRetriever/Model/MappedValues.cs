// -----------------------------------------------------------------------
// <copyright file="MappedValues.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sri.DataRetriever.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

    /// <summary>
    ///     Holds the mapped and transcoded values following the series structure
    /// </summary>
    internal class MappedValues : MappedValuesBase, IMappedValues
    {
        #region Fields

        /// <summary>
        ///     dataset attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeDataSetValues = new List<ComponentValue>();

        /// <summary>
        ///     measure values
        /// </summary>
        private readonly List<ComponentValue> _measureValues = new List<ComponentValue>();

        /// <summary>
        ///     group attribute values. It should contain only the attributes of a specific group.
        /// </summary>
        private readonly List<ComponentValue> _attributeGroupValues = new List<ComponentValue>();

        /// <summary>
        ///     OBS attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeObservationValues = new List<ComponentValue>();

        /// <summary>
        ///     series attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeSeriesValues = new List<ComponentValue>();

        /// <summary>
        ///     metadata attribute values
        /// </summary>
        private readonly List<ComponentValue> _attributeMetadataValues = new List<ComponentValue>();

        /// <summary>
        ///     All dimension values
        /// </summary>
        private readonly List<ComponentValue> _dimensionValues = new List<ComponentValue>();

        /// <summary>
        ///     The dimension key values
        /// </summary>
        private readonly List<ComponentValue> _keyValues = new List<ComponentValue>();

        /// <summary>
        ///     The previous dataset attributes
        /// </summary>
        private readonly string[] _previousAttributeDataSetValues;

        /// <summary>
        ///     The previous key values
        /// </summary>
        private readonly string[] _previousKeyValues;

        /// <summary>
        /// The previous historic values
        /// </summary>
        private Tuple<string,string> _previousHistoricValues;

        /// <summary>
        ///     The list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        private readonly List<ComponentValue> _xsMeasureCachedSeriesAttribute = new List<ComponentValue>();

        /// <summary>
        ///     The list of all series dimension values minus the measure dimension. It is used only when XS measures are mapped.
        /// </summary>
        private readonly List<ComponentValue> _xsMeasureCachedSeriesKey = new List<ComponentValue>();

        /// <summary>
        ///     XS Measure cache. It is used only when XS measures are mapped.
        /// </summary>
        private readonly IDictionary<string, XsMeasureCache> _xsMeasureCaches = new Dictionary<string, XsMeasureCache>(StringComparer.Ordinal);

        /// <summary>
        ///     CrossSectional measure values
        /// </summary>
        private readonly List<ComponentValue> _xsMeasures = new List<ComponentValue>();

        /// <summary>
        ///     A value indicating whether the SDMX DSD Attributes attached to DataSet level have been processed.
        /// </summary>
        private bool _startedDataSet;

        

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappedValues" /> class.
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        /// <param name="componentMappings">
        ///     The component Mappings.
        /// </param>
        public MappedValues(DataRetrievalInfo info, IEnumerable<MappingInfo> componentMappings)
        {
            this.SetComponentOrder(componentMappings, info);
            this.SetTimeDimensionComponent(info.BaseDataQuery.DataStructure.TimeDimension);
            if (info.ShouldIncludeHistory)
            {
                this.SetHistoricValues(info);
            }
            
            if (info.TimeTranscoder != null && !info.IsTimePeriodAtObservation)
            {
                this._keyValues.Add(this.TimeDimensionValue);
            }

            this._previousKeyValues = new string[this._keyValues.Count];
            this._previousAttributeDataSetValues = new string[this._attributeDataSetValues.Count];
        }

        private void SetHistoricValues(DataRetrievalInfo info)
        {
            this.HistoricMappedValues = new HistoricMappedValues();
            if (info.MappingSet.UpdateStatusMapping != null)
            {
                this.HistoricMappedValues.UpdateStatus =
                    new KeyValuePair<string, string>(info.MappingSet.UpdateStatusMapping.Column.Name, string.Empty);
            }

            if (info.MappingSet.LastUpdateMapping != null)
            {
                this.HistoricMappedValues.ValidFrom = new KeyValuePair<string, string>(
                    info.MappingSet.LastUpdateMapping.Column.Name,
                    string.Empty);
            }

            if (info.MappingSet.ValidToMapping !=null)
            {
                this.HistoricMappedValues.ValidTo = new KeyValuePair<string, string>(
                    info.MappingSet.ValidToMapping.GetColumns().First().Name,
                    string.Empty);
            }
            
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MappedValues" /> class.
        /// </summary>
        /// <param name="info">
        ///     The current data retrieval state
        /// </param>
        public MappedValues(DataRetrievalInfo info)
            : this(info, info.MainComponentMappings)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the attributes attached to DataSet level
        /// </summary>
        public virtual IEnumerable<ComponentValue> AttributeDataSetValues
        {
            get
            {
                return this._attributeDataSetValues;
            }
        }

        /// <summary>
        ///     Gets the measures values. 
        /// </summary>
        /// <remarks>
        ///     SDMX 3.0.0 compliant. 
        ///     The primary measure in previous versions will be returned
        ///     if no other measures are found.
        ///     TODO: This property should eventually replace PrimaryMeasure property.
        /// </remarks>
        public virtual IEnumerable<ComponentValue> MeasureValues
        {
            get
            {
                if (this._measureValues.Any())
                {
                    return this._measureValues;
                }
                else
                {
                    return new List<ComponentValue>() { this.PrimaryMeasureValue };
                }
            }
        }

        /// <summary>
        ///     Gets group attribute values. To avoid extra checks, only attributes and dimensions of a specific group should be
        ///     added at <see cref="MappedValuesBase.Add" /> . So this should contain only the attributes of a specific group.
        /// </summary>
        public virtual IEnumerable<ComponentValue> AttributeGroupValues
        {
            get
            {
                return this._attributeGroupValues;
            }
        }

        /// <summary>
        ///     Get the metadata attributes
        /// </summary>
        public virtual IEnumerable<ComponentValue> AttributeMetadataValues
        {
            get
            {
                return this._attributeMetadataValues;
            }
        }

        /// <summary>
        ///     Gets the Observation attribute values
        /// </summary>
        public virtual IEnumerable<ComponentValue> AttributeObservationValues
        {
            get
            {
                return this._attributeObservationValues;
            }
        }

        /// <summary>
        ///     Gets the series attribute values
        /// </summary>
        public virtual IEnumerable<ComponentValue> AttributeSeriesValues
        {
            get
            {
                return this._attributeSeriesValues;
            }
        }

        /// <summary>
        ///     Gets all dimension values. This means either the series dimensions (all of them except time) or the dimensions of a
        ///     specific group.
        /// </summary>
        public IList<ComponentValue> DimensionValues
        {
            get
            {
                return this._dimensionValues;
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the SDMX DSD Attributes attached to DataSet level have been processed.
        /// </summary>
        public bool StartedDataSet
        {
            get
            {
                return this._startedDataSet;
            }

            set
            {
                this._startedDataSet = value;
            }
        }

        /// <summary>
        ///     Gets the list of XS Measure buffer. It is used only when XS measures are mapped.
        /// </summary>
        public IEnumerable<KeyValuePair<string, XsMeasureCache>> XSMeasureCaches
        {
            get
            {
                return this._xsMeasureCaches;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Add <paramref name="xsComponent" /> to buffer
        /// </summary>
        /// <param name="xsComponent">
        ///     The XS Measure to add to buffer
        /// </param>
        public void AddToBuffer(ICrossSectionalMeasure xsComponent)
        {
            XsMeasureCache xsMeasureCache;
            if (!this._xsMeasureCaches.TryGetValue(xsComponent.Id, out xsMeasureCache))
            {
                xsMeasureCache = new XsMeasureCache(this._xsMeasureCachedSeriesKey, this._xsMeasureCachedSeriesAttribute, xsComponent.Code);
                this._xsMeasureCaches.Add(xsComponent.Id, xsMeasureCache);
            }

            xsMeasureCache.XSMeasureCachedObservations.Add(new KeyValuePair<string, string>(this.TimeValue, this.GetXSMeasureValue(xsComponent)));
            xsMeasureCache.Attributes.Add(new List<ComponentValue>(this._attributeObservationValues));
        }

        /// <summary>
        ///     Initialize the buffers used for mappings with XS Measures mapped.
        /// </summary>
        public void InitXsBuffer()
        {
            this._xsMeasureCaches.Clear();

            // we need to copy to avoid overwriting with new values
            CopyComponentValues(this._dimensionValues, this._xsMeasureCachedSeriesKey);
            CopyComponentValues(this._attributeSeriesValues, this._xsMeasureCachedSeriesAttribute);
        }

        /// <summary>
        ///     Gets a value indicating whether this is a new key or it is the same as the last one.
        /// </summary>
        /// <returns>
        ///     a value indicating whether this is a new key or it is the same as the last one.
        /// </returns>
        public bool IsNewKey()
        {
            bool ret = !EqualKeyValues(this._keyValues, this._previousKeyValues);
            if (ret)
            {
                CopyValues(this._keyValues, this._previousKeyValues);
            }

            return ret;
        }

        public bool IsDatasetAttributesDifferent()
        {
            bool ret = !EqualKeyValues(this._attributeDataSetValues, this._previousAttributeDataSetValues);
            
            if (ret)
            {
                CopyValues(this._attributeDataSetValues, this._previousAttributeDataSetValues);
            }

            return ret;
        }

        public bool NewHistoricValues(string action)
        {
            if (_previousHistoricValues == null)
            {
                this._previousHistoricValues = new Tuple<string, string>(HistoricMappedValues.ValidFrom.Value, action);
                return true;
            }

            if (HistoricMappedValues.ValidFrom.Value != this._previousHistoricValues.Item1 ||
                action != this._previousHistoricValues.Item2)
            {
                this._previousHistoricValues = new Tuple<string, string>(HistoricMappedValues.ValidFrom.Value,action);
                return true;
            }

            return false;
        }
        #endregion

        #region Methods

        /// <summary>
        ///     Clear the <paramref name="destination" /> and copy all items from <paramref name="source" /> to
        ///     <paramref name="destination" />
        /// </summary>
        /// <param name="source">The source list of <see cref="ComponentValue" /></param>
        /// <param name="destination">The destination list of <see cref="ComponentValue" /></param>
        private static void CopyComponentValues(IEnumerable<ComponentValue> source, ICollection<ComponentValue> destination)
        {
            destination.Clear();
            foreach (var componentValue in source)
            {
                destination.Add(new ComponentValue(componentValue.Key) { Value = componentValue.Value });
            }
        }

        /// <summary>
        ///     Checks if the dimension at observation is in the dimension references of the component
        ///     <paramref name="componentValue" />
        /// </summary>
        /// <param name="componentValue">The component value</param>
        /// <param name="info">The data retrieval info</param>
        /// <returns>True if it is and false if not</returns>
        protected static bool IsDimensionObsReference(ComponentValue componentValue, DataRetrievalInfo info)
        {
            IBaseDataQuery baseDataQuery = (IBaseDataQuery)info.ComplexQuery ?? info.Query;

            foreach (IAttributeObject attr in baseDataQuery.DataStructure.AttributeList.Attributes)
            {
                if (attr.Id.Equals(componentValue.Key.Id))
                {
                    return attr.DimensionReferences.Contains(info.DimensionAtObservation ?? DimensionObject.TimeDimensionFixedId) ||
                           DatasetStructureReference.AllDimensions.Equals(info.DimensionAtObservation, StringComparison.OrdinalIgnoreCase);
                }
            }

            return false;
        }

        /// <summary>
        ///     Gets the value for the specified <paramref name="component" />
        /// </summary>
        /// <param name="component">
        ///     The CrossSectional measure
        /// </param>
        /// <returns>
        ///     the value for the specified <paramref name="component" />
        /// </returns>
        private string GetXSMeasureValue(ICrossSectionalMeasure component)
        {
            return this._xsMeasures.Find(x => x.Key.Id.Equals(component.Id)).Value;
        }

        /// <summary>
        ///     Initialize the internal order of the components based on the specified <paramref name="componentMappings" />
        /// </summary>
        /// <param name="componentMappings">
        ///     The component mappings
        /// </param>
        /// ///
        /// <param name="info">
        ///     The data retrieval info
        /// </param>
        private void SetComponentOrder(IEnumerable<MappingInfo> componentMappings, DataRetrievalInfo info)
        {
            foreach (var componentMapping in componentMappings)
            {
                var componentValue = new ComponentValue(componentMapping.Component);
                this.ComponentValues.Add(componentValue);
                switch (componentMapping.Component.StructureType.EnumType)
                {
                    case SdmxStructureEnumType.Dimension:
                    case SdmxStructureEnumType.MeasureDimension:
                        this._dimensionValues.Add(componentValue);
                        if (!componentValue.Key.Id.Equals(info.DimensionAtObservation))
                        {
                            this._keyValues.Add(componentValue);
                        }
                        break;
                    case SdmxStructureEnumType.DataAttribute:
                        AddAttribute(componentValue, componentMapping.Attribute.AttachmentLevel, info);
                        break;
                    case SdmxStructureEnumType.Measure:
                        AddMeasure(componentValue, info);
                        break;
                    case SdmxStructureEnumType.PrimaryMeasure:
                        this.PrimaryMeasureValue = componentValue;
                        if(info.Settings.SdmxSchemaVersion == SdmxSchemaEnumType.VersionThree)
                        {
                            //we are requesting a version 3.0.0 response for a 2.1 DSD
                            AddMeasure(componentValue, info);
                        }
                        break;
                    case SdmxStructureEnumType.CrossSectionalMeasure:
                        this._xsMeasures.Add(componentValue);
                        break;
                    case SdmxStructureEnumType.MetadataAttribute:
                        this._attributeMetadataValues.Add(componentValue);
                        break;
                }
            }
        }

        protected virtual void AddAttribute(ComponentValue componentValue, AttributeAttachmentLevel attachmentLevel, DataRetrievalInfo info)
        {
            switch (attachmentLevel)
            {
                case AttributeAttachmentLevel.DataSet:
                    this._attributeDataSetValues.Add(componentValue);
                    break;
                case AttributeAttachmentLevel.Group:

                    // NOTE we expect only the attributes of a specific group to be in _attributeGroupValues
                    this._attributeGroupValues.Add(componentValue);
                    break;
                case AttributeAttachmentLevel.DimensionGroup:
                    if (IsDimensionObsReference(componentValue, info))
                    {
                        this._attributeObservationValues.Add(componentValue);
                    }
                    else
                    {
                        this._attributeSeriesValues.Add(componentValue);
                    }

                    break;
                case AttributeAttachmentLevel.Observation:
                    this._attributeObservationValues.Add(componentValue);
                    break;
            }
        }

        protected virtual void AddMeasure(ComponentValue componentValue, DataRetrievalInfo info)
        {
            this._measureValues.Add(componentValue);
            //in order to write sdmx dsd 3.0.0 with multiple measures and data request for 2.1 - might change in the future 
            if (this.PrimaryMeasureValue == null && PrimaryMeasure.FixedId.Equals(componentValue.Key.Id, StringComparison.Ordinal))
            {
                this.PrimaryMeasureValue = componentValue;
            }
        }

        #endregion
    }
}