// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfoSeries.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    using Estat.Sri.Mapping.Api.Builder;
    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

    /// <summary>
    ///     The current data retrieval state for Time Series output
    /// </summary>
    internal class DataRetrievalInfoSeries : DataRetrievalInfo
    {
        /// <summary>
        ///     The dataSet attributes
        /// </summary>
        private readonly List<MappingInfo> _dataSetAttributes;

        /// <summary>
        ///     The <see cref="NameTable" /> used to store atomized strings from group key values
        /// </summary>
        private readonly NameTable _groupNameTable = new NameTable();

        /// <summary>
        ///     A dictionary between a <see cref="IGroup" /> ID and <see cref="GroupInformation" />
        /// </summary>
        private readonly IDictionary<string, GroupInformation> _groups;

        /// <summary>
        ///     Hold all series and observation components
        /// </summary>
        private readonly List<MappingInfo> _seriesObsComponents;

        /// <summary>
        ///     Hold all of the metadata attributes
        /// </summary>
        private readonly List<MappingInfo> _metadataAttributes;

        /// <summary>
        ///     Writer provided that is based on the series model to write the retrieved data.
        /// </summary>
        private readonly IDataWriterEngine _seriesWriter;

        /// <summary>
        /// The data structure object
        /// </summary>
        private readonly IDataStructureObject _dataStructureObject;

        /// <summary>
        /// The metadata structure object
        /// </summary>
        private readonly IMetadataStructureDefinitionObject _metadataStructureObject;

        /// <summary>
        ///     A value indicating whether values for attributes attached to dataset level should be retrieved from DDB using a
        ///     separate SQL query.
        /// </summary>
        private readonly bool _useDataSetSqlQuery;

        /// <summary>
        ///     The component mappings for series. It is filled by <see cref="_seriesObsComponents" />
        /// </summary>
        private MappingInfo[] _componentMappings;

        /// <summary>
        ///     Holds the SQL string for getting the dataset attributes
        /// </summary>
        private string _dataSetSqlString;

        /// <summary>
        ///     Holds the cached where clauses which is useful for TimeSeries with Groups and/or DataSet attributes
        /// </summary>
        private string _sqlWhereCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrievalInfoSeries" /> class.
        /// </summary>
        /// <param name="mappingSet">The mapping set of the dataflow found in the SDMX query</param>
        /// <param name="query">The current SDMX Query object</param>
        /// <param name="settings">The settings.</param>
        /// <param name="seriesWriter">The series Writer.</param>
        public DataRetrievalInfoSeries(
            IComponentMappingContainer mappingSet, 
            IDataQuery query, 
            DataRetrieverSettings settings, 
            IDataWriterEngine seriesWriter)
            : base(mappingSet, query, settings)
        {
            _dataStructureObject = query.DataStructure;
            _metadataStructureObject = query.MetadataStructure;

            this._seriesWriter = seriesWriter;
            this._seriesObsComponents = new List<MappingInfo>();
            this._dataSetAttributes = new List<MappingInfo>();
            this._metadataAttributes = new List<MappingInfo>();
            this._groups = new Dictionary<string, GroupInformation>(StringComparer.Ordinal);

            // We check for the groups in order too see if we should use a separate SQL query for retrieving dataset attributes
            this._useDataSetSqlQuery = query.DataStructure.Groups.Count > 0;
            this.BuildSeriesMappings();

            // add dimension mappings to groups
            this.BuildTimeSeriesGroupMappings();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrievalInfoSeries" /> class.
        /// </summary>
        /// <param name="mappingSet">The mapping set of the dataflow found in the sdmx query</param>
        /// <param name="query">The current SDMX Query object</param>
        /// <param name="settings">The settings.</param>
        /// <param name="seriesWriter">The series Writer.</param>
        public DataRetrievalInfoSeries(
            IComponentMappingContainer mappingSet, 
            IComplexDataQuery query, 
            DataRetrieverSettings settings, 
            IDataWriterEngine seriesWriter)
            : base(mappingSet, query, settings)
        {
            _dataStructureObject = query.DataStructure;
            this._seriesWriter = seriesWriter;
            this._seriesObsComponents = new List<MappingInfo>();
            this._dataSetAttributes = new List<MappingInfo>();
            this._metadataAttributes = new List<MappingInfo>();
            this._groups = new Dictionary<string, GroupInformation>(StringComparer.Ordinal);


            // We check for the groups in order too see if we should use a separate SQL query for retrieving dataset attributes
            this._useDataSetSqlQuery = query.DataStructure.Groups.Count > 0;
            this.BuildSeriesMappings();

            // add dimension mappings to groups
            this.BuildTimeSeriesGroupMappings();
            _dataStructureObject = query.DataStructure;
        }

        /// <summary>
        ///     Gets series and observation components
        /// </summary>
        public override IList<MappingInfo> MainComponentMappings
        {
            get
            {
                return this._componentMappings ?? (this._componentMappings = this._seriesObsComponents.ToArray());
            }
        }

        /// <summary>
        ///     Gets the DataSet level Attribute mappings
        /// </summary>
        public IList<MappingInfo> DataSetAttributes
        {
            get
            {
                return this._dataSetAttributes;
            }
        }

        /// <summary>
        ///     Gets the metadata attribute mappings
        /// </summary>
        public IList<MappingInfo> MetadataAttributes
        {
            get
            {
                return this._metadataAttributes;
            }
        }

        /// <summary>
        ///     Gets or sets the SQL string for getting the dataset attributes
        /// </summary>
        public string DataSetSqlString
        {
            get
            {
                return this._dataSetSqlString;
            }

            set
            {
                this._dataSetSqlString = value;
            }
        }

        /// <summary>
        ///     Gets the <see cref="NameTable" /> used to store atomized strings from group key values
        /// </summary>
        public NameTable GroupNameTable
        {
            get
            {
                return this._groupNameTable;
            }
        }

        /// <summary>
        ///     Gets a dictionary between a <see cref="IGroup" /> ID and <see cref="GroupInformation" />
        /// </summary>
        public IEnumerable<KeyValuePair<string, GroupInformation>> Groups
        {
            get
            {
                return this._groups;
            }
        }

        /// <summary>
        ///     Gets writer provided that is based on the series model to write the retrieved data. If null the XS writer should be
        ///     set instead.
        /// </summary>
        public IDataWriterEngine SeriesWriter
        {
            get
            {
                return this._seriesWriter;
            }
        }

        /// <summary>
        ///     Gets or sets the cached where clauses which is useful for TimeSeries with Groups and/or DataSet attributes
        /// </summary>
        public string SqlWhereCache
        {
            get
            {
                return this._sqlWhereCache;
            }

            set
            {
                this._sqlWhereCache = value;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether write attributes. It depends on the <see cref="IBaseDataQuery.Attributes" />
        /// </summary>
        /// <value>
        ///     <c>true</c> if we should write attributes; otherwise, <c>false</c>.
        /// </value>
        public bool WriteAttributes
        {
            get
            {
                return !(this.BaseDataQuery.Attributes == AttributesEnumType.None || this.BaseDataQuery.Attributes == AttributesEnumType.Msd);
            }
        }

        /// <summary>
        ///     Gets a value indicating whether write metadata attributes. It depends on the <see cref="IBaseDataQuery.Attributes" />
        /// </summary>
        /// <value>
        ///     <c>true</c> if we should write metadata attributes; otherwise, <c>false</c>.
        /// </value>
        public bool WriteMetadataAttributes
        {
            get
            {
                return this.BaseDataQuery.Attributes == AttributesEnumType.All || this.BaseDataQuery.Attributes == AttributesEnumType.Msd;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether write metadata attributes only.
        /// </summary>
        /// <value>
        ///     <c>true</c> if we should write metadata attributes only; otherwise, <c>false</c>.
        /// </value>
        public bool WriteMetadataAttributesOnly
        {
            get
            {
                return this.BaseDataQuery.Measures == MeasuresEnumType.None && this.BaseDataQuery.Attributes == AttributesEnumType.Msd;
            }
        }

        /// <summary>
        /// In case IsMetadataAvailabilityNeeded == true always write either metadata availability or the actual values.
        /// In case IsMetadataAvailabilityNeeded == false write as per the Attributes setting
        /// </summary>
        public bool WriteMetadata
        {
            get { return this.Query != null && this.Query.IsMetadataAvailabilityNeeded || this.WriteMetadataAttributes; }
        }

        /// <summary>
        ///     Gets a value indicating whether we should write observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if we should write observations; otherwise, <c>false</c>.
        /// </value>
        public bool WriteObservations
        {
            get
            {
                return this.BaseDataQuery.Measures == MeasuresEnumType.All || this.BaseDataQuery.Measures == MeasuresEnumType.Specific;
            }
        }

        /// <summary>
        /// Add <paramref name="mappingInfo" /> to <see cref="_dataSetAttributes" />
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void AddDataSetAttributes(MappingInfo mappingInfo)
        {
            if (this._useDataSetSqlQuery)
            {
                this._dataSetAttributes.Add(mappingInfo);
            }
            else
            {
                this._seriesObsComponents.Add(mappingInfo);
            }
        }

        /// <summary>
        ///     Adds series OBS mapping.
        /// </summary>
        /// <param name="componentMapping">
        ///     The component mapping.
        /// </param>
        private void AddSeriesObsMapping(MappingInfo componentMapping)
        {
            this._seriesObsComponents.Add(componentMapping);
        }

        /// <summary>
        /// Add <paramref name="mappingInfo" /> to <see cref="_metadataAttributes" />
        /// </summary>
        /// <param name="mappingInfo">The mapping information.</param>
        private void AddMetadataAttributes(MappingInfo mappingInfo)
        {
            this._metadataAttributes.Add(mappingInfo);
        }

        /// <summary>
        /// Add <paramref name="componentMapping" /> to <see cref="_groups" />
        /// </summary>
        /// <param name="componentMapping">The group level attribute <see cref="IComponentMappingBuilder" /></param>
        private void AddTimeSeriesGroups(MappingInfo componentMapping)
        {
            var groupEntity = this._dataStructureObject.GetGroup(componentMapping.Attribute.AttachmentGroup);
            GroupInformation information;
            if (!this._groups.TryGetValue(groupEntity.Id, out information))
            {
                information = new GroupInformation(groupEntity);
                this._groups.Add(groupEntity.Id, information);
            }

            information.ComponentMappings.Add(componentMapping);
        }

        /// <summary>
        ///     Build Time series mappings
        /// </summary>
        private void BuildSeriesMappings()
        {
            var dimensions = this._dataStructureObject.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension);
            foreach (var dimension in dimensions)
            {
                MappingInfo builder;
                if (this.ComponentIdMap.TryGetValue(dimension.Id, out builder))
                {
                    this.AddSeriesObsMapping(builder);
                }
            }

            foreach (var attributeObject in this._dataStructureObject.DatasetAttributes)
            {
                MappingInfo builder;
                if (this.ComponentIdMap.TryGetValue(attributeObject.Id, out builder))
                {
                    this.AddDataSetAttributes(builder);
                }
            }

            foreach (var attributeObject in this._dataStructureObject.GroupAttributes)
            {
                MappingInfo builder;
                if (this.ComponentIdMap.TryGetValue(attributeObject.Id, out builder))
                {
                    this.AddTimeSeriesGroups(builder);
                }
            }

            if (this._metadataStructureObject != null)
            {
                foreach (var attributeObject in this._metadataStructureObject.GetMetadataAttributes())
                {
                    MappingInfo builder;
                    if (this.ComponentIdMap.TryGetValue(attributeObject.GetFullIdPath(false), out builder))
                    {
                        this.AddMetadataAttributes(builder);
                    }
                }
            }

            foreach (var attributeObject in this._dataStructureObject.GetSeriesAttributes(this.DimensionAtObservation))
            {
                MappingInfo builder;
                if (this.ComponentIdMap.TryGetValue(attributeObject.Id, out builder))
                {
                    this.AddSeriesObsMapping(builder);
                }
            }

            foreach (var attributeObject in this._dataStructureObject.GetObservationAttributes(this.DimensionAtObservation))
            {
                MappingInfo builder;
                if (this.ComponentIdMap.TryGetValue(attributeObject.Id, out builder))
                {
                    this.AddSeriesObsMapping(builder);
                }
            }

            MappingInfo measureBuilder;
            var measures = this._dataStructureObject.MeasureList == null? new[] {this._dataStructureObject.PrimaryMeasure}:this._dataStructureObject.Measures;
            foreach (var measure in measures)
            {
                if (this.ComponentIdMap.TryGetValue(measure.Id, out measureBuilder))
                {
                    this.AddSeriesObsMapping(measureBuilder);
                }
            }
            

            var crossDsd = this._dataStructureObject as ICrossSectionalDataStructureObject;
            if (crossDsd != null)
            {
                foreach (var crossSectionalMeasure in crossDsd.CrossSectionalMeasures)
                {
                    MappingInfo builder;
                    if (this.ComponentIdMap.TryGetValue(crossSectionalMeasure.Id, out builder))
                    {
                        this.AddSeriesObsMapping(builder);
                    }
                }
            }
        }

        /// <summary>
        ///     Build Time series group mappings
        /// </summary>
        private void BuildTimeSeriesGroupMappings()
        {
            foreach (var valuePair in this._groups)
            {
                var groupObject = _dataStructureObject.GetGroup(valuePair.Key);
                foreach (var dim in groupObject.DimensionRefs)
                {
                    MappingInfo componentMapping;
                    if (this.ComponentIdMap.TryGetValue(dim, out componentMapping))
                    {
                        valuePair.Value.ComponentMappings.Add(componentMapping);
                    }
                    else if (dim.Equals(this.MeasureComponent?.Id))
                    {
                        valuePair.Value.MeasureComponent = this.MeasureComponent;
                    }
                }
            }
        }

        public Tuple<long?, long?> Range => this._settings.Range;

        /// <summary>
        /// Gets or sets the dataset identifier.
        /// </summary>
        /// <value>
        /// The dataset identifier.
        /// </value>
        public string DatasetId { get; set; }
    }
}