// -----------------------------------------------------------------------
// <copyright file="MaxObsStatus.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using Estat.Sri.DataRetriever.Model;

    /// <summary>
    ///     The max OBS status.
    /// </summary>
    internal class MaxObsStatus : IDisposable
    {
        /// <summary>
        ///     The _info
        /// </summary>
        private readonly DataRetrievalInfoSeries _info;

        /// <summary>
        ///     The _last n observations
        /// </summary>
        private readonly int _lastNObservations;

        /// <summary>
        ///     The _last observation queue
        /// </summary>
        private readonly Queue<AdvancedMappedValues> _lastObsQueue;

        /// <summary>
        ///     The limit
        /// </summary>
        private readonly int _limit;

        /// <summary>
        ///     The _queue last observation action
        /// </summary>
        private readonly Func<AdvancedMappedValues, int> _queueLastObsAction;

        /// <summary>
        ///     The _write observation function
        /// </summary>
        private readonly Func<DataRetrievalInfoSeries, AdvancedMappedValues, int> _writeObsFunc;

        /// <summary>
        ///     The _count
        /// </summary>
        private int _count;

        /// <summary>
        ///     The _last observation written
        /// </summary>
        private int _lastObsWritten;

        /// <summary>
        ///     The _total count
        /// </summary>
        private int _totalWrittenCount;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MaxObsStatus" /> class.
        /// </summary>
        /// <param name="info">
        ///     The info.
        /// </param>
        /// <param name="writeObsFunc">
        ///     The write observation method (accepts multiple measures).
        /// </param>
        public MaxObsStatus(
            DataRetrievalInfoSeries info, 
            Func<DataRetrievalInfoSeries, AdvancedMappedValues, int> writeObsFunc)
        {
            this._info = info;
            this._writeObsFunc = writeObsFunc;
            this._limit = info.Limit;
            var firstNObservations = info.FirstNObservations;
            this._lastNObservations = info.LastNObservations;
            this._lastObsQueue =
                new Queue<AdvancedMappedValues>(Math.Max(this._lastNObservations, 0));
            if (info.HasFirstAndLastNObservations)
            {
                this._queueLastObsAction = func =>
                    {
                        var val = this.WriteOrSkip(firstNObservations, func);
                        if (val != 0)
                        {
                            return val;
                        }

                        this._lastObsQueue.Enqueue(func);

                        if (this._lastObsQueue.Count > this._lastNObservations)
                        {
                            this._lastObsQueue.Dequeue();
                        }

                        return 0;
                    };
            }
            else if (info.HasFirstNObservations)
            {
                this._queueLastObsAction = func => this.WriteOrSkip(firstNObservations, func);
            }
            else if (info.HasLastNObservations)
            {
                // NOTE Assume that we have "DESC".
                this._queueLastObsAction = func => this.WriteOrSkip(this._lastNObservations, func);
            }
            else
            {
                this._queueLastObsAction = this.WriteObs;
            }
        }

        /// <summary>
        ///     Gets the last observation written
        /// </summary>
        public int LastObservationsWritten
        {
            get
            {
                return this._lastObsWritten;
            }
        }

        /// <summary>
        ///     Adds the specified write observation.
        /// </summary>
        /// <param name="writeObs">The write observation action (accepts multiple measures).</param>
        /// <returns>The number of observation actually written.</returns>
        public int Add(AdvancedMappedValues componentValues)
        {
            return this._queueLastObsAction(componentValues);
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Resets the count.
        /// </summary>
        public void ResetCount()
        {
            int lastObs = 0;
            while (this._lastObsQueue.Count > 0)
            {
                var func = this._lastObsQueue.Dequeue();
                if (this._lastObsQueue.Count < this._lastNObservations
                    && (this._limit <= 0 || this._limit < this._totalWrittenCount))
                {
                    lastObs += this.LastObservationsWritten + this.WriteObs(func);
                }
            }

            this._totalWrittenCount += lastObs;

            this._lastObsWritten = lastObs;
            this._count = 0;
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed">
        ///     <c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only
        ///     unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                this.ResetCount();
            }
        }

        /// <summary>
        ///     Writes the observation.
        /// </summary>
        /// <param name="mappedValues">The tuple.</param>
        /// <returns>The number of observations.</returns>
        private int WriteObs(AdvancedMappedValues mappedValues)
        {
            return this._writeObsFunc(this._info,mappedValues);
        }

        /// <summary>
        ///     Writes the observation or skips it.
        /// </summary>
        /// <param name="firstNObservations">
        ///     The first n observations.
        /// </param>
        /// <param name="values">
        ///     The values.
        /// </param>
        /// <returns>
        ///     The number of observations written.
        /// </returns>
        private int WriteOrSkip(int firstNObservations, AdvancedMappedValues values)
        {
            if (this._count < firstNObservations)
            {
                var val = this.WriteObs(values);
                this._count += val;
                this._totalWrittenCount += val;
                return val;
            }

            this._count++;
            return 0;
        }
    }
}