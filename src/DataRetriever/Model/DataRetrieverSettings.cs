// -----------------------------------------------------------------------
// <copyright file="DataRetrieverSettings.cs" company="EUROSTAT">
//   Date Created : 2017-09-14
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.Utils.Helper;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    /// The data retriever settings.
    /// </summary>
    public class DataRetrieverSettings
    {
        /// <summary>
        /// The component mapping manager
        /// </summary>
        private IComponentMappingManager _componentMappingManager;

        /// <summary>
        /// The _connection string settings.
        /// </summary>
        private ConnectionStringSettings _connectionStringSettings;

        /// <summary>
        /// The _default header.
        /// </summary>
        private IHeader _defaultHeader;

        /// <summary>
        /// The _max number observations.
        /// </summary>
        private int _maxNumberObservations;

        /// <summary>
        /// The _sdmx schema version.
        /// </summary>
        private SdmxSchemaEnumType _sdmxSchemaVersion;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrieverSettings"/> class.
        /// </summary>
        public DataRetrieverSettings()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRetrieverSettings"/> class.
        /// </summary>
        /// <param name="defaultHeader">
        /// The default header.
        /// </param>
        /// <param name="connectionStringSettings">
        /// The connection string settings.
        /// </param>
        /// <param name="sdmxSchemaVersion">
        /// The SDMX schema version.
        ///   The SDMX schema version.
        ///     This controls the behavior of <see cref="ISdmxDataRetrievalWithWriter"/> when no results are found. If set to
        ///     <see cref="SdmxSchemaEnumType.VersionTwoPointOne"/> DR will throw a <see cref="SdmxNoResultsException"/>.
        ///     Otherwise a empty dataset will be written. This affects only <see cref="ISdmxDataRetrievalWithWriter"/> behavior.
        ///     <see cref="IAdvancedSdmxDataRetrievalWithWriter"/> will always throw an exception.
        ///     While <see cref="ISdmxDataRetrievalWithCrossWriter"/> and <see cref="IDataRetrieverTabular"/> will never throw an
        /// </param>
        /// <param name="maxNumberObservations">
        /// The maximum number observations.
        /// </param>
        public DataRetrieverSettings(IHeader defaultHeader, ConnectionStringSettings connectionStringSettings, SdmxSchemaEnumType sdmxSchemaVersion, int maxNumberObservations)
        {
            this._defaultHeader = defaultHeader;
            this._connectionStringSettings = connectionStringSettings;
            this._sdmxSchemaVersion = sdmxSchemaVersion;
            this._maxNumberObservations = maxNumberObservations;
        }

        /// <summary>
        /// Gets or sets the component mapping manager
        /// </summary>
        /// <value>
        /// The component mapping manager.
        /// </value>
        public IComponentMappingManager ComponentMappingManager
        {
            get
            {
                return this._componentMappingManager;
            }

            set
            {
                this._componentMappingManager = value;
            }
        }

        /// <summary>
        /// Gets or sets the component mapping validation manager.
        /// </summary>
        /// <value>
        /// The component mapping validation manager.
        /// </value>
        public IComponentMappingValidationManager ComponentMappingValidationManager { get; set; }

        /// <summary>
        /// Gets or sets the connection builder
        /// </summary>
        /// <value>
        /// The connection builder.
        /// </value>
        public IBuilder<DbConnection, DdbConnectionEntity> ConnectionBuilder { get; set; }

        /// <summary>
        /// Gets or sets the connection string settings.
        /// </summary>
        public ConnectionStringSettings ConnectionStringSettings
        {
            get
            {
                return this._connectionStringSettings;
            }

            set
            {
                this._connectionStringSettings = value;
            }
        }

        /// <summary>
        /// Gets or sets the default header.
        /// </summary>
        public IHeader DefaultHeader
        {
            get
            {
                return this._defaultHeader;
            }

            set
            {
                this._defaultHeader = value;
            }
        }

        /// <summary>
        /// Gets or sets the header retriever engine.
        /// </summary>
        /// <value>
        /// The header retriever engine.
        /// </value>
        public IHeaderRetrieverEngine HeaderRetrieverEngine { get; set; }

        /// <summary>
        /// Gets or sets the max number observations.
        /// </summary>
        public int MaxNumberObservations
        {
            get
            {
                return this._maxNumberObservations;
            }

            set
            {
                this._maxNumberObservations = value;
            }
        }

        /// <summary>
        /// Gets or sets the sdmx schema version.
        /// </summary>
        /// <remarks>
        ///     The SDMX schema version. This controls the behavior of <see cref="ISdmxDataRetrievalWithWriter" /> when no results
        ///     are found. If set to <see cref="SdmxSchemaEnumType.VersionTwoPointOne" /> DR will throw a
        ///     <see cref="SdmxNoResultsException" />. Otherwise a empty dataset will be written. This affects only
        ///     <see cref="ISdmxDataRetrievalWithWriter" /> behavior. <see cref="IAdvancedSdmxDataRetrievalWithWriter" /> will
        ///     always throw an exception.
        ///     While <see cref="ISdmxDataRetrievalWithCrossWriter" /> and <see cref="IDataRetrieverTabular" /> will never throw an
        ///     exception for no results.
        /// </remarks>
        public SdmxSchemaEnumType SdmxSchemaVersion
        {
            get
            {
                return this._sdmxSchemaVersion;
            }

            set
            {
                this._sdmxSchemaVersion = value;
            }
        }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Data Retriever should output local data as annotation.
        /// When this is set to <c>true</c> then Data Retriever will include annotations with local data and the mapping. Specificall there will be two annotation types.
        /// <para>
        /// First the annotation for storing the local column and local code/value which will be present for each component id and value and attached to all levels with component values:
        /// <list type="table">
        /// <listheader>
        /// <term>Annotation Field</term>
        /// <description>Value - description</description>
        /// </listheader>
        /// <item>
        ///     <term>Type</term>
        ///     <description>Always <c>SRI_LOCAL_DATA</c></description>
        /// </item>
        /// <item>
        ///     <term>Title</term>
        ///     <description>The local column name</description>
        /// </item>
        /// <item>
        ///     <term>Text</term>
        ///     <description>The local code/value</description>
        /// </item>
        /// </list>
        /// </para>
        /// <para>
        /// Second the annotation for storing the local column and SDMX component mapping which will be attached at dataset level.
        /// <list type="table">
        /// <listheader>
        /// <term>Annotation Field</term>
        /// <description>Value - description</description>
        /// </listheader>
        /// <item>
        ///     <term>Type</term>
        ///     <description>Always <c>SRI_MAPPING</c></description>
        /// </item>
        /// <item>
        ///     <term>Title</term>
        ///     <description>The SDMX component ID</description>
        /// </item>
        /// <item>
        ///     <term>Text</term>
        ///     <description>The local column name(s) separated by semicolon</description>
        /// </item>
        /// </list>
        /// </para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if Data Retriever output local data as annotation; otherwise, <c>false</c>.
        /// </value>
        public bool OutputLocalDataAsAnnotation { get; set; }

        /// <summary>
        /// Gets or sets the range.
        /// </summary>
        /// <value>
        /// The range.
        /// </value>
        public Tuple<long?, long?> Range { get; set; }

        /// <summary>
        /// Structure usage for the header
        /// </summary>
        public SdmxStructureEnumType StructureUsage { get; set; }

        /// <summary>Gets or sets the dataset action.</summary>
        /// <value>The dataset action.</value>
        public string DatasetAction { get; set; }

        /// <summary>Gets or sets a value indicating whether it is a point in time query.</summary>
        /// <value>
        ///   <c>true</c> if [point in time]; otherwise, <c>false</c>.
        /// </value>
        public bool IsPit { get; set; }

        /// <summary>
        /// Gets or sets the metadata level.
        /// </summary>
        public MetadataLevelEnumType MetadataLevel { get; set; }

        /// <summary>
        /// Gets or sets the confidentiality status values.
        /// </summary>
        public HashSet<string> ConfidentialityStatusValues { get; set; }

        /// <summary>
        /// Gets the DatabaseSetting.
        /// </summary>
        public DatabaseSetting DatabaseSetting
        {
            get
            {
                if (ConnectionStringSettings != null && !string.IsNullOrEmpty(ConnectionStringSettings.ProviderName) && DatabaseType.DatabaseSettings[ConnectionStringSettings.ProviderName] != null)
                {
                    return DatabaseType.DatabaseSettings[ConnectionStringSettings.ProviderName];
                }

                return null;
            }
        }
    }
}