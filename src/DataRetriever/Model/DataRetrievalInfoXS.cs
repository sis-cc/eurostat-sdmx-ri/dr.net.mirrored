// -----------------------------------------------------------------------
// <copyright file="DataRetrievalInfoXS.cs" company="EUROSTAT">
//   Date Created : 2016-03-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.DataRetriever.Model
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;

    using Estat.Sri.Mapping.Api.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;

    /// <summary>
    ///     The current data retrieval state for Cross Sectional output
    /// </summary>
    internal class DataRetrievalInfoXS : DataRetrievalInfo
    {
        /// <summary>
        ///     Gets the XSMeasure attached code to Concept ref map
        /// </summary>
        private readonly Dictionary<string, string> _xsMeasureCodeToConcept =
            new Dictionary<string, string>(StringComparer.Ordinal);

        /// <summary>
        ///     Writer provided that is based on the XS model to write the retrieved data.
        /// </summary>
        private readonly ICrossSectionalWriterEngine _xsWriter;

        /// <summary>
        /// The cross DSD
        /// </summary>
        private readonly ICrossSectionalDataStructureObject _crossDsd;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataRetrievalInfoXS" /> class.
        /// </summary>
        /// <param name="mappingSet">
        ///     The mapping set of the dataflow found in the sdmx query
        /// </param>
        /// <param name="query">
        ///     The current SDMX Query object
        /// </param>
        /// <param name="connectionStringSettings">
        ///     The Mapping Store connection string settings
        /// </param>
        /// <param name="xsWriter">
        ///     The ICrossSectionalWriter Writer.
        /// </param>
        public DataRetrievalInfoXS(
            IComponentMappingContainer mappingSet, 
            IDataQuery query,
            DataRetrieverSettings connectionStringSettings, 
            ICrossSectionalWriterEngine xsWriter)
            : base(mappingSet, query, connectionStringSettings)
        {
            this._xsWriter = xsWriter;
            this._crossDsd = query.DataStructure as ICrossSectionalDataStructureObject;
            if (this.MeasureComponent == null)
            {
                if (this._crossDsd != null)
                {
                    foreach (var crossSectionalMeasure in this._crossDsd.CrossSectionalMeasures)
                    {
                        this._xsMeasureCodeToConcept.Add(crossSectionalMeasure.Code, crossSectionalMeasure.Id);
                    }
                }
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has first n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has first n observations; otherwise, <c>false</c>.
        /// </value>
        public override bool HasFirstNObservations
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this instance has last n observations.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance has last n observations; otherwise, <c>false</c>.
        /// </value>
        public override bool HasLastNObservations
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        ///     Gets the XSMeasure attached code to Concept ref map
        /// </summary>
        public IDictionary<string, string> XSMeasureCodeToConcept
        {
            get
            {
                return this._xsMeasureCodeToConcept;
            }
        }

        /// <summary>
        ///     Gets the writer provided that is based on the XS model to write the retrieved data. If null the seriesWriter should
        ///     be set instead.
        /// </summary>
        public ICrossSectionalWriterEngine XSWriter
        {
            get
            {
                return this._xsWriter;
            }
        }

        /// <summary>
        /// Gets the cross DSD.
        /// </summary>
        /// <value>
        /// The cross DSD.
        /// </value>
        public ICrossSectionalDataStructureObject CrossDsd
        {
            get
            {
                return this._crossDsd;
            }
        }
    }
}