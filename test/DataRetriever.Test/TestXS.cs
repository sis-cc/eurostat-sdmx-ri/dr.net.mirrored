// -----------------------------------------------------------------------
// <copyright file="TestXS.cs" company="EUROSTAT">
//   Date Created : 2013-12-03
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    [TestFixture("sqlserver")]
    public class TestXS : TestBase
    {
        #region Fields

        /// <summary>
        /// The _data query parse manager.
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager;

        /// <summary>
        /// The _data retrieval.
        /// </summary>
        private readonly ISdmxDataRetrievalWithCrossWriter _dataRetrieval;

        /// <summary>
        /// The _retrieval manager.
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TestXS"/> class.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        public TestXS(string name) : base(name)
        {
            var connectionString = ConfigurationManager.ConnectionStrings [name];
            this._dataRetrieval = new DataRetrieverCore(CreateSettings(SdmxSchemaEnumType.VersionTwo, nameof(TestXS)));
            this._dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.VersionTwo);
            Database database = new Database(this.GetConnectionStringSettings());
            IRetrievalEngineContainer container = new RetrievalEngineContainer(database);
            ICommonSdmxObjectRetrievalManager retrievalManager = new MappingStoreCommonSdmxObjectRetriever(container);
            this._retrievalManager = new SdmxObjectRetrievalManagerWrapper(retrievalManager);
        }

        [TestCase("tests/v20/get-CENSUSHUB_Q_XS1.xml")]
        public void TestXSOutput(string filePath)
        {
            IList<IDataQuery> dataQueries;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, this._retrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName))
            {
                var dataWriter = new CrossSectionalWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));

                this._dataRetrieval.GetData(dataQuery, dataWriter);
                writer.Flush();
            }

            var fileInfo = new FileInfo(outputFileName);
            Assert.IsTrue(fileInfo.Exists);
            var dsd = dataQuery.DataStructure as ICrossSectionalDataStructureObject;
            Assert.NotNull(dsd);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                    case "DataSet":
                                        AssertDimensionsAreMapped(dsd.GetCrossSectionalAttachDataSet(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension)), reader);
                                        break;
                                    case "Section":
                                        AssertDimensionsAreMapped(dsd.GetCrossSectionalAttachSection(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension)), reader);

                                        break;
                                    case "Group":
                                        AssertDimensionsAreMapped(dsd.GetCrossSectionalAttachGroup(true, SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension)), reader);

                                        break;
                                    case "OBS_VALUE":
                                        AssertDimensionsAreMapped(dsd.GetCrossSectionalAttachObservation(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dimension)), reader);
                                        break;
                                }
                            }

                            break;
                    }
                }
            }
        }

        private static void AssertDimensionsAreMapped(IEnumerable<IComponent> attachedComponents, XmlReader reader)
        {
            foreach (var dimension in attachedComponents)
            {
                var value = reader.GetAttribute(dimension.Id);
                Assert.False(string.IsNullOrWhiteSpace(value), "Dimension : {0}", dimension.Id);
            }
        }
    }
}