// -----------------------------------------------------------------------
// <copyright file="LocalDataAsAnnotationTest.cs" company="EUROSTAT">
//   Date Created : 2018-01-04
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using NUnit.Framework;

namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Util.Collections;

    [TestFixture("sqlserver")]
    public class LocalDataAsAnnotationTest : TestBase
    {
        private readonly DataRetrieverCore _dataRetrievalRest;
        private readonly IDataQueryParseManager _dataQueryParseManager;
        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        private readonly DataRetrieverSettings _dataRetrieverSettings;

        public LocalDataAsAnnotationTest(string storeId)
            : base(storeId)
        {
            this._dataRetrieverSettings = this.CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne, nameof(LocalDataAsAnnotationTest));
            this._dataRetrieverSettings.OutputLocalDataAsAnnotation = true;
            this._dataRetrievalRest = new DataRetrieverCore(this._dataRetrieverSettings);
            this._dataQueryParseManager = new DataQueryParseManager();
            Database database = new Database(this.GetConnectionStringSettings());
            IRetrievalEngineContainer container = new RetrievalEngineContainer(database);
            ICommonSdmxObjectRetrievalManager retrievalManager = new MappingStoreCommonSdmxObjectRetriever(container);
            this._retrievalManager = new SdmxObjectRetrievalManagerWrapper(retrievalManager);

        }

        /// <summary>
        /// The test dimension at OBS.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        [TestCase("data/IT,56_259/ALL/ALL")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AREA")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_ACTIVITY")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_INDICATOR")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=TIME_PERIOD")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AllDimensions")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=1&firstNObservations=1")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=3")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=4")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=4")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&firstNObservations=4")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=3")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=5")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=5&firstNObservations=4")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=3&firstNObservations=4")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5&firstNObservations=3")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5&firstNObservations=3")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=dataonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=nodata")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=serieskeysonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=full")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=dataonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=nodata")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=serieskeysonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=full")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=dataonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=nodata")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=serieskeysonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=dataonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=nodata")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=serieskeysonly")]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full")]
        [TestCase("data/ESTAT,DEMOGRAPHY/A.DK.T../ALL?dimensionAtObservation=TIME_PERIOD&detail=full")]
        [TestCase("data/ESTAT,DEMOGRAPHY/A.DK.T../ALL?dimensionAtObservation=AllDimensions&detail=full")]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", IgnoreReason = "PC Axis not supported yet")]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", IgnoreReason = "PC Axis not supported yet")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/......./?lastNObservations=5&startPeriod=1920-01&endPeriod=2940-01")]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/......./?lastNObservations=5")]
        [TestCase("data/ESTAT,EGR_CORE_ENT,1.0/all/ALL/?detail=full&dimensionAtObservation=ENT_FRAME_RYEAR", IgnoreReason = "Missing mapping set from MSDB")]
        public async Task ShouldHaveLocalDataAsAnnotationsAsync(string filePath)
        {
            var connectionString = this.GetConnectionStringSettings();
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            var dataQuery = this._dataQueryParseManager.ParseRestQuery(filePath, mappingStoreSdmxObjectRetrievalManager);

            Assert.IsNotNull(dataQuery);

            var outputFileName = string.Format("ShouldHaveLocalDataAsAnnotations-{0}-{1}-{2}-{3}-out.xml", dataQuery.Dataflow.Id, dataQuery.DimensionAtObservation, dataQuery.FirstNObservations, dataQuery.LastNObservations);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                ISdmxDataRetrievalWithWriter sdmxDataRetrievalWithWriter = this._dataRetrievalRest;
                await sdmxDataRetrievalWithWriter.GetDataAsync(dataQuery, dataWriter);
                writer.Flush();
            }

            var container = this._dataRetrieverSettings.ComponentMappingManager.GetBuilders(this.StoreId, dataQuery.Dataflow, dataQuery.DataStructure, dataQuery.MetadataStructure, false);
            var withColumn = container.ComponentMappings.Where(x => x.Value.GetColumns() != null && x.Value.GetColumns().Count > 0).ToDictionary(x => x.Key, v => v.Value.GetColumns().Select(c => c.Name).ToArray());
            var dictionary = new DictionaryOfSets<string, string>(StringComparer.Ordinal);
            foreach (var pair in withColumn)
            {
                foreach (var s in pair.Value)
                {
                    dictionary.AddToSet(s, pair.Key);
                }
            }

            if (container.TimeDimensionMapping != null && ObjectUtil.ValidCollection(container.TimeDimensionMapping.GetColumns()))
            {
                foreach (var dataSetColumnEntity in container.TimeDimensionMapping.GetColumns())
                {
                    dictionary.AddToSet(dataSetColumnEntity.Name, DimensionObject.TimeDimensionFixedId);
                }
            }

            var stack = new Stack<DatasetPosition>();
            IAnnotationMutableObject currentAnnotation = null;
            string text = null;
            var foundAnnotation = new HashSet<DatasetPosition>(); 
            using (var reader = XmlReader.Create(outputFileName))
            {
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    switch (nodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                        case "Annotation":
                                            currentAnnotation = new AnnotationMutableCore();
                                            foundAnnotation.Remove(stack.Peek());
                                            break;
                                        case "DataSet":
                                            Assert.That(!reader.IsEmptyElement, localName);
                                            stack.Push(DatasetPosition.Dataset);
                                            foundAnnotation.Add(stack.Peek());
                                            break;
                                        case "Series":
                                            Assert.That(!reader.IsEmptyElement, localName);
                                            stack.Push(DatasetPosition.Series);
                                            foundAnnotation.Add(stack.Peek());
                                            break;
                                        case "Group":
                                            Assert.That(!reader.IsEmptyElement, localName);
                                            stack.Push(DatasetPosition.Group);
                                            foundAnnotation.Add(stack.Peek());
                                            break;
                                        case "Obs":
                                            Assert.That(!reader.IsEmptyElement, localName);
                                            stack.Push(DatasetPosition.Observation);
                                            foundAnnotation.Add(stack.Peek());
                                            break;
                                }
                            }
                           break;
                        case XmlNodeType.EndElement:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                    case "Annotation":
                                        var index = stack.Peek();
                                        Assert.That(index, Is.Not.EqualTo(DatasetPosition.Null));
                                        Assert.That(currentAnnotation, Is.Not.Null);
                                        if (index == DatasetPosition.Dataset)
                                        {
                                            Assert.That(currentAnnotation.Type, Is.EqualTo("SRI_MAPPING"));
                                            var component = dataQuery.DataStructure.GetComponent(currentAnnotation.Title);
                                            Assert.That(component, Is.Not.Null);
                                            IMappingEntity mapping;
                                            if (component.StructureType.EnumType != SdmxStructureEnumType.TimeDimension)
                                            {
                                                mapping = container.ComponentMappings[component.Id];
                                            }
                                            else
                                            {
                                                mapping = container.TimeDimensionMapping;
                                            }

                                            if (mapping.GetColumns() != null && mapping.GetColumns().Count > 0)
                                            {
                                                Assert.That(currentAnnotation.Text, Is.Not.Empty);
                                                var columns = currentAnnotation.Text[0].Value.Split(';');
                                                var expectedColumns = mapping.GetColumns().Select(x => x.Name);
                                                Assert.That(columns, Is.EquivalentTo(expectedColumns));
                                            }
                                            else
                                            {
                                                Assert.That(currentAnnotation.Text, Is.Empty);
                                            }
                                        }
                                        else
                                        {
                                            Assert.That(currentAnnotation.Type, Is.EqualTo("SRI_LOCAL_DATA"));
                                            Assert.That(dictionary.ContainsKey(currentAnnotation.Title), "The '{0}' was not found in dictionary = [{1}]", currentAnnotation.Title, string.Join(", ", dictionary.Keys));
                                            var hasMandatoryValueComponent = (from s in dictionary[currentAnnotation.Title] let attributeObject = dataQuery.DataStructure.GetAttribute(s) where dataQuery.DataStructure.GetDimension(s) != null || (attributeObject != null && attributeObject.Mandatory) select s).Any();
                                            if (hasMandatoryValueComponent)
                                            {
                                                Assert.That(currentAnnotation.Text, Is.Not.Empty, currentAnnotation.Title);
                                            }
                                        }

                                        currentAnnotation = null;
                                        break;
                                    case "DataSet":
                                    case "Series":
                                    case "Group":
                                    case "Obs":
                                        var lastTag = stack.Pop();
                                        Assert.That(!foundAnnotation.Contains(lastTag), localName + ":" + lastTag.ToString());
                                        break;
                                    case "AnnotationText":
                                        if (currentAnnotation != null)
                                        {
                                            currentAnnotation.AddText(reader.XmlLang, text);
                                        }

                                        break;
                                    case "AnnotationTitle":
                                        if (currentAnnotation != null)
                                        {
                                            currentAnnotation.Title = text;
                                        }

                                        break;
                                    case "AnnotationType":
                                        if (currentAnnotation != null)
                                        {
                                            currentAnnotation.Type = text;
                                        }

                                        break;
                                }
                            }

                            break;
                        case XmlNodeType.Text:
                            text = reader.Value;
                            break;
                    }

                }
            }
        }
    }
}
