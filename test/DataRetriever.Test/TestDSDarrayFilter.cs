// -----------------------------------------------------------------------
// <copyright file="TestDSDarrayMapping.cs" company="EUROSTAT">
//   Date Created : 2022-11-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using Estat.Nsi.DataRetriever;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    [TestFixture("sqlserver_schratch")]
    [TestFixture("mysql_scratch")]
    public class TestDSDarrayFilter : TestArrayMappingBase
    {
        private const string OUTPUT_FILENAME = @"tests/DSDarrayFilter-out.xml";
        private long _mappingSetPK;
        private bool _nto1MappingCreated;
        private bool _constantMappingCreated;

        protected override long MappingSetPK => _mappingSetPK;

        #region setup

        public TestDSDarrayFilter(string storeId)
            : base(storeId)
        {
        }

        [OneTimeSetUp]
        public void OneSetUp()
        {
            InitializeMappingStore();

            string dataflowUrn = ImportStructures();

            CreateRequiredMappings(dataflowUrn);

            // add array mapping
            // map OBS_CONF as 1 to 1 with transcoding
            var attributeMapping = CreateTestComponentMappingEntity("OBS_CONF", "X_LBIRTHST_M");
            attributeMapping.DefaultValues.Add("DEF_1");
            attributeMapping.DefaultValues.Add("DEF_2");
            var attrMappingWithPK = this.EntityPersistenceManager.Add(attributeMapping);
            Assert.IsNotNull(attrMappingWithPK);
            var transcodings = new Dictionary<string, List<string>>()
            {
                { "81111", new(){"CODE_11", "CODE_12"} },
                { "31111", new(){"CODE_21", "CODE_22"} },
                { "9012", new(){"CODE_31", "CODE_32"} }
            };
            AddTranscodingRules(transcodings, "X_LBIRTHST_M", attrMappingWithPK.EntityId);
        }

        private void CreateRequiredMappings(string dataflowUrn)
        {
            // add mapping entities
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(connectionStringManager), null);

            DdbConnectionEntity ddbConnectionEntity;
            if (StoreId.Equals("sqlserver_schratch", StringComparison.OrdinalIgnoreCase))
            {
                ddbConnectionEntity = new DdbConnectionEntity()
                {
                    Name = "STS 2.0 connection",
                    Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                    DbType = "SqlServer",
                    DbUser = "ddb",
                    Password = "123",
                    AdoConnString = "Data Source=test-ddb-mssql;Initial Catalog=Demography_TypeB;Integrated Security=False;User ID=ddb;Password=123"
                };
            }
            else if (StoreId.Equals("mysql_scratch", StringComparison.OrdinalIgnoreCase))
            {
                ddbConnectionEntity = new DdbConnectionEntity()
                {
                    Name = "STS 2.0 connection",
                    Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                    DbType = "MySQL",
                    DbUser = "mauser",
                    Password = "123",
                    AdoConnString = "server=sodi-test;database=demography_type_b;user id=mauser;password=123"
                };
            }
            else
            {
                throw new ArgumentException($"cannot recognize {StoreId} database.");
            }

            long ddbConnectionPK = entityPeristenceFactory.GetEngine<DdbConnectionEntity>(StoreId).Add(ddbConnectionEntity);
            Assert.NotNull(ddbConnectionPK);

            var datasetEntity = new DatasetEntity()
            {
                Name = "STS 2.0 dataset",
                Description = "Dataset for new ESTAT.STS.2.0 datastructure",
                Query = "select" +
                    " X_FREQ, X_COUNTRY, X_ADJT_F, X_DEATHST_F, X_LBIRTHST_F, X_NETMT_F, X_PJAN1T_F," +
                    " X_PJANT_F, X_ADJT_M, X_DEATHST_M, X_TIME," +
                    " X_LBIRTHST_M, X_NETMT_M, X_PJAN1T_M, X_PJANT_M" + // available columns for mapping
                    " from type_b_demography",
                EditorType = "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant",
                ParentId = ddbConnectionPK.ToString()
            };
            long datasetPK = entityPeristenceFactory.GetEngine<DatasetEntity>(StoreId).Add(datasetEntity);
            Assert.NotNull(datasetPK);

            var mappingSetEntity = new MappingSetEntity()
            {
                Name = "STS 2.0 mapping set",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowUrn
            };
            _mappingSetPK = entityPeristenceFactory.GetEngine<MappingSetEntity>(StoreId).Add(mappingSetEntity);
            Assert.NotNull(MappingSetPK);

            // add required component mappings
            List<ComponentMappingEntity> componentMappings = new List<ComponentMappingEntity>
            {
                // component id, dataset column name(s)
                // dimensions
                CreateTestComponentMappingEntity("FREQ", "X_FREQ"),
                CreateTestComponentMappingEntity("REF_AREA", "X_COUNTRY"),
                CreateTestComponentMappingEntity("ADJUSTMENT", "X_ADJT_F"),
                CreateTestComponentMappingEntity("STS_INDICATOR", "X_DEATHST_F"),
                CreateTestComponentMappingEntity("STS_ACTIVITY", "X_LBIRTHST_F"),
                CreateTestComponentMappingEntity("STS_INSTITUTION", "X_NETMT_F"),
                CreateTestComponentMappingEntity("STS_BASE_YEAR", "X_PJAN1T_F"),
                // attributes
                CreateTestComponentMappingEntity("TIME_FORMAT", "X_PJANT_F"), // mandatory, series
                CreateTestComponentMappingEntity("OBS_PRE_BREAK", "X_ADJT_M"), // optional, observation, scalar
                // measures
                CreateTestComponentMappingEntity("MEASURE_2", "X_DEATHST_M")
            };
            var entitiesWithPK = this.EntityPersistenceManager.AddEntities(componentMappings);
            Assert.AreEqual(componentMappings.Count, entitiesWithPK.Count(), "required component mappings");

            // special case - time mapping
            // time dimension is TIME_PERIOD
            var timeMapping = new TimeDimensionMappingEntity()
            {
                EntityId = MappingSetPK.ToString(),
                StoreId = this.StoreId,
                SimpleMappingColumn = "X_TIME"
            };
            var timeMappingWithPK = this.EntityPersistenceManager.Add(timeMapping);
            Assert.IsNotNull(timeMappingWithPK);
        }

        #endregion

        #region tests

        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[OBS_CONF]=CODE_11+CODE_12", 640, 27)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[OBS_CONF]=CODE_11+CODE_12,CODE_21+CODE_22", 1256, 53)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[OBS_CONF]=ne:CODE_31+CODE_32", 2312, 97)]
        public void TestFilter1to1TranscodingMapping(string query, int expectedNoRows_sqlserver, int expectedRows_mysql)
        {
            // get data
            using (DataflowFilterContext context = new DataflowFilterContext(DataflowFilter.Any))
            {
                GetDataAndWriteFileCompact(query, OUTPUT_FILENAME, SdmxSchemaEnumType.VersionThree);
            }
            int observationsCount = CountObservations(OUTPUT_FILENAME);
            int expectedNoRows = GetExpectedNoRows(expectedNoRows_sqlserver, expectedRows_mysql);
            Assert.AreEqual(expectedNoRows, observationsCount);
        }

        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=7890+8901", 96, 4)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=7890+8901,8888+9999", 744, 31)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=ne:8888+9999", 1760, 74)]
        public void TestFilterNto1Mapping(string query, int expectedNoRows_sqlserver, int expectedRows_mysql)
        {
            if (!_nto1MappingCreated)
            {
                CreateNto1Mapping();
                _nto1MappingCreated = true;
            }

            // get data
            using (DataflowFilterContext context = new DataflowFilterContext(DataflowFilter.Any))
            {
                GetDataAndWriteFileCompact(query, OUTPUT_FILENAME, SdmxSchemaEnumType.VersionThree);
            }
            int observationsCount = CountObservations(OUTPUT_FILENAME);
            int expectedNoRows = GetExpectedNoRows(expectedNoRows_sqlserver, expectedRows_mysql);
            Assert.AreEqual(expectedNoRows, observationsCount);
        }

        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=CONST_1+CONST_2", 2408, 101)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=CONST_1+CONST_2,anyval_1+anyval_2", 2408, 101)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=anyval_1+anyval_2", 0, 0)]
        [TestCase("data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[MEASURE_1]=ne:CONST_1+CONST_2", 0, 0)]
        public void TestFilterConstantMapping(string query, int expectedNoRows_sqlserver, int expectedRows_mysql)
        {
            if (!_constantMappingCreated)
            {
                CreateConstantMapping();
                _constantMappingCreated = true;
            }

            // get data
            using (DataflowFilterContext context = new DataflowFilterContext(DataflowFilter.Any))
            {
                GetDataAndWriteFileCompact(query, OUTPUT_FILENAME, SdmxSchemaEnumType.VersionThree);
            }
            int observationsCount = CountObservations(OUTPUT_FILENAME);
            int expectedNoRows = GetExpectedNoRows(expectedNoRows_sqlserver, expectedRows_mysql);
            Assert.AreEqual(expectedNoRows, observationsCount);
        }

        // all c-parameter operators other than eq,ne are not supported (yet) for array values
        [TestCaseSource(nameof(TestCases_InvalidRequests))]
        public void TestInvalidRequests(string query)
        {
            using (DataflowFilterContext context = new DataflowFilterContext(DataflowFilter.Any))
            {
                Assert.Throws<DataRetrieverException>(() => GetDataAndWriteFileCompact(query, OUTPUT_FILENAME, SdmxSchemaEnumType.VersionThree));
            }
        }

        #endregion

        #region test cases sources

        private static IEnumerable<string> TestCases_InvalidRequests()
        {
            string[] operators = new string[]
            {
                "lt", "le", "gt", "ge", "co", "nc", "sw", "ew"
            };
            string queryFormat = "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=1&c[REF_AREA]=Greece&c[OBS_CONF]={0}:CODE_11+CODE_12";

            foreach (string c_operator in operators)
            {
                yield return string.Format(queryFormat, c_operator);
            }
        }

        #endregion

        #region helper methods

        /// <summary>
        /// Counts the number of observations returned in an xml data file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static int CountObservations(string fileName)
        {
            int count = 0;

            var fileInfo = new FileInfo(fileName);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName.Equals("Obs", StringComparison.Ordinal))
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        private int GetExpectedNoRows(int sqlServerNo, int mysqlNo)
        {
            if (StoreId.Equals("sqlserver_schratch", StringComparison.OrdinalIgnoreCase))
            {
                return sqlServerNo;
            }
            else if (StoreId.Equals("mysql_scratch", StringComparison.OrdinalIgnoreCase))
            {
                return mysqlNo;
            }
            else
            {
                throw new ArgumentException($"cannot recognize {StoreId} database.");
            }
        }

        private void CreateNto1Mapping()
        {
            DeleteComponentMappings("MEASURE_1");
            var measureMapping = CreateTestComponentMappingEntity("MEASURE_1", "X_ADJT_M", "X_DEATHST_M");
            var measureMappingWithPK = this.EntityPersistenceManager.Add(measureMapping);
            Assert.IsNotNull(measureMappingWithPK);
        }
        
        private void CreateConstantMapping()
        {
            DeleteComponentMappings("MEASURE_1");
            var measureMapping = CreateTestComponentMappingEntity("MEASURE_1");
            measureMapping.ConstantValues.Add("CONST_1");
            measureMapping.ConstantValues.Add("CONST_2");
            var measureMappingWithPK = this.EntityPersistenceManager.Add(measureMapping);
            Assert.IsNotNull(measureMappingWithPK);
        }

        #endregion
    }
}
