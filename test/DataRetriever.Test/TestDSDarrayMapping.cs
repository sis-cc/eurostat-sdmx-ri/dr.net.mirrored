// -----------------------------------------------------------------------
// <copyright file="TestDSDarrayMapping.cs" company="EUROSTAT">
//   Date Created : 2022-11-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using DryIoc;
    using System.Xml;
    using Org.Sdmxsource.Util.Extensions;
    using System;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Constants;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Org.BouncyCastle.Crypto.Digests;

    [TestFixture("sqlserver_schratch")]
    public class TestDSDarrayMapping : TestArrayMappingBase
    {
        private const string OUTPUT_FILENAME = @"tests/DSDarrayMapping-out.xml";
        private long _mappingSetPK;

        protected override long MappingSetPK => _mappingSetPK;

        #region setup

        public TestDSDarrayMapping(string storeId)
            : base(storeId)
        {
        }

        [OneTimeSetUp]
        public void OneSetUp()
        {
            InitializeMappingStore();

            string dataflowUrn = ImportStructures();

            CreateRequiredMappings(dataflowUrn);
        }

        private void CreateRequiredMappings(string dataflowUrn)
        {
            // add mapping entities
            var entityPeristenceFactory = new EntityPeristenceFactory(new DatabaseManager(connectionStringManager), null);
            var ddbConnectionEntity = new DdbConnectionEntity()
            {
                Name = "STS 2.0 connection",
                Description = "DDB connection for new ESTAT.STS.2.0 datastructure",
                DbType = "SqlServer",
                DbUser = "ddb",
                Password = "123",
                AdoConnString = "Data Source=test-ddb-mssql;Initial Catalog=DDB_B_TEST_JANUARY2014;Integrated Security=False;User ID=ddb;Password=123"
            };
            long ddbConnectionPK = entityPeristenceFactory.GetEngine<DdbConnectionEntity>(StoreId).Add(ddbConnectionEntity);
            Assert.NotNull(ddbConnectionPK);

            var datasetEntity = new DatasetEntity()
            {
                Name = "STS 2.0 dataset",
                Description = "Dataset for new ESTAT.STS.2.0 datastructure",
                Query = "select" +
                    " FREQ, REF_AREA, ADJUSTMENT, ACCOUNTING_ENTRY, ACTIVITY, INSTR_ASSET, REF_PERIOD," +
                    " TIME_FORMAT, TRANSFORMATION, OBS_VALUE, REF_PERIOD_DETAIL," +
                    " OBS_STATUS, REF_YEAR_PRICE, UNIT_MULT, STO, CONF_STATUS, DECIMALS, EMBARGO_DATE" + // available columns for mapping
                    " from ESA_MAIN_TOT_ESTAT_10",
                EditorType = "org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant",
                ParentId = ddbConnectionPK.ToString()
            };
            long datasetPK = entityPeristenceFactory.GetEngine<DatasetEntity>(StoreId).Add(datasetEntity);
            Assert.NotNull(datasetPK);

            var mappingSetEntity = new MappingSetEntity()
            {
                Name = "STS 2.0 mapping set",
                Description = "Mapping set for new ESTAT.STS.2.0 datastructure",
                DataSetId = datasetPK.ToString(),
                ParentId = dataflowUrn
            };
            _mappingSetPK = entityPeristenceFactory.GetEngine<MappingSetEntity>(StoreId).Add(mappingSetEntity);
            Assert.NotNull(MappingSetPK);

            // add required component mappings
            List<ComponentMappingEntity> componentMappings = new List<ComponentMappingEntity>
            {
                // component id, dataset column name(s)
                // dimensions
                CreateTestComponentMappingEntity("FREQ", "FREQ"),
                CreateTestComponentMappingEntity("REF_AREA", "REF_AREA"),
                CreateTestComponentMappingEntity("ADJUSTMENT", "ADJUSTMENT"),
                CreateTestComponentMappingEntity("STS_INDICATOR", "ACCOUNTING_ENTRY"),
                CreateTestComponentMappingEntity("STS_ACTIVITY", "ACTIVITY"),
                CreateTestComponentMappingEntity("STS_INSTITUTION", "INSTR_ASSET"),
                CreateTestComponentMappingEntity("STS_BASE_YEAR", "REF_PERIOD"),
                // attributes
                CreateTestComponentMappingEntity("TIME_FORMAT", "TIME_FORMAT"), // mandatory, series
                CreateTestComponentMappingEntity("OBS_PRE_BREAK", "TRANSFORMATION"), // optional, observation, scalar
                // measures
                CreateTestComponentMappingEntity("MEASURE_2", "OBS_VALUE")
            };
            var entitiesWithPK = this.EntityPersistenceManager.AddEntities(componentMappings);
            Assert.AreEqual(componentMappings.Count, entitiesWithPK.Count(), "required component mappings");

            // special case - time mapping
            // time dimension is TIME_PERIOD
            var timeMapping = new TimeDimensionMappingEntity()
            {
                EntityId = MappingSetPK.ToString(),
                StoreId = this.StoreId,
                SimpleMappingColumn = "REF_PERIOD_DETAIL"
            };
            var timeMappingWithPK = this.EntityPersistenceManager.Add(timeMapping);
            Assert.IsNotNull(timeMappingWithPK);
        }

        #endregion

        [TestCaseSource(nameof(TestCases_Nto1ColumnsMappingNoTranscoding))]
        public void TestNto1ColumnsMappingNoTranscoding(string componentName, List<string> ddbColumns, string query, List<string> expectedValues)
        {
            Action addMappings = () =>
            {
                // add component mappings
                // each component maps to multiple dataset columns
                var componentMapping = CreateTestComponentMappingEntity(componentName, ddbColumns.ToArray());
                var entityWithPK = this.EntityPersistenceManager.Add(componentMapping);
                Assert.IsNotNull(entityWithPK);
            };
            
            TestTemplate(componentName, query, expectedValues, addMappings);
        }

        [TestCaseSource(nameof(TestCases_1to1ColumnsMappingWithArrayTranscoding))]
        public void Test1to1ColumnsMappingWithArrayTranscoding(
            string componentName, string ddbColumn, Dictionary<string, List<string>> transcodingRules,
            string query, List<string> expectedValues)
        {
            Action addMappings = () =>
            {
                // add component mappings
                var componentMapping = CreateTestComponentMappingEntity(componentName, ddbColumn);
                var mappingWithPK = this.EntityPersistenceManager.Add(componentMapping);
                Assert.IsNotNull(mappingWithPK);

                // add transcoding rules
                foreach (var rule in transcodingRules)
                {
                    var transcodingRule = new TranscodingRuleEntity()
                    {
                        ParentId = mappingWithPK.EntityId,
                        StoreId = this.StoreId
                    };
                    transcodingRule.LocalCodes.Add(new LocalCodeEntity()
                    {
                        ObjectId = rule.Key,
                        ParentId = ddbColumn
                    });
                    foreach (string sdmxCode in rule.Value)
                    {
                        transcodingRule.DsdCodeEntities.Add(new IdentifiableEntity() { ObjectId = sdmxCode });
                    }
                    var ruleWithPK = this.EntityPersistenceManager.Add(transcodingRule);
                    Assert.IsNotNull(ruleWithPK);
                }
            };

            TestTemplate(componentName, query, expectedValues, addMappings);
        }

        [TestCaseSource(nameof(TestCases_MappingWithConstantArrayValues))]
        public void TestMappingWithConstantArrayValues(string componentName, List<string> constantValues, string query)
        {
            Action addMappings = () =>
            {
                // add component mappings
                var componentMapping = CreateTestComponentMappingEntity(componentName);
                componentMapping.ConstantValues.AddAll(constantValues);
                var mappingWithPK = this.EntityPersistenceManager.Add(componentMapping);
                Assert.IsNotNull(mappingWithPK);
            };

            TestTemplate(componentName, query, constantValues, addMappings);
        }

        [TestCaseSource(nameof(TestCases_MappingWithDefaultArrayValues))]
        public void TestMappingWithDefaultArrayValues(
            string componentName, string ddbColumn, Dictionary<string, List<string>> transcodingRules,
            List<string> defaultArrayValues, string query, List<string> expectedValues)
        {
            Action addMappings = () =>
            {
                // add component mappings
                var componentMapping = CreateTestComponentMappingEntity(componentName, ddbColumn);
                componentMapping.DefaultValues.AddAll(defaultArrayValues);
                var mappingWithPK = this.EntityPersistenceManager.Add(componentMapping);
                Assert.IsNotNull(mappingWithPK);

                // add transcoding rules
                AddTranscodingRules(transcodingRules, ddbColumn, mappingWithPK.EntityId);
            };

            TestTemplate(componentName, query, expectedValues, addMappings);
        }

        #region test cases

        /// <summary>
        /// Map a component that accepts array values to multiple dataset columns (N to 1)
        /// and retrieve the values in these columns as array values of the component.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<object[]> TestCases_Nto1ColumnsMappingNoTranscoding()
        {
            // each test case creates a mapping of a different attribute to two columns (same columns every time)
            // and creates a data query that contains a single row with the expected values in the columns.
            string[] testAttributes = new string[]
            {
                "OBS_CONF",     // observation level
                "AVAILABILITY", // dimension group level
                "DECIMALS",     // group level
                "OBS_STATUS"    // dataset level
            };

            foreach (string attribute in testAttributes)
            {
                yield return new object[]
                {
                    // the attribute
                    attribute, 
                    // the mapped dataset columns
                    new List<string>(){ "STO", "CONF_STATUS" }, 
                    // the data query; data queries use the SDMX codes, not local
                    // the key query path is used for dimensions only, to filter components use the c-parameter instead
                    // this one should return a single observation
                    // Note that the order of components is taken from the "position" attribute in the xml
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995", 
                    // the expected array values for the component
                    new List<string>(){ "YA1", "F" }
                };
            }

            // measure test case
            yield return new object[]
            {
                "MEASURE_1",
                new List<string>(){ "REF_YEAR_PRICE", "UNIT_MULT" },
                "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                // MEASURE_1 is declared "Double", so should accepting double number type values
                new List<string>(){ "1995", "6" }
            };
        }

        /// <summary>
        /// Map a component that accepts array values to a single dataset column (1 to 1)
        /// and retrieve the values in multiple rows of this column as array values of the component,
        /// according to the transcoding rules specified.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<object[]> TestCases_1to1ColumnsMappingWithArrayTranscoding()
        {
            // rows of specific value of a dataset column are translated to array values of the component
            return new List<object[]>
            {
                // attribute test case
                new object[]
                {
                    // the component; uses codelist ECB:CL_OBS_CONF(1.0)
                    "OBS_CONF",
                    // the mapped dataset column
                    "STO", 
                    // the transcoding rules; local value as key | sdmx values as value
                    new Dictionary<string, List<string>>()
                    {
                        { "YA1", new(){"CODE_11", "CODE_12"} }
                    },
                    // the data query; should return a single observation
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                    // the expected array values for the component
                    new List<string>(){ "CODE_11", "CODE_12" }
                },
                // measure test case
                new object[]
                {
                    // the component; uses codelist ECB:CL_OBS_CONF(1.0)
                    "MEASURE_1",
                    // the mapped dataset column
                    "UNIT_MULT", 
                    // the transcoding rules; local value as key | sdmx values as value
                    new Dictionary<string, List<string>>()
                    {
                        { "6", new(){"123.45", "678.90"} }
                    },
                    // the data query; should return a single observation
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                    // the expected array values for the component
                    new List<string>(){ "123.45", "678.90" }
                }
            };
        }

        /// <summary>
        /// Maps a component with an array of constant values.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<object[]> TestCases_MappingWithConstantArrayValues()
        {
            return new List<object[]>
            {
                // attribute test case
                new object[]
                {
                    // the component
                    "OBS_CONF", 
                    // the constant array values for the component
                    new List<string>(){ "CONSTANT_VALUE_1", "CONSTANT_VALUE_2" },
                    // the data query; should return a single row each time
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                },
                // measure test case
                new object[]
                {
                    "MEASURE_1",
                    new List < string >() { "CONSTANT_VALUE_3", "CONSTANT_VALUE_4" },
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                }
            };
        }

        /// <summary>
        /// Maps a component with a collection of transcodng rules
        /// and an array of default values for the rest codes.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<object[]> TestCases_MappingWithDefaultArrayValues()
        {
            return new List<object[]>
            {
                new object[]
                {
                    // the component
                    "OBS_CONF", 
                    // the mapped dataset column
                    "DECIMALS", 
                    // the transcoding rules; local value as key | sdmx values as value
                    new Dictionary<string, List<string>>()
                    {
                        { "YA1", new(){"Y", "A", "1"} },
                        { "B1G", new(){"B", "1", "G"} }
                    },
                    // the default values
                    new List<string>(){ "DEFAULT_VALUE_1", "DEFAULT_VALUE_2" },
                    // the data query; should return a single row each time
                    // with either a mapped code or unmapped to test the default
                    // this returns null for DECIMALS (mapped to OBS_CONF), so default values should be used
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                    // the expected values
                    new List<string>(){ "DEFAULT_VALUE_1", "DEFAULT_VALUE_2" }
                },
                new object[]
                {
                    "OBS_CONF", 
                    "CONF_STATUS", 
                    new Dictionary<string, List<string>>()
                    {
                        { "YA1", new(){"Y", "A", "1"} },
                        { "B1G", new(){"B", "1", "G"} }
                    },
                    new List<string>(){ "DEFAULT_VALUE_1", "DEFAULT_VALUE_2" },
                    // this returns "F" for CONF_STATUS (which is not mapped), so default values should be used
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                    // the expected values
                    new List<string>(){ "DEFAULT_VALUE_1", "DEFAULT_VALUE_2" }
                },
                new object[]
                {
                    "OBS_CONF", 
                    "STO",
                    new Dictionary<string, List<string>>()
                    {
                        { "YA1", new(){"Y", "A", "1"} },
                        { "B1G", new(){"B", "1", "G"} }
                    },
                    new List<string>(){ "DEFAULT_VALUE_1", "DEFAULT_VALUE_2" },
                    // this returns mapped code
                    "data/dataflow/ESTAT/SSTSCONS_PROD_M/2.0/*?c[FREQ]=A&c[STS_INDICATOR]=_Z&c[STS_BASE_YEAR]=1995",
                    new List<string>(){"Y", "A", "1"}
                },
            };
        }

        #endregion

        #region helper methods

        /// <summary>
        /// This is the common test sequence.
        /// The only thing that changes is the mappings being created for each test case.
        /// </summary>
        /// <param name="componentName"></param>
        /// <param name="query"></param>
        /// <param name="expectedValues"></param>
        /// <param name="addMappings"></param>
        private void TestTemplate(string componentName, string query, List<string> expectedValues,
            Action addMappings)
        {
            // delete existing mappings for component (if any)
            // so that it can be replaced and not add new ones
            DeleteComponentMappings(componentName);

            // add mappings & transcodings
            addMappings.Invoke();

            // get data
            using (DataflowFilterContext context = new DataflowFilterContext(DataflowFilter.Any))
            {
                GetDataAndWriteFileCompact(query, OUTPUT_FILENAME, SdmxSchemaEnumType.VersionThree);
            }

            // validate
            Assert.IsTrue(ValidateScalarValues(OUTPUT_FILENAME, "Obs", "OBS_PRE_BREAK"), "scalar attributes");
            List<string> arrayValues = ParseArrayValues(OUTPUT_FILENAME, componentName);
            CollectionAssert.AreEquivalent(expectedValues, arrayValues);
        }

        /// <summary>
        /// Returns the array values for a given component.
        /// </summary>
        /// <param name="fileName">The xml file name to read the results from.</param>
        /// <param name="componentName">The name of the component for which to get the array values.</param>
        /// <returns></returns>
        private static List<string> ParseArrayValues(string fileName, string componentName)
        {
            List<string> arrayValues = new();

            /* SAMPLE xml
            <Series FREQ="A" REF_AREA="4D" ADJUSTMENT="_Z" STS_INDICATOR="_Z" STS_ACTIVITY="_Z" STS_INSTITUTION="_Z" STS_BASE_YEAR="1995" TIME_FORMAT="P1Y">
              <Obs TIME_PERIOD="C" OBS_STATUS="A">
                <Comp id="MEASURE_1" xsi:type="ns1:MEASURE_1Type">
                  <Value>1995</Value>
                  <Value>6</Value>
                </Comp>
                <Comp id="OBS_CONF" xsi:type="ns1:OBS_CONFType">
                  <Value>YA1</Value>
                  <Value>F</Value>
                </Comp>
              </Obs>
            </Series>
             */

            var fileInfo = new FileInfo(fileName);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                bool readValues = false;
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName.Equals("Comp", StringComparison.InvariantCulture) &&
                        reader.GetAttribute("id").Equals(componentName, StringComparison.InvariantCulture))
                    {
                        readValues = true;
                    }
                    else if (readValues && 
                        reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName.Equals("Value", StringComparison.InvariantCulture))
                    {
                        arrayValues.Add(reader.ReadElementContentAsString());
                    }
                    else if (reader.NodeType == XmlNodeType.EndElement &&
                        reader.LocalName.Equals("Comp", StringComparison.InvariantCulture))
                    {
                        readValues = false;
                    }
                }
            }

            return arrayValues;
        }

        /// <summary>
        /// Validates that the scalar type of attributes have also been written.
        /// </summary>
        /// <param name="fileName">The xml output file.</param>
        /// <param name="attributeAssignmentLevel">Series or Obs</param>
        /// <param name="componentNames">the Ids of the components to check</param>
        /// <returns><c>true</c> if all given attributes where found.</returns>
        private static bool ValidateScalarValues(string fileName, string attributeAssignmentLevel, params string[] componentNames)
        {
            bool result = false;

            var fileInfo = new FileInfo(fileName);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName.Equals(attributeAssignmentLevel, StringComparison.InvariantCulture))
                    {
                        foreach (string componentName in componentNames)
                        {
                            if (reader.GetAttribute(componentName) == null)
                            {
                                return false;
                            }
                            else
                            {
                                result = true;
                            }
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}
