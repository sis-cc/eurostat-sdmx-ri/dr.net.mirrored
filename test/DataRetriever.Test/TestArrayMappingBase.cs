// -----------------------------------------------------------------------
// <copyright file="TestArrayMappingBase.cs" company="EUROSTAT">
//   Date Created : 2023-02-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DryIoc;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStore.Store.Factory;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.XmlHelper;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Estat.Sri.Mapping.Api.Constant;

    /// <summary>
    /// Abstract class for array mapping tests
    /// </summary>
    public abstract class TestArrayMappingBase : TestBase
    {
        private readonly IReadableDataLocationFactory _readableDataLocationFactory = new ReadableDataLocationFactory();
        private readonly IStructureParsingManager _parsingManager = new StructureParsingManager();
        private readonly IStructureSubmitterEngine _submitterEngine;

        protected readonly IEntityPersistenceManager EntityPersistenceManager;

        protected abstract long MappingSetPK { get; }

        public TestArrayMappingBase(string storeId)
            : base(storeId)
        {
            var submitFactory = new StructureSubmitMappingStoreFactory((storeId) => GetConnectionStringSettings());
            _submitterEngine = submitFactory.GetEngine(storeId);
            EntityPersistenceManager = IoCContainer.Resolve<IEntityPersistenceManager>();
        }

        protected string ImportStructures()
        {
            // add structures
            string structuresFile = @"tests/test-sdmxv3.0.0-measures-array-ESTAT+STS+2.0-DF-DSD-full.xml";
            var sdmxObjects = ReadStructures(structuresFile, SdmxSchemaEnumType.VersionThree);
            sdmxObjects.Action = DatasetAction.GetFromEnum(DatasetActionEnumType.Append);
            var importResults = _submitterEngine.SubmitStructures(sdmxObjects);
            foreach (IResponseWithStatusObject response in importResults)
            {
                Assert.AreEqual(ResponseStatus.Success, response.Status,
                    string.Format("found errors when importing structure {0}: {1}",
                    response.StructureReference.MaintainableUrn, string.Join(',', response.Messages.SelectMany(m => m.Text).Select(t => t.Value))));
            }
            string dataflowUrn = importResults
                .First(r => r.StructureReference.MaintainableStructureEnumType == SdmxStructureEnumType.Dataflow)
                .StructureReference.MaintainableUrn.AbsoluteUri;
            return dataflowUrn;
        }

        protected ISdmxObjects ReadStructures(string filepath, SdmxSchemaEnumType sdmxSchema)
        {
            FileInfo fileInfo = new FileInfo(filepath);
            using (IReadableDataLocation rdl = _readableDataLocationFactory.GetReadableDataLocation(fileInfo))
            {
                IStructureWorkspace workspace = _parsingManager.ParseStructures(rdl);

                ISdmxObjects structureBeans = workspace.GetStructureObjects(false);

                XMLParser.ValidateXml(rdl, sdmxSchema);

                return structureBeans;
            }
        }

        protected ComponentMappingEntity CreateTestComponentMappingEntity(string componentName, params string[] datasetColumnNames)
        {
            var mappingEntity = new ComponentMappingEntity()
            {
                Name = componentName,
                ParentId = MappingSetPK.ToString(),
                Type = "test",
                Component = new Estat.Sri.Mapping.Api.Model.Component() { ObjectId = componentName },
                StoreId = this.StoreId
            };
            foreach (string columnName in datasetColumnNames)
            {
                mappingEntity.AddColumn(new DataSetColumnEntity() { Name = columnName });
            }
            return mappingEntity;
        }

        protected void AddTranscodingRules(Dictionary<string, List<string>> transcodingRules, string ddbColumn, string parentId)
        {
            foreach (var rule in transcodingRules)
            {
                var transcodingRule = new TranscodingRuleEntity()
                {
                    ParentId = parentId,
                    StoreId = this.StoreId
                };
                transcodingRule.LocalCodes.Add(new LocalCodeEntity()
                {
                    ObjectId = rule.Key,
                    ParentId = ddbColumn
                });
                foreach (string sdmxCode in rule.Value)
                {
                    transcodingRule.DsdCodeEntities.Add(new IdentifiableEntity() { ObjectId = sdmxCode });
                }
                var ruleWithPK = this.EntityPersistenceManager.Add(transcodingRule);
                Assert.IsNotNull(ruleWithPK);
            }
        }

        protected void DeleteComponentMappings(string componentName)
        {
            EntityQuery mappingQuery = new EntityQuery()
            {
                ObjectId = new Criteria<string>(OperatorType.Exact, componentName),
                ParentId = new Criteria<string>(OperatorType.Exact, MappingSetPK.ToString()),
                EntityType = EntityType.Mapping
            };
            var mappings = IoCContainer.Resolve<IEntityRetrieverManager>().GetEntities<ComponentMappingEntity>(StoreId, mappingQuery, Detail.IdOnly);
            // should be only one mapping if for one component
            foreach (var entityId in mappings.Select(m => m.EntityId))
            {
                this.EntityPersistenceManager.Delete<ComponentMappingEntity>(StoreId, entityId);
            }
        }
    }
}
