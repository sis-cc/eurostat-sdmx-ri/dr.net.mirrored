// -----------------------------------------------------------------------
// <copyright file="TestMaxAllowedObservations.cs" company="EUROSTAT">
//   Date Created : 2015-01-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for Max allowed observation behavior. This is not related to DataQuery First/Last obs or Default limit.
    /// </summary>
    [TestFixture("sqlserver")]
    public class TestMaxAllowedObservations : TestBase
    {
        #region Fields

        /// <summary>
        /// The _data query parse manager.
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager;

        /// <summary>
        /// The _default header
        /// </summary>
        private readonly HeaderImpl _defaultHeader;
        private ISdmxObjectRetrievalManager _retrievalManager;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDataRetrieverSdmxV21"/> class. 
        /// </summary>
        public TestMaxAllowedObservations(string name)
            : base(name)
        {
            this._dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.Null);
            this._defaultHeader = new HeaderImpl("TestMaxAllowedObservations", "ZZ9");
            Database database = new Database(this.GetConnectionStringSettings());
            IRetrievalEngineContainer container = new RetrievalEngineContainer(database);
            ICommonSdmxObjectRetrievalManager retrievalManager = new MappingStoreCommonSdmxObjectRetriever(container);
            this._retrievalManager = new SdmxObjectRetrievalManagerWrapper(retrievalManager);
        }

        #endregion

        /// <summary>
        /// Test unit for 
        /// </summary>

        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AREA", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_A/ALL/ALL?dimensionAtObservation=AllDimensions", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=1&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=1&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=1&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,SSTSCONS_PROD_DT_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=1&firstNObservations=1", "sqlserver", 100)]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver", 1000000, IgnoreReason = "PC Axis not supported yet")]
        public async Task TestRestAsync(string restUrl, string connectionName, int allowedObs)
        {
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            var dataQuery = this._dataQueryParseManager.ParseRestQuery(restUrl, mappingStoreSdmxObjectRetrievalManager);

            Assert.IsNotNull(dataQuery);

            var outputFileName = string.Format("REST-{0}-{1}-{2}-{3}--max-obsout.xml", dataQuery.Dataflow.Id, dataQuery.DimensionAtObservation, dataQuery.FirstNObservations, dataQuery.LastNObservations);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings {Indent = true}))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));

                var dataRetrieverSettings = CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne, "TestRest");
                dataRetrieverSettings.MaxNumberObservations = allowedObs;
                dataRetrieverSettings.StoreId = connectionName;
                ISdmxDataRetrievalWithWriter sdmxDataRetrievalWithWriter = new DataRetrieverCore(dataRetrieverSettings);
                await sdmxDataRetrievalWithWriter.GetDataAsync(dataQuery, dataWriter);
                writer.Flush();
            }
        }

        [TestCase("data/IT,56_259/ALL/ALL", "sqlserver", 10, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&firstNObservations=3", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&firstNObservations=4", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&firstNObservations=4", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&firstNObservations=4", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=3", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=5", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&lastNObservations=5&firstNObservations=4", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&lastNObservations=3&firstNObservations=4", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&lastNObservations=5&firstNObservations=3", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&lastNObservations=5&firstNObservations=3", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=dataonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=nodata", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=serieskeysonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_ACTIVITY&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=dataonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=nodata", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=serieskeysonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=STS_INDICATOR&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=dataonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=nodata", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=serieskeysonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=dataonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=nodata", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=serieskeysonly", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,SSTSIND_PROD_M/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,DEMOGRAPHY/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,DEMOGRAPHY/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=TIME_PERIOD&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException), IgnoreReason = "PC Axis not supported yet")]
        [TestCase("data/ESTAT,CPI_PCAXIS/ALL/ALL?dimensionAtObservation=AllDimensions&detail=full", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException), IgnoreReason = "PC Axis not supported yet")]
        public void TestRest(string restUrl, string connectionName, int allowedObs, Type type)
        {
            Assert.ThrowsAsync(type, async () => await TestRestAsync(restUrl, connectionName, allowedObs));
        }

        [TestCase("tests/get-structure-specific-primary-measure-op.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-primary-measure-op2.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-primary-measure-op-and.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data-firstnobs-lastnobs.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full-notequal-and.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full-equal-or.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full-numeric-less.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full-numeric-greater.xml", "sqlserver", 100)]
        [TestCase("tests/text-search-madb-contains.xml", "sqlserver", 100)]
        [TestCase("tests/text-search-madb-not-contains.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data-dimatobs.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data.xml", "sqlserver", 100)]
        [TestCase("tests/ESTAT+SSTSIND_PROD_M+2.0_2014_01_28_18_05_46.query.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data-dimatobs.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data-dimatobs-sts_activity.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-data2.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-dataonly.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-full-all-dim.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-serieskey.xml", "sqlserver", 100)]
        [TestCase("tests/get-structure-specific-nodata.xml", "sqlserver", 100)]
        public async Task TestSoap21Async(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            IList<IComplexDataQuery> dataQueries;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildComplexDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v21-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings {Indent = true}))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
                var dataRetrieverSettings = CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne, "TestSoap21");
                dataRetrieverSettings.MaxNumberObservations = allowedObs;
                dataRetrieverSettings.StoreId = name;
                IAdvancedSdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(dataRetrieverSettings);
                await advancedSdmxDataRetrievalWithWriter.GetDataAsync(dataQuery, dataWriter);
                writer.Flush();
            }
        }

        [TestCase("tests/get-structure-specific-data-firstnobs-lastnobs.xml", "sqlserver", 1, typeof(SdmxResponseSizeExceedsLimitException))]
        [TestCase("tests/get-structure-specific-data-default-limit.xml", "sqlserver", 100, typeof(SdmxResponseTooLargeException))]
        [TestCase("tests/IT1+161_267+1.0_2014_06_03_17_17_55.query.xml", "sqlserver", 100, typeof(SdmxResponseSizeExceedsLimitException))]
        public void TestSoap21(string filePath, string name, int allowedObs, Type type)
        {
            Assert.ThrowsAsync(type, async () => await TestSoap21Async(filePath, name, allowedObs));
        }

        [TestCase("tests/v20/QueryTimeRangeV20.xml", "sqlserver", 1)]
        public void TestSDMXv20Fail(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            IList<IDataQuery> dataQueries;
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v20-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings {Indent = true}))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));
                var dataRetrieverSettings = CreateSettings(SdmxSchemaEnumType.VersionTwo, "TestSDMXv20");
                dataRetrieverSettings.MaxNumberObservations = allowedObs;
                dataRetrieverSettings.StoreId = name;
                ISdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(dataRetrieverSettings);
                Assert.ThrowsAsync<SdmxResponseSizeExceedsLimitException>(async () => await advancedSdmxDataRetrievalWithWriter.GetDataAsync(dataQuery, dataWriter));
                writer.Flush();
            }
        }


        [TestCase("tests/v20/QueryTimeRangeV20.xml", "sqlserver", 1000)]
        public void TestSDMXv20(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            IList<IDataQuery> dataQueries;
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v20-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings {Indent = true}))
            {
                IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));
                var dataRetrieverSettings = CreateSettings(SdmxSchemaEnumType.VersionTwo, "TestSDMXv20");
                dataRetrieverSettings.MaxNumberObservations = allowedObs;
                dataRetrieverSettings.StoreId = name;
                ISdmxDataRetrievalWithWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(dataRetrieverSettings);
                advancedSdmxDataRetrievalWithWriter.GetData(dataQuery, dataWriter);
                writer.Flush();
            }
        }


        [TestCase("tests/v20/get-CENSUSHUB_Q_XS1.xml", "sqlserver", 100000)]
        public async Task TestSDMXv20XSAsync(string filePath, string name, int allowedObs)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[name];
            IList<IDataQuery> dataQueries;
            var mappingStoreSdmxObjectRetrievalManager = _retrievalManager;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            Assert.IsNotEmpty(dataQueries);
            var dataQuery = dataQueries.First();

            var outputFileName = string.Format("{0}-soap-v20-xs-max-obs-out.xml", filePath);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings {Indent = true}))
            {
                ICrossSectionalWriterEngine dataWriter = new CrossSectionalWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo));
                var dataRetrieverSettings = CreateSettings(SdmxSchemaEnumType.VersionTwo, "TestSDMXv20XS");
                dataRetrieverSettings.MaxNumberObservations = allowedObs;
                dataRetrieverSettings.StoreId = name;
                ISdmxDataRetrievalWithCrossWriter advancedSdmxDataRetrievalWithWriter = new DataRetrieverCore(dataRetrieverSettings);
                await advancedSdmxDataRetrievalWithWriter.GetDataAsync(dataQuery, dataWriter);
                writer.Flush();
            }
        }


        [TestCase("tests/v20/get-CENSUSHUB_Q_XS1.xml", "sqlserver", 4, typeof(SdmxResponseSizeExceedsLimitException))]
        public void TestSDMXv20XS(string filePath, string name, int allowedObs, Type type)
        {
            Assert.ThrowsAsync(type, async () => await TestSDMXv20XSAsync(filePath, name, allowedObs));
        }
    }
}