// -----------------------------------------------------------------------
// <copyright file="TestBase.cs" company="EUROSTAT">
//   Date Created : 2016-10-24
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Plugin.SqlServer.Builder;

namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using DryIoc;
    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.Mapping.Api.Constant;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Mapping.MappingStore.Factory;
    using Estat.Sri.Mapping.MappingStore.Manager;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using log4net;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;
    using Org.Sdmxsource.Util.ResourceBundle;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TestBase));
        /// <summary>
        /// The _data query parse manager.
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager;
        /// <summary>
        /// The container
        /// </summary>
        private readonly Container _container;

        protected readonly IMappingStoreManager mappingStoreManager;

        protected readonly IConfigurationStoreManager connectionStringManager;

        private readonly ISdmxObjectRetrievalManager _retrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        protected TestBase(string storeId)
        {
            var appSetting = ConfigurationManager.AppSettings["RetrieverFactory"];
            
            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(appSetting);
            SdmxException.SetMessageResolver(new MessageDecoder());
            this._dataQueryParseManager = new DataQueryParseManager();
            try
            {
                StoreId = storeId;
                _container =
                    new Container(
                        rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments)
                            .WithoutThrowOnRegisteringDisposableTransient());
                Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);

                var sqlServerPluginAssembly = typeof(SqlServerConnectionBuilder).Assembly;
                var mySqlPluginAssembly = typeof(Estat.Sri.Plugin.MySql.Engine.MySqlDatabaseProviderEngine).Assembly;
                var oraclePluginAssembly = typeof(Estat.Sri.Plugin.Oracle.Builder.OracleSettingsBuilder).Assembly;
                var assemblies = new List<Assembly>()
                {
                    sqlServerPluginAssembly,
                    mySqlPluginAssembly,
                    oraclePluginAssembly
                };
                assemblies.AddRange(new[] { typeof(IDatabaseProviderManager).Assembly, typeof(DatabaseManager).Assembly });
                _container.RegisterMany(assemblies, type => !typeof(IEntity).IsAssignableFrom(type));

                //_container.Register<IEntityAuthorizationManager, StubAuthorizationManager>();
                _container.Unregister<IEntityAuthorizationManager>();
                MappingStoreIoc.Register<RetrievalEngineContainerFactory>("MappingStoreRetrieversFactory");
                _container.Register<IStructureParsingManager, StructureParsingManager>(
                    made: Made.Of(() => new StructureParsingManager()));
                _container.Register<IStructureWriterFactory, SdmxStructureWriterFactory>();
                _container.Register<IStructureWriterManager, StructureWriterManager>();
                _container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>();
                mappingStoreManager = _container.Resolve<IMappingStoreManager>();
                connectionStringManager = _container.Resolve<IConfigurationStoreManager>();
                MappingStoreIoc.Container.RegisterMany(
                new[] { typeof(IEntityRetrieverManager).Assembly, typeof(EntityPeristenceFactory).Assembly },
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);

                _retrievalManager = new SdmxObjectRetrievalManagerWrapper(new MappingStoreCommonSdmxObjectRetriever(GetRetrievalEngineContainer()));
            }
            catch (Exception e)
            {
                _log.Error(e);


                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Gets or sets the store identifier.
        /// </summary>
        /// <value>
        /// The store identifier.
        /// </value>
        public string StoreId { get; set; }

        /// <summary>
        /// Gets the containers
        /// </summary>
        /// <value>
        /// </value>
        public Container IoCContainer
        {
            get
            {
                return _container;
            }
        }

        /// <summary>
        /// Gets the complex data queries.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="mappingStoreSdmxObjectRetrievalManager">The mapping store SDMX object retrieval manager.</param>
        /// <returns>Complex data queries</returns>
        protected IList<IComplexDataQuery> GetComplexDataQueries(string filePath, ISdmxObjectRetrievalManager mappingStoreSdmxObjectRetrievalManager)
        {
            IList<IComplexDataQuery> dataQueries;
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filePath))
            {
                dataQueries = this._dataQueryParseManager.BuildComplexDataQuery(dataLocation, mappingStoreSdmxObjectRetrievalManager);
            }

            return dataQueries;
        }


        /// <summary>
        /// Gets the complex data queries.
        /// </summary>
        /// <param name="queryUrl">The file path.</param>
        /// <param name="mappingStoreSdmxObjectRetrievalManager">The mapping store SDMX object retrieval manager.</param>
        /// <returns>Complex data queries</returns>
        protected IDataQuery GetRestQuery(string queryUrl, ISdmxObjectRetrievalManager mappingStoreSdmxObjectRetrievalManager)
        {
            return this._dataQueryParseManager.ParseRestQuery(queryUrl, mappingStoreSdmxObjectRetrievalManager);
        }

        /// <summary>
        /// Creates the settings.
        /// </summary>
        /// <param name="sdmxSchemaEnumType">Type of the SDMX schema enum.</param>
        /// <returns></returns>
        protected DataRetrieverSettings CreateSettings(SdmxSchemaEnumType sdmxSchemaEnumType, string testId)
        {
            var connectionString = this.GetConnectionStringSettings();
            DataRetrieverSettings settings = new DataRetrieverSettings();
            settings.SdmxSchemaVersion = sdmxSchemaEnumType;
            settings.StoreId = StoreId;
            settings.ComponentMappingManager = this.IoCContainer.Resolve<IComponentMappingManager>();
            settings.ComponentMappingValidationManager = this.IoCContainer.Resolve<IComponentMappingValidationManager>();
            settings.HeaderRetrieverEngine = new HeaderRetrieverEngine(connectionString);
            settings.ConnectionBuilder = this.IoCContainer.Resolve<IBuilder<DbConnection, DdbConnectionEntity>>();
            settings.ConnectionStringSettings = connectionString;
            settings.DefaultHeader = new HeaderImpl(testId, "ZZ9");
            return settings;
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <returns></returns>
        protected ConnectionStringSettings GetConnectionStringSettings()
        {
            var connectionString = this.connectionStringManager.GetSettings<ConnectionStringSettings>().FirstOrDefault(stringSettings => stringSettings.Name == this.StoreId);
            return connectionString;
        }

        protected IRetrievalEngineContainer GetRetrievalEngineContainer()
        {
            return MappingStoreIoc.Container.Resolve<IRetrievalEngineContainerFactory>(serviceKey: MappingStoreIoc.ServiceKey)
                .GetRetrievalEngineContainer(new Database(this.GetConnectionStringSettings()));
        }

        protected void InitializeMappingStore()
        {
            var engineByStoreId = this.mappingStoreManager.GetEngineByStoreId(StoreId);
            var actionResult = engineByStoreId.Initialize(new DatabaseIdentificationOptions() { StoreId = StoreId });
            Assert.That(actionResult.Status, Is.EqualTo(StatusType.Success), string.Join(", ", actionResult.Messages));
        }

        protected IDataQueryV2 GetDataAndWriteFileGeneric(string query, string outputFileName, SdmxSchemaEnumType sdmxSchema)
        {
            return GetDataAndWriteFile(query, outputFileName, sdmxSchema,
                (xmlWriter) => new GenericDataWriterEngine(xmlWriter, SdmxSchema.GetFromEnum(sdmxSchema)));
        }

        protected IDataQueryV2 GetDataAndWriteFileCompact(string query, string outputFileName, SdmxSchemaEnumType sdmxSchema)
        {
            return GetDataAndWriteFile(query, outputFileName, sdmxSchema,
                (xmlWriter) => new CompactDataWriterEngine(xmlWriter, SdmxSchema.GetFromEnum(sdmxSchema)));
        }

        private IDataQueryV2 GetDataAndWriteFile(string query, string outputFileName, SdmxSchemaEnumType sdmxSchema, 
            Func<XmlWriter, IDataWriterEngine> getDataWriter)
        {
            IDataQueryV2 dataQuery = BuildDataQuery(query);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            {
                var dataWriter = getDataWriter(writer);
                this.GetDataRetriever(sdmxSchema).GetData(dataQuery, dataWriter);
                writer.Flush();
            }
            return dataQuery;
        }

        protected IDataQueryV2 BuildDataQuery(string queryPath)
        {
            var parts = queryPath.Split('?');
            var queryString = parts[0].Split('/');
            IDictionary<string, string> paramsDict = new Dictionary<string, string>();

            if (parts.Count() == 2)
            {
                var queryParameters = parts[1].Split('&');
                foreach (string parameter in queryParameters)
                {
                    var pair = parameter.Split('=');
                    paramsDict.Add(pair[0], pair[1]);
                }
            }
            
            IRestDataQueryV2 restDataQuery = new RESTDataQueryCoreV2(queryString, paramsDict);
            IDataQueryV2 query = new DataQueryV2Impl(restDataQuery, _retrievalManager, false);
            return query;
        }

        protected DataRetrieverCore GetDataRetriever(SdmxSchemaEnumType sdmxSchema)
        {
            return new DataRetrieverCore(CreateSettings(sdmxSchema, $"TestDataRetriever{sdmxSchema}"));
        }
    }
}