﻿// -----------------------------------------------------------------------
// <copyright file="TestDataRetrieverSdmxV20.cs" company="EUROSTAT">
//   Date Created : 2015-03-06
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace DataRetriever.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Xml;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.DataRetriever.Model;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for <see cref="DataRetrieverCore"/>
    /// </summary>
    [TestFixture("sqlserver")]
    public class TestDataRetrieverSdmxV20 : TestBase
    {
        /// <summary>
        /// The data query parse manager.
        /// </summary>
        private IDataQueryParseManager dataQueryParseManager;

        /// <summary>
        /// The parsing manager.
        /// </summary>
        private IStructureParsingManager parsingManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase"/> class.
        /// </summary>
        public TestDataRetrieverSdmxV20(string storeId)
            : base(storeId)
        {
            dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.Null);
            parsingManager = new StructureParsingManager();
        }

        /// <summary>
        /// Test unit for <see cref="DataRetrieverCore"/> 
        /// </summary>
        [TestCase("tests/v20/two-components-under-or.xml", "tests/v20/dataflows.xml", "tests/v20/ESTAT+STS+2.0.xml")]
        public void TestNestedAndOr(string queryFile, string dataflowFile, string dsdFile)
        {
            var retrievalManager = this.GetSdmxObjectRetrievalManager(dataflowFile, dsdFile);
            DataRetrieverSettings settings = this.CreateSettings(SdmxSchemaEnumType.VersionTwo, "TestNestedAndOr");

            ISdmxDataRetrievalWithWriter dr = new DataRetrieverCore(settings);
            IList<IDataQuery> dataQuery;
            using (var fileReadableDataLocation = new FileReadableDataLocation(queryFile))
            {
                dataQuery = this.dataQueryParseManager.BuildDataQuery(fileReadableDataLocation, retrievalManager);
                Assert.IsNotEmpty(dataQuery);
            }

            var outputFileName = string.Format("{0}-TestNestedAndOr-out.xml", queryFile);
            using (XmlWriter writer = XmlWriter.Create(outputFileName, new XmlWriterSettings { Indent = true }))
            using (IDataWriterEngine dataWriter = new CompactDataWriterEngine(writer, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo)))
            {
                dr.GetData(dataQuery.First(), dataWriter);
            }
        }


        /// <summary>
        /// Gets the SDMX object retrieval manager.
        /// </summary>
        /// <param name="dataflowFile">The dataflow file.</param>
        /// <param name="dsdFile">The DSD file.</param>
        /// <returns>The SDMX Object retrieval manager</returns>
        private ISdmxObjectRetrievalManager GetSdmxObjectRetrievalManager(string dataflowFile, string dsdFile)
        {
            ISdmxObjects objects = new SdmxObjectsImpl();
            using (var fileDataFlowReadableDataLocation = new FileReadableDataLocation(dataflowFile))
            {
                var structureWorkspace = this.parsingManager.ParseStructures(fileDataFlowReadableDataLocation);
                ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);
                objects.Merge(structureBeans);
                Assert.IsNotEmpty(structureBeans.Dataflows);
            }

            using (var fileKeybeanReadableDataLocation = new FileReadableDataLocation(dsdFile))
            {
                var structureWorkspace = this.parsingManager.ParseStructures(fileKeybeanReadableDataLocation);
                ISdmxObjects structureBeans = structureWorkspace.GetStructureObjects(false);
                objects.Merge(structureBeans);
                Assert.IsNotEmpty(structureBeans.DataStructures);
            }
            ISdmxObjectRetrievalManager retrievalManager = new InMemoryRetrievalManager(objects);
            return retrievalManager;
        }

    }
}