// -----------------------------------------------------------------------
// <copyright file="TestREST20DataRequests.cs" company="EUROSTAT">
//   Date Created : 2022-10-20
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace DataRetriever.Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.StructureRetriever.Manager;
    using NUnit.Framework;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;

    [TestFixture("sqlserver")]
    public class TestREST20DataRequests : TestBase
    {
        public TestREST20DataRequests(string storeId)
            : base(storeId)
        {
        }

        // these test cases are performed on mastore_test docker image database
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*.*.*.*.*.B1GQ?c[CONF_STATUS]=F", 3)]//test c-parameter combined old filter 
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09", 34)]// test time range special case
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09&c[CONF_STATUS]=F", 17)] // test time range with another c-parameter
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*.*.*.*.*.B1GQ?c[TIME_PERIOD]=ge:1994-Q3+le:1996-M09&c[CONF_STATUS]=F", 1)]// test all together
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=BTE,_T", 8)]// test OR
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[OBS_VALUE]=ge:300+le:302", 34)]// ranged; note that actually string values
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=sw:D21", 8)]// test operator sw
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=ew:1", 16)]// test operator ew
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=ne:D21", 64)]// test operator ne
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=co:T", 20)]// test operator co
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=nc:T", 48)]// test operator nc
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=sw:D+co:3", 8)]// starts with D and contains 3
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=sw:K,L", 8)]// starts either with K or L
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[ACTIVITY]=sw:K,ew:Z", 24)]// starts with K or ends with Z
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?c[STO]=B1GQ&c[CONF_STATUS]=F,BM", 3)]// two parameters
        public void TestParameter_c(string query, int numObsExpected)
        {
            var outputFileName = "tests/c-parameter-out.xml";
            GetDataAndWriteFileGeneric(query, outputFileName, SdmxSchemaEnumType.VersionTwoPointOne);
            int numObservations = CountObservations(outputFileName);
            Assert.AreEqual(numObsExpected, numObservations);
        }

        // these test cases are performed on mastore_test docker image database
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=dataset", 0)]
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=series", 2)]
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=obs", 3)]
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=dsd", 9)] // all non-metadata attributes 
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=msd", 0)] // no msd exists; change this when we have a working example
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=all", 9)] // all; dsd + msd
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=none", 0)]
        // test another dataflow; filter the results because its too many
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=dataset", 2)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=series", 4)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=obs", 0)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=dsd", 6)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=msd", 0)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=all", 6)]
        [TestCase("data/dataflow/ESTAT/DEMOGRAPHY/+/*.Greece.*.*.16666.*?attributes=none", 0)]
        public void TestParameter_attributes(string query, int numAttrExpected)
        {
            var outputFileName = "tests/attributes-parameter-out.xml";
            IDataQueryV2 dataQuery = GetDataAndWriteFileGeneric(query, outputFileName, SdmxSchemaEnumType.VersionTwoPointOne);
            int numAttr = ParseAttributes(outputFileName, dataQuery.Attributes.EnumType).Count;
            Assert.AreEqual(numAttrExpected, numAttr);
        }

        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=CONF_STATUS")]
        [TestCase("data/dataflow/ESTAT/NA_ESTAT_MAIN_T0101A/+/*?attributes=CONF_STATUS,UNIT_MULT")]
        public void TestParameter_attributesSpecific(string query)
        {
            var outputFileName = "tests/attributes-parameter-out.xml";
            IDataQueryV2 dataQuery = GetDataAndWriteFileGeneric(query, outputFileName, SdmxSchemaEnumType.VersionTwoPointOne);
            string[] queryAttr = dataQuery.Attributes.RestParam.Split(',');
            string[] dataAttr = ParseAttributes(outputFileName, dataQuery.Attributes.EnumType).ToArray();
            CollectionAssert.AreEquivalent(queryAttr, dataAttr);
        }

        /// <summary>
        /// Counts the number of observations returned in an xml data file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static int CountObservations(string fileName)
        {
            int count = 0;

            var fileInfo = new FileInfo(fileName);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName.Equals("Obs", StringComparison.Ordinal))
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Gets the distinct attributes names in the given <paramref name="fileName"/>,
        /// that are under the given <paramref name="attachmentLevel"/>.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="attachmentLevel"></param>
        /// <returns>A set of attribute names.</returns>
        /// <exception cref="SdmxSemmanticException">for attributes with attachment level other than <paramref name="attachmentLevel"/>.</exception>
        private static HashSet<string> ParseAttributes(string fileName, AttributesEnumType attachmentLevel)
        {
            HashSet<string> attributes = new HashSet<string>();

            var fileInfo = new FileInfo(fileName);
            Assert.IsTrue(fileInfo.Exists);
            using (var fileStream = fileInfo.OpenRead())
            using (var reader = XmlReader.Create(fileStream))
            {
                AttributesEnumType currentLevel = AttributesEnumType.Dsd;
                bool isAttribute = false;
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        switch (reader.LocalName)
                        {
                            case "Group":
                                currentLevel = AttributesEnumType.Dsd;
                                isAttribute = false;
                                break;
                            case "Series":
                                currentLevel = AttributesEnumType.Series;
                                isAttribute = false;
                                break;
                            case "Obs":
                                currentLevel = AttributesEnumType.Obs;
                                isAttribute = false;
                                break;
                            case "DataSet":
                                currentLevel = AttributesEnumType.Dataset;
                                isAttribute = false;
                                break;
                        }
                        if (reader.LocalName.Equals("Attributes", StringComparison.OrdinalIgnoreCase))
                        {
                            if (!(attachmentLevel == AttributesEnumType.All ||
                                attachmentLevel == AttributesEnumType.Specific ||
                                attachmentLevel == AttributesEnumType.Dsd ||
                                attachmentLevel == currentLevel))
                            {
                                throw new SdmxSemmanticException($"Attributes were found under {currentLevel}, while {attachmentLevel} was requested.");
                            }
                            isAttribute = true;
                        }
                        if (isAttribute && reader.LocalName.Equals("Value", StringComparison.OrdinalIgnoreCase))
                        {
                            attributes.Add(reader.GetAttribute("id"));
                        }
                    }
                }
            }

            return attributes;
        }
    }
}
