// -----------------------------------------------------------------------
// <copyright file="TestDeltaQueries.cs" company="EUROSTAT">
//   Date Created : 2016-11-03
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;

namespace DataRetriever.Test
{
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Sri.MappingStoreRetrieval.Config;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test Delta queries
    /// </summary>
    [TestFixture("sqlserver")]
    [TestFixture("odp")]
    [TestFixture("mysql")]
    public class TestDeltaQueries : TestBase
    {
        /// <summary>
        /// The advanced SDMX data retrieval
        /// </summary>
        private readonly IAdvancedSdmxDataRetrievalWithWriter _advancedSdmxDataRetrieval;

        /// <summary>
        /// The data retrieval with writer
        /// </summary>
        private readonly ISdmxDataRetrievalWithWriter _dataRetrievalWithWriter;

        /// <summary>
        /// The retriever
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _retriever;

        /// <summary>
        /// The data writer manager
        /// </summary>
        private readonly IDataWriterManager _dataWriterManager;

        /// <summary>
        /// The data reader manager
        /// </summary>
        private readonly IDataReaderManager _dataReaderManager;

        /// <summary>
        /// The data location factory
        /// </summary>
        private readonly IReadableDataLocationFactory _dataLocationFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="TestDeltaQueries" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public TestDeltaQueries(string name): base(name)
        {
            var connectionString = new ConnectionStringRetriever().GetConnectionStringSettings(name);
            var dataRetrieval = new DataRetrieverCore(CreateSettings(SdmxSchemaEnumType.VersionTwoPointOne, "TestDeltaQueries"));
            this._advancedSdmxDataRetrieval = dataRetrieval;
            this._dataRetrievalWithWriter = dataRetrieval;
            this._dataWriterManager = new DataWriterManager();
            this._dataReaderManager = new DataReaderManager();
            this._dataLocationFactory = new ReadableDataLocationFactory();
            Database database = new Database(this.GetConnectionStringSettings());
            IRetrievalEngineContainer container = new RetrievalEngineContainer(database);
            ICommonSdmxObjectRetrievalManager retrievalManager = new MappingStoreCommonSdmxObjectRetriever(container);
            this._retriever = new SdmxObjectRetrievalManagerWrapper(retrievalManager);
        }

        /// <summary>
        /// Tests the rest last update.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="expectedObservations">The expected observations.</param>
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL?updatedAfter=2015-06-21T00:00:00Z", 6)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL", 11)]
        [TestCase("data/NAMAIN_IDC_N/ALL", 9)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/M......?updatedAfter=2015-06-21T00:00:00Z", 5)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/M......", 7)]
        [TestCase("data/STSCONS_UPDATE_CONS_LAST/ALL?updatedAfter=2015-05-21T00:00:00Z", 13)]
        [TestCase("data/ESTAT,STSCONS_UPDATE_CONS_LAST/ALL", 13)]
        [TestCase("data/STSCONS_UPDATE_OFF/ALL?updatedAfter=2015-06-21T00:00:00Z", 72)]
        public void TestRestLastUpdate(string query, int expectedObservations)
        {
            var dataQuery = this.GetRestQuery(query, this._retriever);

            FileInfo result = new FileInfo(string.Format(CultureInfo.InvariantCulture, "TestRESTLastUpdate-{0}", dataQuery.Dataflow.Id));
            using (var outPutStream = result.Create())
            using (var writer = this._dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21)), outPutStream))
            {
                this._dataRetrievalWithWriter.GetData(dataQuery, writer);
            }

            var obsCount = this.GetObsCount(result, dataQuery);

            Assert.That(obsCount, Is.EqualTo(expectedObservations));
        }

        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL?updatedAfter=2016-06-21T00:00:00Z", 0, typeof(DataRetrieverException))]
        [TestCase("data/STSCONS_UPDATE_CONS_LAST/ALL?updatedAfter=2016-06-21T00:00:00Z", 0, typeof(SdmxNoResultsException))]
        public void TestRestLastUpdateException(string query, int expectedObservations,Type type)
        {
            Assert.Throws(
                type, () => { TestRestLastUpdate(query, expectedObservations); });
        }

        /// <summary>
        /// Tests the rest update status.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="expectedObservations">The expected observations.</param>
        /// <param name="expectedDatasets">The expected datasets.</param>
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL?updatedAfter=2015-06-21T00:00:00Z", 6, 3)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL", 11, 1)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/M......?updatedAfter=2015-06-21T00:00:00Z", 5, 3)]
        [TestCase("data/STSCONS_UPDATE_FROM_DDB/M......", 7, 1)]
        [TestCase("data/STSCONS_UPDATE_STATUS_CONS/ALL?updatedAfter=2015-06-21T00:00:00Z", 13, 1)]
        [TestCase("data/STSCONS_UPDATE_STATUS_CONS/ALL", 13, 1)]
        [TestCase("data/STSCONS_UPDATE_STATUS_CONS/ALL?updatedAfter=2016-06-21T00:00:00Z", 13, 1)]
        [TestCase("data/STSCONS_UPDATE_OFF/ALL?updatedAfter=2015-06-21T00:00:00Z", 72, 1)]
        public void TestRestUpdateStatus(string query, int expectedObservations, int expectedDatasets)
        {
            var dataQuery = this.GetRestQuery(query, this._retriever);

            FileInfo result = new FileInfo(string.Format(CultureInfo.InvariantCulture, "TestRESTLastUpdate-{0}", dataQuery.Dataflow.Id));
            using (var outPutStream = result.Create())
            using (var writer = this._dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21)), outPutStream))
            {
                this._dataRetrievalWithWriter.GetData(dataQuery, writer);
            }

            var obsCount = this.GetObsCount(result, dataQuery);
            var datasetCount = this.GetDataSetCount(result, dataQuery);

            Assert.That(obsCount, Is.EqualTo(expectedObservations));
            Assert.That(datasetCount, Is.EqualTo(expectedDatasets));
        }

        [TestCase("data/STSCONS_UPDATE_FROM_DDB/ALL?updatedAfter=2016-06-21T00:00:00Z", 0, 0, typeof(DataRetrieverException),"NoRecordsFound", Description = "Possibly refactor DR to throw SdmxNoResultsException")]
        public void TestRestUpdateStatus(string query, int expectedObservations, int expectedDatasets, Type type, string expectedMessage)
        {
            Assert.Throws(type, () => { TestRestUpdateStatus(query, expectedObservations, expectedDatasets); }, expectedMessage);
        }

        /// <summary>
        /// Tests the soap21 last updated.
        /// </summary>
        /// <param name="queryFile">The query file.</param>
        /// <param name="expectedObservations">The expected observations.</param>
        [TestCase("tests/get-structure-specific-last_update_ddb.xml", 5)]
        [TestCase("tests/get-structure-specific-last_update_ddb_end_period.xml", 7)]
        [TestCase("tests/get-structure-specific-last_update_ddb_with_constraints.xml", 4)]
        [TestCase("tests/get-structure-specific-last_update_ddb_range.xml", 4)]
        [TestCase("tests/get-structure-specific-last_update_cons.xml", 13)]
        [TestCase("tests/get-structure-specific-update-off.xml", 72)]
        
        public void TestSoap21LastUpdated(string queryFile, int expectedObservations)
        {
            var dataQueries = this.GetComplexDataQueries(queryFile, this._retriever);
            FileInfo result = new FileInfo(string.Format(CultureInfo.InvariantCulture, "TestSoap21LastUpdated-{0}", new FileInfo(queryFile).Name));
            var complexDataQuery = dataQueries.First();
            using (var outPutStream = result.Create())
            using (var writer = this._dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21)), outPutStream))
            {
                this._advancedSdmxDataRetrieval.GetData(complexDataQuery, writer);
            }

            var obsCount = this.GetObsCount(result, complexDataQuery);

            Assert.That(obsCount, Is.EqualTo(expectedObservations));
        }

        [TestCase("tests/get-structure-specific-last_update_cons_no_results.xml", 0,  typeof(SdmxNoResultsException))]
        [TestCase("tests/get-structure-specific-last_update_ddb_no_results.xml", 0,  typeof(DataRetrieverException))]
        public void TestSoap21LastUpdatedException(string queryFile, int expectedObservations, Type type)
        {
            Assert.Throws(type, () => TestSoap21LastUpdated(queryFile, expectedObservations));
        }

        /// <summary>
        /// Tests the soap21 last updated.
        /// </summary>
        /// <param name="queryFile">The query file.</param>
        /// <param name="expectedObservations">The expected observations.</param>
        [TestCase("tests/get-structure-specific-ddb-update-active.xml", 11)]
        [TestCase("tests/get-structure-specific-ddb-update-added.xml", 8)]
        [TestCase("tests/get-structure-specific-ddb-update-updated.xml", 3)]
        [TestCase("tests/get-structure-specific-ddb-update-deleted.xml", 2)]
        [TestCase("tests/get-structure-specific-cons-update-active.xml", 13)]
        [TestCase("tests/get-structure-specific-cons-update-added.xml", 13)]
        [TestCase("tests/get-structure-specific-ddb-update-deleted_with_last_up.xml", 1)]
        public void TestSoap21UpdateStatus(string queryFile, int expectedObservations)
        {
            var dataQueries = this.GetComplexDataQueries(queryFile, this._retriever);
            FileInfo result = new FileInfo(string.Format(CultureInfo.InvariantCulture, "TestSoap21LastUpdated-{0}", new FileInfo(queryFile).Name));
            var complexDataQuery = dataQueries.First();
            using (var outPutStream = result.Create())
            using (var writer = this._dataWriterManager.GetDataWriterEngine(new SdmxDataFormatCore(DataType.GetFromEnum(DataEnumType.Compact21)), outPutStream))
            {
                this._advancedSdmxDataRetrieval.GetData(complexDataQuery, writer);
            }

            var obsCount = this.GetObsCount(result, complexDataQuery);

            Assert.That(obsCount, Is.EqualTo(expectedObservations));
        }


        [TestCase("tests/get-structure-specific-cons-update-updated.xml", 0, typeof(SdmxNoResultsException))]
        [TestCase("tests/get-structure-specific-cons-update-deleted.xml", 0, typeof(SdmxNoResultsException))]
        public void TestSoap21UpdateStatusException(string queryFile, int expectedObservations, Type type)
        {
            Assert.Throws(type, () => TestSoap21UpdateStatus(queryFile, expectedObservations));
        }

        /// <summary>
            /// Gets the observation count.
            /// </summary>
            /// <param name="result">The result.</param>
            /// <param name="dataQuery">The data query.</param>
            /// <returns>Gets the observation count</returns>
            private int GetObsCount(FileInfo result, IBaseDataQuery dataQuery)
        {
            int obsCount = 0;
            using (var readableLocation = this._dataLocationFactory.GetReadableDataLocation(result))
            using (var reader = this._dataReaderManager.GetDataReaderEngine(readableLocation, dataQuery.DataStructure, dataQuery.Dataflow))
            {
                while (reader.MoveNextDataset())
                {
                    while (reader.MoveNextKeyable())
                    {
                        if (reader.CurrentKey.Series)
                        {
                            while (reader.MoveNextObservation())
                            {
                                obsCount++;
                            }
                        }
                    }
                }
            }

            return obsCount;
        }

        /// <summary>
        /// Gets the dataset count.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>Gets the dataset count</returns>
        private int GetDataSetCount(FileInfo result, IBaseDataQuery dataQuery)
        {
            int count = 0;
            using (var readableLocation = this._dataLocationFactory.GetReadableDataLocation(result))
            using (var reader = this._dataReaderManager.GetDataReaderEngine(readableLocation, dataQuery.DataStructure, dataQuery.Dataflow))
            {
                while (reader.MoveNextDataset())
                {
                    count++;
                }
            }

            return count;
        }
    }
}